<?php
date_default_timezone_set('Europe/Warsaw');
error_reporting(E_ALL);
ini_set('display_errors',1);

define('REQUEST_MICROTIME', microtime(true));
chdir(dirname(dirname(__DIR__)));
define('APPLICATION_PATH', __DIR__.'/../..');

include 'init_autoloader.php';
Zend\Mvc\Application::init(include 'config/console/console.config.php')->run();
