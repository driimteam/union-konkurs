<?php
date_default_timezone_set('Europe/Warsaw');
error_reporting(0);
ini_set('display_errors', 0);

/**
 * Sciezka do glownego katalogu z systemem
 *
 */

define('_ROOT_DIR', realpath(dirname(__FILE__) . '/../../../'));
define('_UPLOAD_BASEDIR', _ROOT_DIR . '/data/upload');


/**
 * Include path
 */
set_include_path('.' . PATH_SEPARATOR . _ROOT_DIR . '/vendor/');

function __autoload($name) {
    require_once str_replace('_', '/', $name) . '.php';
    //Zend_Loader :: loadClass($name);
}

$inputParams = array();
$_FILEPATH = '';


if(preg_match("/attachments/i",$_SERVER['REQUEST_URI'])) {
    list(,$lparams) = explode("attachments",$_SERVER['REQUEST_URI']);
    list($lparams) = explode('?',$lparams);
	
    $_FILEPATH = $lparams;
    
    $arrParams = explode("/i/",$lparams);
    if(count($arrParams)>1){
        list($lparams,$image) = explode("/i/",$lparams);
    }else{
        $image=$lparams;
        $lparams = "";
    }
    
    $params = explode("/",$lparams);
    $inputParams['i'] = $image;
    if(count($params) > 0)
    for($i=1;$i<count($params);$i++) {
        $inputParams[$params[$i]] = $params[++$i];
    }
}
$file = !empty($inputParams['i']) ? $inputParams['i'] : '';
if(!$file)
    exit;

$filePath = _UPLOAD_BASEDIR . '/' . $file;

$finfo = new finfo(FILEINFO_MIME_TYPE); // return mime type ala mimetype extension
$fileInfo = $finfo->file($filePath);

$arrFile = explode('/',$file);
$filename = array_pop($arrFile);

$expire = 30;  // Lebensdauer der Seite im Cache in Minuten
$mod_gmt = gmdate("D, d M Y H:i:s", getlastmod()) ." GMT";
// HTTP 1.0 kennt keine privaten Caches, also nix cachen
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Last-Modified: " . $mod_gmt);

// HTTP 1.1
header("Cache-Control: private, max-age=" . $expire * 60);

// MSIE 5.x special
header("Cache-Control: pre-check=" . $expire * 60, FALSE);
header("Content-Type: {$fileInfo}");
header("Content-Transfer-Encoding: binary");
header("Content-Length: ".@filesize($filePath));
header('Content-Disposition: attachment; filename="'.$filename.'"');
readfile($filePath) or die("");


?>