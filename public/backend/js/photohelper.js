$.fn.photohelper = function(options) {
    
     var settings = $.extend({
        // These are the defaults.
        path: "",
        type: "",
        size: ""
        }, options );
    
      settings.path = settings.path.replace("/thumbs",""); 
      var srcthumb = '/thumbs/' + settings.type + '/'  + settings.size + '/i/' + settings.path;
      this.removeAttr('style');
      return this.attr('src',srcthumb);
}