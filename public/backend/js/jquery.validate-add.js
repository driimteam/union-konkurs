jQuery.validator.addMethod("maxWords", function(value, element, params) { 
    return this.optional(element) || value.match(/\b\w+\b/g).length < params; 
}, "Please enter {0} words or less."); 
 
jQuery.validator.addMethod("minWords", function(value, element, params) { 
    return this.optional(element) || value.match(/\b\w+\b/g).length >= params; 
}, "Please enter at least {0} words."); 
 
jQuery.validator.addMethod("rangeWords", function(value, element, params) { 
    return this.optional(element) || value.match(/\b\w+\b/g).length >= params[0] && value.match(/bw+b/g).length < params[1]; 
}, "Please enter between {0} and {1} words.");


jQuery.validator.addMethod("letterswithbasicpunc", function(value, element) {
	return this.optional(element) || /^[a-z-.,()'\"\s]+$/i.test(value);
}, "Letters or punctuation only please");  

jQuery.validator.addMethod("alphanumeric", function(value, element) {
	return this.optional(element) || /^\w+$/i.test(value);
}, "Letters, numbers, spaces or underscores only please");  

jQuery.validator.addMethod("lettersonly", function(value, element) {
	return this.optional(element) || /^[a-z]+$/i.test(value);
}, "Letters only please"); 

jQuery.validator.addMethod("nowhitespace", function(value, element) {
	return this.optional(element) || /^\S+$/i.test(value);
}, "No white space please"); 

jQuery.validator.addMethod("ziprange", function(value, element) {
	return this.optional(element) || /^90[2-5]\d\{2}-\d{4}$/.test(value);
}, "Your ZIP-code must be in the range 902xx-xxxx to 905-xx-xxxx");


/**
 * matches US phone number format 
 * 
 * where the area code may not start with 1 and the prefix may not start with 1 
 * allows '-' or ' ' as a separator and allows parens around area code 
 * some people may want to put a '1' in front of their number 
 * 
 * 1(212)-999-2345
 * or
 * 212 999 2344
 * or
 * 212-999-0983
 * 
 * but not
 * 111-123-5434
 * and not
 * 212 123 4567
 */
jQuery.validator.addMethod("phone", function(value, element){
    
    return this.optional(element) || (/^\d{9}$/.test(value));
}, jQuery.validator.messages.phone);


jQuery.validator.addMethod("password", function(value, element){
    if($(element).hasClass('password')){ 
        if($.trim(value).length < 8){
            return false;
        }
        if(!(/^.*[0-9].*$/.test(value))){
            return false;
        }
    }
    
    return true;
    
}, jQuery.validator.messages.password);


jQuery.validator.addMethod("captcha", function(value, element){
    if($.trim(value).length == 5){
        return true;
    }else{ 
        return false;
    }
}, jQuery.validator.messages.captcha);

	
/* veneo js validators */
jQuery.validator.addMethod("postalcode", function(value, element, param) {
     return  /^\d{2}-\d{3}$/i.test($(element).val());
    
}, jQuery.validator.messages.postalcode);

jQuery.validator.addMethod("file", function(value, element, param) {
     return  /^.*\.(jpg|png|jpeg)$/i.test($(element).val());

}, jQuery.validator.messages.file);
	
jQuery.validator.addClassRules({
    file: {file : true},
    postalcode: {postalcode : true},
    password: {password : true},
    phone: {phone : true},
    captcha: {captcha : true} 
});
	

	
	
	
	
	
	
	
	
	
	
