(function($) {

    $.fn.renderTemplate = function(templateName,data) {
        var template = $(templateName).html();
        var renderedHtml = Mustache.render(template,data);
        $(this).hide().html(renderedHtml); 
        $(this).hide().fadeIn(1000);
    }

}(jQuery));