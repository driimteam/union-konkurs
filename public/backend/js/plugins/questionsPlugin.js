(function($) {
    $.fn.questionAnswers = function(options) {
        var settings = $.extend({
            answerTemplate: null,
            classTemplate: null,
            addAnswerClassButton: null,
            removeClassButton: null,
            limitAnswers: 4
        }, options);
        
        var qaController = new QuestionAnswersController($(this), settings);
        qaController.init();
    }
}(jQuery));

var QuestionAnswersController = function(el, settings) {
    var that = this;
    this.nr_answers=0;
    this.el = el;
    this.answers = [];
    this.startNrAnswers=0;
    this.settings = settings;
    this.template = settings.answerTemplate;

    this.init = function(){
        this.initListener();
    }
    this.initListener = function(){
        this.initClickAdd();
        this.initClickRemove();
        this.startNrAnswers = $(that.settings.classTemplate).length;
    }
    this.initClickAdd = function(){
        $(that.settings.addAnswerClassButton).click(function(e){
            e.preventDefault();
            var nrAnswers = $(that.settings.classTemplate).length;
            if(that.settings.limitAnswers > nrAnswers){
                that.addAnswerTemplate();
            }
        });
    }
    
    this.initClickRemove = function(){
        $(that.settings.removeClassButton).click(function(e){
            e.preventDefault();
            var elId = $(this).attr('data-id-remove');
            $('#answer_id_'+elId).remove();
        });
    }

    this.initListenerElement = function(idElement){
        var currentElement = $('#answer_id_'+idElement);
        currentElement.find(that.settings.removeClassButton).click(function(e){
            e.preventDefault();
            currentElement.remove();
        });
        
    }
    this.addAnswerTemplate = function(){
       that.nr_answers++;
       var d = new Date();
       var n = d.getTime();
       var template = $(that.template).html();
       var uniqueId = "new_"+that.nr_answers+'_'+n;
       var nr = that.startNrAnswers  + that.nr_answers;
       //var nr = that.startNrAnswers$(that.settings.classTemplate).length;
       var renderedHtml = Mustache.render(template,{uniqueId: uniqueId, nr: nr});
       that.el.append(renderedHtml);
       that.initListenerElement(uniqueId);
    }
}