$(function () {
    $('.tree li:has(ul)').addClass('parent_li').find(' > span').attr('title', '');
    $('.tree li.parent_li > span').click(function (e) {
        e.stopPropagation();
        var children = $(this).parent('li.parent_li').find('> ul > li');
        if (children.is(":visible")) {
            children.hide('fast');
            $(this).attr('title', 'Expand this branch').find(' > i').addClass('glyphicon-collapse-up').removeClass('glyphicon-collapse-down');
        } else {
            children.show('fast');
            $(this).attr('title', 'Collapse this branch').find(' > i').addClass('glyphicon-collapse-down').removeClass('glyphicon-collapse-up');
        }
       
    });
});