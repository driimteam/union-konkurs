<?php
date_default_timezone_set('Europe/Warsaw');
error_reporting(E_ALL);
ini_set('display_errors', 1);

/**
 * Sciezka do glownego katalogu z systemem
 *
 */

define('_ROOT_DIR', realpath(dirname(__FILE__) . '/../../../'));

define('_UPLOAD_BASEDIR', _ROOT_DIR . '/data/upload');
define('_UPLOAD_IMAGE_MAX_SHOW_WIDTH', 1024);
define('_UPLOAD_IMAGE_MAX_SHOW_HEIGHT', 768);
define('_UPLOAD_MAX_FILESIZE', 100000000);


/**
 * Include path
 */
set_include_path('.' . PATH_SEPARATOR . _ROOT_DIR . '/module/commons/');

function __autoload($name) {
    require_once str_replace('_', '/', $name) . '.php';
    //Zend_Loader :: loadClass($name);
}

$inputParams = array();
$_FILEPATH = '';

if(preg_match("/thumbs/i",$_SERVER['REQUEST_URI'])) {
    list(,$lparams) = explode("thumbs",$_SERVER['REQUEST_URI']);
    list($lparams) = explode('?',$lparams);
	
    $_FILEPATH = $lparams;
    
    $arrParams = explode("/i/",$lparams);
    if(count($arrParams)>1){
        list($lparams,$image) = explode("/i/",$lparams);
    }else{
        $image=$lparams;
        $lparams = "";
    }
    
    $params = explode("/",$lparams);
    $inputParams['i'] = $image;
    if(count($params) > 0)
    for($i=1;$i<count($params);$i++) {
        $inputParams[$params[$i]] = $params[++$i];
    }
}
$file = !empty($inputParams['i']) ? $inputParams['i'] : '';
if(!$file)
    exit;

$format = !empty($inputParams['resize']) ? $inputParams['resize'] :
             (!empty($inputParams['crop']) ? $inputParams['crop'] : false);



$thumbFile = dirname(__FILE__) .$_FILEPATH;

$dirPath = Tools_Image::getFileDirPath ($thumbFile);


$res = Tools_Image::checkDirsExistAndCreate ($dirPath, dirname(__FILE__));


if(!empty($inputParams['resize'])) {
    list($w,$h) = explode('x',$inputParams['resize']);
   	$picsize = getimagesize(_UPLOAD_BASEDIR . '/' . $file);
    $xPic = $picsize[0];
    $yPic = $picsize[1];
    
	if(!(($xPic < $w) && ($yPic < $h))){
    	$res = Tools_Image::resizeImage(_UPLOAD_BASEDIR . '/' . $file, $w, $h, $thumbFile, 100, '');
	}else{
		copy(_UPLOAD_BASEDIR . '/' . $file,$thumbFile);
	}
} elseif(!empty($inputParams['crop'])) {
    list($w,$h) = explode('x',$inputParams['crop']);
    Tools_Image::cropImage(_UPLOAD_BASEDIR . '/' . $file, $w, $h, $thumbFile, 100, (!empty($inputParams['start']) ? $inputParams['start'] : 'center'));
} else {
    copy(_UPLOAD_BASEDIR . '/' . $file,$thumbFile);
}

$expire = 30;  // Lebensdauer der Seite im Cache in Minuten
$mod_gmt = gmdate("D, d M Y H:i:s", getlastmod()) ." GMT";
// HTTP 1.0 kennt keine privaten Caches, also nix cachen
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Last-Modified: " . $mod_gmt);

// HTTP 1.1
header("Cache-Control: private, max-age=" . $expire * 60);

// MSIE 5.x special
header("Cache-Control: pre-check=" . $expire * 60, FALSE);

if(preg_match("/\.png$/i",$thumbFile)) {
    header ("Content-type: image/png");
} elseif(preg_match("/\.gif$/i",$thumbFile)) {
    header ("Content-type: image/gif");
} else {
    header('Content-type: image/jpeg');
}
header("Content-Transfer-Encoding: binary");
header("Content-Length: ".@filesize($thumbFile));
readfile($thumbFile) or die("");


?>