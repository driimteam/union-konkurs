<?php
date_default_timezone_set('Europe/Warsaw');
if(getenv('APPLICATION_ENV')){
    error_reporting(E_ALL);
    ini_set('display_errors',1);
}
/**
 * This makes our life easier when dealing with paths. Everything is relative
 * to the application root now.
 */
define('REQUEST_MICROTIME', microtime(true));


chdir(dirname(dirname(__DIR__)));
define('APPLICATION_PATH', __DIR__.'/../..');
//3poziomu
// Setup autoloading
include 'init_autoloader.php';

// Run the application!
Zend\Mvc\Application::init(include 'config/backend/backend.config.php')->run();
