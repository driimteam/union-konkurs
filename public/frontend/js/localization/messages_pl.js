/*
 * Translated default messages for the jQuery validation plugin.
 * Locale: PL (Polish; język polski, polszczyzna)
 */
(function ($) {
        
        /*
        * Translated default messages for the jQuery validation plugin.
        * Language: PL
        */
       jQuery.extend(jQuery.validator.messages, {
           required: "Pole wymagane",
           remote: "Pole wymagane",
           email: "Podaj prawidłowy adres e-mail",
           url: "Podaj prawidłowy adres URL",
           date: "Podaj prawidłową datę. Poprawny format to yyyy-mm-dd",
           datetime: "Podaj prawidłową datę. Poprawny format to yyyy-mm-dd hh:mm",
           dateISO: "Podaj prawidłową datę (ISO).",
           number: "Podaj prawidłową liczbę",
           digits: "Podaj tylko cyfry",
           creditcard: "Podaj prawidłową kartę kredytową",
           equalTo: "Podaj tę samą wartość ponownie",
           accept: "Podaj wartość z prawidłowym rozszerzeniem",
           maxlength: jQuery.format("Wpisz nie więcej niż {0} znaków"),
           minlength: jQuery.format("Wpisz przynajmniej {0} znaków"),
           rangelength: jQuery.format("Wpisz od {0} do {1} znaków"),
           rangeValue: jQuery.format("Podaj wartość z przedziału od {0} do {1}"),
           range: jQuery.format("Podaj wartość z przedziału od {0} do {1}"),
           maxValue: jQuery.format("Podaj wartość mniejszą bądź równą {0}"),
           max: jQuery.format("Podaj wartość większą bądź równą {0}"),
           minValue: jQuery.format("Podaj wartość większą bądź równą {0}"),
           min: jQuery.format("Podaj wartość większą bądź równą {0}"),
           postalcode: "Niepoprawny kod",
           phone: "Podaj 9 cyfr, bez kierunkowego",
           captcha: "Przepisz kod z obrazka",
           file: "Wgraj plik w formacie JPG lub PNG", 
           password: 'Hasło musi mieć przynajmniej 8 znaków oraz zawierać litery i cyfry',
           youtubeurl: 'Nieprawidłowy adres url'
       });
}(jQuery));