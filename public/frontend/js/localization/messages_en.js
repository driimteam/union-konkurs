/*
 * Translated default messages for the jQuery validation plugin.
 * Locale: PL (Polish; język polski, polszczyzna)
 */
(function ($) {
        
        /*
        * Translated default messages for the jQuery validation plugin.
        * Language: PL
        */
       jQuery.extend(jQuery.validator.messages, {
           postalcode: "Incorrect postalcode",
           phone: "11 digits with country prefix",
           file: "only pdf, doc, docx format", 
       });
}(jQuery));