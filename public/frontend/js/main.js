var timeoutObjects = [];

if (console === undefined) {
    consoleobject = function() {
        this.log = function(data) {
            return data;
        }
    }
    var console = new consoleobject();
}


function log(param) {
    console.log(param);
}

function trackSectionPage(){
    var sectionId = $('section.active').attr('id');
    console.log('#'+sectionId);
    //ga('send', 'pageview', '/#'+sectionId);
}

function eventClickGA(el){
    var category = el.attr('data-category');
    var action = el.attr('data-action');
    var name = el.attr('data-name');
    console.log(category+" :: "+action+" :: "+name);
    //ga('send', 'event',category, action, name);

}

function eventGA(categoryEvent, actionEvent, nameEvent){
    console.log(categoryEvent+" :: "+actionEvent+" :: "+nameEvent);
    //ga('send', 'event',categoryEvent, actionEvent, nameEvent);
}

$(document).ready(function() {

    var cookieEX = Cookies.get('cookies');

    if (cookieEX === "true") {
        $('#cookies').hide();
    }

    $('#cookieAgree').click(function(ev) {
        ev.preventDefault();
        Cookies.set('cookies', true, {expires: 7});
        $('#cookies').fadeOut(function() {
            $(this).remove();
        });
    });
   

    $('.show-more').click(function() {
        $(this).children('.legal').toggleClass('active')
        $(this).children('.show-legal').toggleClass('active')
    });

    $('.js-ga-btn').click(function(){
        eventClickGA($(this));
    });

    $('.mobile-menu').click(function() {
        $('.main-navigation').toggleClass('active');
        $(this).toggleClass('active');
    });
    $('.main-navigation').click(function() {
        $(this).toggleClass('active');
        $('.mobile-menu').toggleClass('active');
    });

    $('[data-toggle="tooltip"]').tooltip();

    $('[data-toggle="popover"]').popover({
        trigger: "hover focus"
    });
});
