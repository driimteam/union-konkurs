$(document).ready(function() {

    var cookieEX = Cookies.get('cookies');

    if (cookieEX === "true") {
        $('#cookies').hide();
    }

    $('#cookieAgree').click(function(ev) {
        ev.preventDefault();
        Cookies.set('cookies', true, {
            expires: 7
        });
        $('#cookies').fadeOut(function() {
            $(this).remove();
        });
    });

    $('.show-more').click(function() {
        $(this).children('.legal').toggleClass('active')
        $(this).children('.show-legal').toggleClass('active')
    });

    $('.mobile-menu').click(function() {
        $('.main-navigation').toggleClass('active');
        $(this).toggleClass('active');
    });
    $('.main-navigation').click(function() {
        $(this).toggleClass('active');
        $('.mobile-menu').toggleClass('active');
    });

    $("[data-toggle='tooltip']").tooltip();

});
