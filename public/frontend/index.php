<?php

date_default_timezone_set('Europe/Warsaw');

if (getenv('APPLICATION_ENV')) {
    error_reporting(E_ALL);
    ini_set('display_errors', 1);
} else {

    
    if (!array_key_exists('HTTP_HTTPS', $_SERVER)) {
        if (!preg_match('/(jpe?g|gif|png|ico|css|zip|tgz|gz|rar|bz2|doc|xls|exe|pdf|ppt|txt|tar|mid|midi|wav|bmp|rtf|js|swf|avi)$/i', $_SERVER['REQUEST_URI'])) {
            header('Location: https://maszpojecie.com.pl' . $_SERVER['REQUEST_URI']);
            exit;
        }
    }
        
   //if (date('Y-m-d H:i:s') > "2015-10-20 00:00:00") {
//        if (
//                !isset($_SERVER['PHP_AUTH_USER']) ||
//                !(
//                ($_SERVER['PHP_AUTH_USER'] == 'konkurs' && $_SERVER['PHP_AUTH_PW'] == 'uikonkurs2015')
//                )
//        ) {
//            header('WWW-Authenticate: Basic realm="Free tee for IT"');
//            header('HTTP/1.0 401 Unauthorized');
//            echo 'Login';
//            exit;
//        }
    //}
}
/**
 * This makes our life easier when dealing with paths. Everything is relative
 * to the application root now.
 */
define('REQUEST_MICROTIME', microtime(true));


chdir(dirname(dirname(__DIR__)));
define('APPLICATION_PATH', __DIR__ . '/../..');
//3poziomu
// Setup autoloading
include 'init_autoloader.php';
require_once 'e_errorhandler.php';
// Run the application!
Zend\Mvc\Application::init(include 'config/frontend/frontend.config.php')->run();
