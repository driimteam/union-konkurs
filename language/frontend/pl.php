<?php
return array(
    'error_youtube_url' =>"Nieprawidłowy adres youtube",
    'form_invalid_data'=>"Sprawdź formularz i spróbuj ponownie",
    'error_email_exist'=>"Podany adres e-mail został już użyty",
    'mail_to_confirm_appliaction_title'=>'Potwierdzenie zgłoszenia',
    'msg_cofirm_application_error'=> 'Wystąpił błąd. Zgłoszenie nie zostało potwierdzone',
    
    'label_content' => 'Wpisz odpowiedź (maksimum 500 znaków)',
    'label_youtube_link' => 'Wklej link do filmu na YouTube',
    'label_user_name'=>'Imię i nazwisko',
    'label_user_email'=>"Adres e-mail",
    'label_rule1'=>"",
    'label_rule2'=>"",
    
    'competition_type_FRIENDS'=>"ZNAJOMI",
    'competition_type_CHILDREN'=>"DZIECI",
);
?>
