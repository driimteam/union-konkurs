<?php

return array(
    'msg_error_tshirt_has_not_been_choosen'=>"T-shirt has not been choosen",

    'quiz_section_'.\QuestionnaireApp\Models\Entity\Question::SECTION_JAVA =>'JAVA',
    'quiz_section_'.\QuestionnaireApp\Models\Entity\Question::SECTION_NET =>'.NET',
    'quiz_section_'.\QuestionnaireApp\Models\Entity\Question::SECTION_QA =>'QA',
    'quiz_section_'.\QuestionnaireApp\Models\Entity\Question::SECTION_ORACLE =>'ORACLE',
);
?>
