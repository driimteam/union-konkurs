<?php

return array(
    
    
    //translate to Cms\Form\NodeForm
    'cms_node_form_type_page' => "Pojedyńcza strona",
    'cms_node_form_type_list' => "Lista stron",
    'cms_node_form_type_empty' => "Pusty węzeł",
    
    'cms_node_form_status_active' => "Aktywny",
    'cms_node_form_status_inactive' => "Nieaktywny",
    'cms_content_form_status_active' => "Aktywny",
    'cms_content_form_status_inactive' => "Nieaktywny",
    'cms_content_form_status_new' => "Nowy",
    
    'cms_structure_application'=>"WWW",
    'cms_structure_module'=>"MODULY",
    'cms_node_add_application'=>"Dodaj węzeł",
    'cms_node_list_application'=>"Struktura www",
    'cms_node_add_module'=>"Dodaj węzeł",
    'cms_node_list_module'=>"Struktura",
    
    'cms_btn_class_status_active'=>"btn-success",
    'cms_btn_class_status_inactive'=>"btn-danger",
    
    //field names by plugin name
    'cms_article_title' =>"Tytuł",
    'cms_article_main_picture' =>"Zdjęcie główne",
    'cms_article_publication_date' =>"Data publikacji",
    'cms_article_lead' =>"Lead",
    'cms_article_text' =>"Opis",
    'cms_article_status' =>"Status",
    
    
    'cms_article_title' =>"Tytuł",
    'cms_article_title' =>"Tytuł",
    'cms_article_title' =>"Tytuł",
    
    'flag_1'=>"TAK",
    'flag_0'=>"NIE",
    'flag_'=>"NIE",
    'status_ACTIVE'=>"aktywne",
    'status_INACTIVE'=>"nieaktywne",
    
    'form_status_active' =>"Aktywny",
    'form_status_inactive' =>"Nieaktywny",
    'form_status_' =>"Nieaktywny",

    'cms_btn_class_status_active'=>'btn-success',
    'cms_btn_class_status_inactive'=>'btn-danger',

    'status_CONFIRMED'=>"potwierdzone",
    'status_TO_VERIFY'=>"do&nbsp;potwierdzenia",
    'status_REJECTED'=>"odrzucone",
    'status_NEW'=>"nowe",
    
    'competition_type_FRIENDS'=>"Dla&nbsp;znajomych",
    'competition_type_CHILDREN'=>"Dla&nbsp;dzieci",

    'mail_title_confirmed' =>'Congratulations!',
    'mail_title_rejected' => 'Oh no, an error occurred while processing your request',
    

);
?>
