<?php
namespace DoctrineExtensions;
use DoctrineExtensions\Module as DoctrineExtensions;

class Module
{
     public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/lib/' . __NAMESPACE__,
                ),
            ),
        );
    }
}
