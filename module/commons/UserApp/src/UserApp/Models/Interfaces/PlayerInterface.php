<?php

namespace UserApp\Models\Interfaces;

/**
 * A Content.
 */
interface PlayerInterface
{
    
    public function getId();
    public function getType();
    public function getDisplayName();
    
}