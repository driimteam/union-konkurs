<?php

namespace UserApp\Models\Mapper;

use Doctrine\ORM\EntityManager;
use ZfcUserDoctrineORM\Mapper\User as ZfcUserOrmMapper;
use Doctrine\ORM\Tools\Pagination\Paginator as ORMpaginator;
use Zend\Paginator\Paginator;
use Zend\Stdlib\Hydrator\HydratorInterface;
use UserApp\Models\Entity\User as UserEntity;

class User extends ZfcUserOrmMapper
{
    protected $em;

    const ENTITY_CLASS = '\UserApp\Models\Entity\User';

    public function __construct(EntityManager $em, $options)
    {
        if (is_array($options)) {
            $this->em      = $em;
            $this->options = $options;
        } else {
            parent::__construct($em, $options);
        }
    }

    public function save($entity)
    {
        $this->em->persist($entity);
        $this->em->flush();
    }

    public function mergeEntity($entity){
        $this->em->merge($entity);
    }

    public function update($entity){
        $entity= $this->em->merge($entity);
        $this->em->persist($entity);
        $this->em->flush();
    }

    public function findByEmail($email)
    {
        $queryBd = $this->em->createQueryBuilder();
        $queryBd->from(self::ENTITY_CLASS, 'user')->select("user")
            ->andWhere('user.email=:email');
        $queryBd->setParameter('email', $email);
        $result  = $queryBd->getQuery()->getOneOrNullResult();
        return $result;
    }

    public function findById($id)
    {
        $queryBd = $this->em->createQueryBuilder();
        $queryBd->from(self::ENTITY_CLASS, 'user')->select("user")
            ->andWhere('user.id=:id');
        $queryBd->setParameter('id', $id);
        $result  = $queryBd->getQuery()->getOneOrNullResult();
        return $result;
    }

    public function findByUsername($username)
    {
        $queryBd = $this->em->createQueryBuilder();
        $queryBd->from(self::ENTITY_CLASS, 'user')->select("user")
            ->andWhere('user.username=:username');
        $queryBd->setParameter('username', $username);
        $result  = $queryBd->getQuery()->getOneOrNullResult();
        return $result;
    }

    public function saveUsers($users)
    {
        $conn = $this->em->getConnection();
        $conn->beginTransaction();
        try {
            foreach ($users as $user) {
                $this->em->persist($user);
            }
            $this->em->flush();
            $conn->commit();
            return true;
        } catch (\Exception $e) {
            $conn->rollback();
            return false;
        }
    }

    public function _addCondtitions($query, $params)
    {
        if (!empty($params['sort'])) {
            $query->addOrderBy($params['sort']['order'], $params['sort']['dir']);
        }
        if (!empty($params['addsort'])) {
            $query->addOrderBy($params['addsort']['order'],
                $params['addsort']['dir']);
        }
    }

    public function findByPager($page, $limit, $params = array())
    {

        $offset  = $limit * ($page - 1);
        $queryBd = $this->em->createQueryBuilder();
        $queryBd->from(self::ENTITY_CLASS, 'user')->select("user")
            ->andWhere('user.role = :role');
        $queryBd->setParameter('role', UserEntity::USER_ROLE_MEMBER);

        $this->_addCondtitions($queryBd, $params);
        $queryBd->setMaxResults($limit)->setFirstResult($offset);

        $paginator = new ORMpaginator($queryBd);
        $adapter   = new \App\Paginator\DoctrinePaginatorAdapter($paginator);
        $paginator = new Paginator($adapter);
        $paginator->setCurrentPageNumber($page);
        $paginator->setItemCountPerPage($limit);

        return $paginator;
    }
}