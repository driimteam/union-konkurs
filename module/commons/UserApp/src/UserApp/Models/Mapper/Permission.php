<?php

namespace UserApp\Models\Mapper;

use Doctrine\ORM\EntityManager;
use Cms\Options\ModuleUserOptions;
use Zend\Stdlib\Hydrator\HydratorInterface;

class Permission
{
   /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;
    protected $options;
    const ENTITY_CLASS = '\UserApp\Models\Entity\Permission';

    public function __construct(EntityManager $em, $options = array()) {
        $this->options = $options;
        $this->em = $em;
    }
    
    public function findBy(array $params=array()){
        $repository = $this->em->getRepository(self::ENTITY_CLASS);
        $list = $repository->findBy($params);
        return $list;
    }
    
    public function save($role){
        $this->em->persist($role);
        $this->em->flush();
    }
    
    public function find($id){
        return $this->em->find(self::ENTITY_CLASS,$id);
    }
   
}