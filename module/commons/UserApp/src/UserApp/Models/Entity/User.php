<?php

namespace UserApp\Models\Entity;

use ZfcRbac\Identity\IdentityInterface;
use ZfcUserDoctrineORM\Entity\User as OrmUserEntity;
use UserApp\Models\Interfaces\PlayerInterface;

class User extends OrmUserEntity implements IdentityInterface, PlayerInterface {

    const USER_ROLE_MEMBER = "member";
    const USER_ROLE_GUEST = "guest";
    
    protected $roles = array();

    public function getRoles() {
        return array($this->getRole());
    }


    /**
     * @var boolean
     */
    private $rule1;

    /**
     * @var boolean
     */
    private $rule2;

    /**
     * @var integer
     */
    private $userId;

    /**
     * Set rule1
     *
     * @param boolean $rule1
     *
     * @return User
     */
    public function setRule1($rule1) {
        $this->rule1 = $rule1;

        return $this;
    }

    /**
     * Get rule1
     *
     * @return boolean
     */
    public function getRule1() {
        return $this->rule1;
    }

    /**
     * Set rulePersonalData
     *
     * @param boolean $rulePersonalData
     *
     * @return User
     */
    public function setRule2($rule2) {
        $this->rule2 = $rule2;

        return $this;
    }

    /**
     * Get rulePersonalData
     *
     * @return boolean
     */
    public function getRule2() {
        return $this->rule2;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId() {
        return $this->userId;
    }

    /**
     * @var string
     */
    private $role;


    /**
     * Set role
     *
     * @param string $role
     *
     * @return User
     */
    public function setRole($role)
    {
        $this->role = $role;
    
        return $this;
    }

    /**
     * Get role
     *
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }
    
    public function getType(){
        return self::TYPE_USER; 
    }
    
    public function setUserId($userId) {
        $this->userId = $userId;
        return $this;
    }
    
    public function setId($id) {
        $this->id = $id;
        return $this;
    }
    
}
