<?php

namespace UserApp\Service;

use Zend\ServiceManager\ServiceManagerAwareInterface;
use Zend\ServiceManager\ServiceManager;
use Zend\Crypt\Password\Bcrypt;
use Zend\Stdlib\Hydrator;
use Zend\Stdlib\DispatchableInterface;

class User implements ServiceManagerAwareInterface {

    /**
     * @var pluginMapper
     */
    protected $roleMapper;
    protected $permissionMapper;
    protected $userMapper;
    

    /**
     * @var ServiceManager
     */
    protected $serviceManager;
    protected $eventManager;
    protected $authService;

    /**
     * @var UserServiceOptionsInterface
     */

   

    /**
     * Retrieve service manager instance
     *
     * @return ServiceManager
     */
    public function getServiceManager() {
        return $this->serviceManager;
    }

    /**
     * Set service manager instance
     *
     * @param ServiceManager $serviceManager
     * @return User
     */
    public function setServiceManager(ServiceManager $serviceManager) {
        $this->serviceManager = $serviceManager;
        return $this;
    }

    public function setEntityManager(EntityManager $em) {
        $this->em = $em;
    }

    public function getEntityManager() {
        if (null === $this->em) {
            $this->em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        }
        return $this->em;
    }

    public function getRoleMapper() {
        if (null === $this->roleMapper) {
            $this->roleMapper = $this->getServiceManager()->get('user_role_mapper');
        }
        return $this->roleMapper;
    }
    
    public function getPermissionMapper() {
        if (null === $this->permissionMapper) {
            $this->permissionMapper = $this->getServiceManager()->get('user_permission_mapper');
        }
        return $this->permissionMapper;
    }
    
    public function getUserMapper() {
        if (null === $this->userMapper) {
            $this->userMapper = $this->getServiceManager()->get('user_mapper');
        }
        return $this->userMapper;
    }
    
}
