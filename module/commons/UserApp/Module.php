<?php

namespace UserApp;

use UserApp\Module as UserApp;
use Zend\ModuleManager\ModuleManager;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ModuleManager\Feature\ServiceProviderInterface;
use Zend\Stdlib\Hydrator\ClassMethods;
use Zend\Mvc\MvcEvent;
use Zend\View\Helper;
use Zend\Mvc\ModuleRouteListener;

class Module implements ServiceProviderInterface {

    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getControllerPluginConfig() {
        return array();
    }

    public function getServiceConfig() {
        return array(
            'invokables' => array(
                'userapp_service' => 'UserApp\Service\User',
            ),
            'factories' => array(
                'user_mapper' => function ($sm) {
                    $em = $sm->get('Doctrine\ORM\EntityManager');
                    $options = array();
                    $mapper = new \UserApp\Models\Mapper\User($em, $options);
                    return $mapper;
                },
                'user_role_mapper' => function ($sm) {
                    $em = $sm->get('Doctrine\ORM\EntityManager');
                    $options = array();
                    $mapper = new \UserApp\Models\Mapper\Role($em, $options);
                    return $mapper;
                },
                'user_permission_mapper' => function ($sm) {
                    $em = $sm->get('Doctrine\ORM\EntityManager');
                    $options = array();
                    $mapper = new \UserApp\Models\Mapper\Permission($em, $options);
                    return $mapper;
                },
            ),
        );
    }

    public function getConfig() {
        return include __DIR__ . '/config/module.config.php';
    }

}