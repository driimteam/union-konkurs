<?php
namespace UserApp;
return array(
    'doctrine' => array(
        'driver' => array(
             'zfcuser_driver' =>array(
                'class' => 'Doctrine\ORM\Mapping\Driver\XmlDriver',
                'cache' => 'array',
                'paths' => array(__DIR__ .'/xml/orm')
            ),

            'orm_default' =>array(
                'drivers' => array(
                    'UserApp\Models\Entity'  =>  'zfcuser_driver',
                    'ZfcUserDoctrineORM\Entity'  =>  'zfcuser_driver'
                )
            )
        )
    ),
     'view_helpers' => array(
        'invokables' => array(
        )
    ),
);

