<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/zf2 for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace App\View\Helper;
use Zend\View\Helper\AbstractHtmlElement as AbstractHtmlElement;
/**
 * Helper for ordered and unordered lists
 */
class Photo extends AbstractHtmlElement
{
    /**
     * @return string The list XHTML.
     */
    public function __invoke($path, $options=array())
    {
        $additionParams = '';
    	if(!empty($options['resize'])) {
	        $type = 'resize';
	        $size = $options['resize'];
	    }elseif(!empty($options['crop'])) {
	        $type = 'crop';
	        $size = $options['crop'];
	    }
	    
            $url = str_replace("/thumbs","",$path);
	    $alt = !empty($options['alt']) ? $options['alt'] : '';
	    
	    
	    $alt = htmlspecialchars(strip_tags($alt));
	    if($url[0] == '/') {
	        $url = substr($url,1);
	    }
	    
	    $src = '/thumbs/' . $type . '/' .$size .$additionParams. '/i/' . $url;
	    if(!empty($options['onlypath'])){
	    	return $src;
	    }else{
	    	return '<img src="'.$src.'" alt="'.$alt.'" title="'.$alt.'" />';	
	    }
    }
}
