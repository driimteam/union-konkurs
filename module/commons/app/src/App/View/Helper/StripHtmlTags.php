<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/zf2 for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace App\View\Helper;
use Zend\View\Helper\AbstractHtmlElement as AbstractHtmlElement;
use Zend\Filter\StripTags;
/**
 * Helper for ordered and unordered lists
 */
class StripHtmlTags extends AbstractHtmlElement
{
   
   
    public function __invoke($string)
    {
        $strip = new StripTags();
        $result=$strip->filter($string);
        return $result;
    }
}