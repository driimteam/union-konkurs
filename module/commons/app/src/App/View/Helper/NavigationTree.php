<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/zf2 for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace App\View\Helper;
use Zend\View\Helper\AbstractHtmlElement as AbstractHtmlElement;
/**
 * Helper for ordered and unordered lists
 */
class NavigationTree extends AbstractHtmlElement
{
    protected $partial;
    protected $level;
    protected $levelend = 3;
    protected $flag = false;
    /**
     * Generates a 'List' element.
     *
     * @param  array $items   Array with the elements of the list
     * @param  bool  $ordered Specifies ordered/unordered list; default unordered
     * @param  array $attribs Attributes for the ol/ul tag.
     * @param  bool  $escape  Escape the items.
     * @return string The list XHTML.
     */
    public function __invoke($navigation)
    {
        $this->navigationTree($navigation, true);
        
    }
    
    public function navigationTree($tree, $isFirst) {
        if(!is_null($tree) && count($tree) > 0) {
            
            foreach($tree as $tmpNode){break;}
            if($tmpNode->get('level') > 1){
                return false;
            }
            if($isFirst){
                echo '<ul class="c-menu-root">';
            }else{
                echo '<ul>';
            }
            foreach($tree as $page) {
                $nodeObj = $page;
                
                $classActive = ($page->isActive(true)) ? "selected" : "";
                
                $classLi="";
                $classA = "";
                if($page->hasPages() && ($page->get('level')<1)){
                    $classLi='c-menu-has-sub';
                    $classA = 'subLink';
                }
                echo '<li class="'.$classLi.' '.$classActive.'" id="sort_'.$nodeObj->id.'">';
                        if($page->hasPages()  && ($page->get('level')<1)){
                            $pagesFirst = $page->getPages();
                            foreach($pagesFirst as $pageFirst){
                                $hrefCurrent = urldecode($pageFirst->getHref());
                                break;
                            }
                            
                        }else{
                            $hrefCurrent = urldecode($page->getHref());
                        }
                        if($page->home){
                            $hrefCurrent = "/".$this->getView()->layout()->lang;
                        }
                        
                        echo '<a class="'.$classA.'" href="'.$hrefCurrent.'">'.$page->getLabel().'</a>';
                        
                        $parent = $page->getParent();
                         if($page->hasPages()  && ($page->get('level')<1)){
                           echo '<div class="c-menu-title">'.$page->getLabel().'</div>'; 
                        }
                        if($page->hasPages()){
                            $this->navigationTree($page->getPages(),false);
                        }
                echo '</li>';
            }
            echo '</ul>';
        }
    }
}