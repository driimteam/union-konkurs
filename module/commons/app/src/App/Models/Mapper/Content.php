<?php

namespace App\Models\Mapper;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Pagination\Paginator as ORMpaginator;
use Zend\Paginator\Paginator;
use Zend\Stdlib\Hydrator\HydratorInterface;
use App\Models\Interfaces\SearchInterface;

class Content {

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;
    protected $lang;
    protected $options;
    protected $only_active;
    protected $pluginConfig;

    const ENTITY_CLASS = '\App\Models\Entity\Content';

    public function __construct(EntityManager $em, $options = array()) {
        $this->lang = !empty($options['lang']) ? $options['lang'] : 'pl';
        $this->options = $options;
        $this->only_active = !empty($options['only_active']) ? $options['only_active'] : false;
        $this->pluginConfig = !empty($options['pluginConfig']) ? $options['pluginConfig'] : false;
        //$result = $this->getEntityManager()->getRepository('Cms\Entity\Content');//->findAll() ;
        //$tablePrefix = new \Gedmo\TablePrefix('cnt_');
        //$evm = $em->getEventManager();
        //$evm->addEventListener(\Doctrine\ORM\Events::loadClassMetadata, $tablePrefix);   
        $this->em = $em;
    }

    public function checkSearch($content) {
        $node = $content->node;

        $pluginConfigNode = $this->pluginConfig[$node->plugin];
        if (isset($pluginConfigNode['is_search']) && $pluginConfigNode['is_search']) {
            if ($content instanceof SearchInterface) {
                if (!empty($this->options['searchMapper'])) {
                    $searchMapper = $this->options['searchMapper'];
                    $searchMapper->refreshSearch($content);
                }
            }
        }
    }

    public function insert(\App\Models\Entity\Content $content, $contentLang) {

        $this->em->getConnection()->beginTransaction(); // suspend auto-commit
        try {

            $this->em->persist($content);
            $this->em->flush();

            $contentLang->create_date = date('Y-m-d H:i:s');
            $contentLang->content_id = $content->id;
            $contentLang->lang = $this->lang;

            $this->em->persist($contentLang);
            $this->em->flush();
            $this->em->clear();

            $content->setContentLang($contentLang);
            $this->checkSearch($content);

            $this->em->getConnection()->commit();
        } catch (Exception $e) {
            $this->em->getConnection()->rollback();
            $this->em->close();
            throw $e;
        }
    }

    public function save(\App\Models\Entity\Content $content) {
        $this->em->getConnection()->beginTransaction(); // suspend auto-commit
        try {

            $contentLang = $content->getContentLang();

            if (!$contentLang->content_id) {
                $contentLang->content_id = $content->id;
                $contentLang->create_date = date('Y-m-d H:i:s');
                $content->setContentLang(null);//because doctrine exception associations
                $this->em->persist($content);
                $this->em->flush();
            } else {
                $contentLang->update_date = date('Y-m-d H:i:s');
            }

            $contentLang->lang = $this->lang;

            $this->em->persist($contentLang);
            $this->em->flush();
            $this->em->clear();
            $this->checkSearch($content);
            $this->em->getConnection()->commit();
        } catch (Exception $e) {
            $this->em->getConnection()->rollback();
            $this->em->close();
            throw $e;
        }
    }

    private function _addCondtitions($query, $params) {
        if (!empty($params['byNodeId'])) {
            $query->andWhere("c.nod_id = :nodId");
            $query->setParameter('nodId', $params['byNodeId']);
        }

        if (!empty($params['byParentNodeId'])) {
            $query->andWhere("n.parent_id = :parentNodId");
            $query->setParameter('parentNodId', $params['byParentNodeId']);
        }

        if (!empty($params['search'])) {
            $like = '%' . $params['search'] . '%';
            $query->andWhere($query->expr()->like("cl.title", ":search"));
            $query->setParameter('search', $like);
        }
        if (!empty($params['sort'])) {
            $query->addOrderBy($params['sort']['order'], $params['sort']['dir']);
        }
        if (!empty($params['addsort'])) {
            $query->addOrderBy($params['addsort']['order'], $params['addsort']['dir']);
        }

        if (!empty($params['byRoot'])) {
            $query->andWhere("n.root = :byRoot");
            $query->setParameter('byRoot', $params['byRoot']);
        }

        if (!empty($params['byNodeType'])) {
            $query->andWhere("n.type = :byNodeType");
            $query->setParameter('byNodeType', $params['byNodeType']);
        }

        if (!empty($params['greaterThenLevel'])) {
            $query->andWhere("n.level > :level");
            $query->setParameter('level', $params['greaterThenLevel']);
        }

        if (!empty($params['byPlugin'])) {
            $query->andWhere("n.plugin = :pluginName");
            $query->setParameter('pluginName', $params['byPlugin']);
            ;
        }

        if (!empty($params['byCategory'])) {
            $query->andWhere("cl.category_id = :categoryId");
            $query->setParameter('categoryId', $params['byCategory']);
        }

        if (!empty($params['idInPath'])) {
            $this->em->getConfiguration()->addCustomStringFunction('FIND_IN_SET', 'DoctrineExtensions\Query\Mysql\FindInSet');
            $query->andWhere('FIND_IN_SET(:idToPath,n.path) != 0');
            $query->setParameter('idToPath', $params['idInPath']);
        }

        if (!empty($params['onlyActive']) || $this->only_active) {
            $query->andWhere("cl.status = :statusContent")->andWhere("nl.status = :statusNode");
            $query->setParameter('statusContent', \App\Models\Entity\Content::STATUS_ACTIVE);
            $query->setParameter('statusNode', \App\Models\Entity\Node::STATUS_ACTIVE);
        }
    }

    public function findAllPager($page, $limit, $params = array()) {
        $offset = $limit * ($page - 1);
        $queryBd = $this->em->createQueryBuilder();
        $queryBd->from('App\\Models\\Entity\\Content', 'c')->select("c, n, nl, cl")
                ->join("c.node", 'n')
                ->join('n.node_lang', 'nl')
                ->join("c.content_lang", 'cl')
                ->andWhere("cl.lang = :lang")
                ->andWhere("nl.lang = :lang");


        $queryBd->setParameter('lang', $this->lang);

        $this->_addCondtitions($queryBd, $params);
        $queryBd->setMaxResults($limit)->setFirstResult($offset);
        $result = $queryBd->getQuery()->getResult();
        $paginator = new ORMpaginator($queryBd, false);

        $adapter = new \App\Paginator\DoctrinePaginatorAdapter($paginator);
        $paginator = new Paginator($adapter);
        $paginator->setCurrentPageNumber($page);
        $paginator->setItemCountPerPage($limit);

        return $paginator;
    }

    public function findAll($params = array(), $limit = false) {
        $queryBd = $this->em->createQueryBuilder();
        $queryBd->from('App\\Models\\Entity\\Content', 'c')->select("c, n, nl, cl")
                ->join("c.node", 'n')
                ->join('n.node_lang', 'nl')
                ->join("c.content_lang", 'cl')
                ->andWhere("cl.lang = :lang")
                ->andWhere("nl.lang = :lang");
        $queryBd->setParameter('lang', $this->lang);

        $this->_addCondtitions($queryBd, $params);
        if ($limit) {
            $queryBd->setMaxResults($limit);
        }
        $result = $queryBd->getQuery()->getResult();

        return $result;
    }

    public function findContentsByNodePager($page, $limit, $nodeId, $params = array()) {
        $params['byNodeId'] = $nodeId;
        $paginator = $this->findAllPager($page, $limit, $params);
        return $paginator;
    }

    public function findContentsByNode($nodeId, $params = array()) {
        $params['byNodeId'] = $nodeId;
        $result = $this->findAll($params);
        return $result;
    }

    public function findChildrenContents($node, $params = array()) {
        $params['byRoot'] = $node->root;
        $params['greaterThenLevel'] = $node->level;
        $params['idInPath'] = $node->id;
        $result = $this->findAll($params);
        return $result;
    }

    public function find($id, $onlyActive = false) {

        $queryBd = $this->em->createQueryBuilder()->from('App\\Models\\Entity\\Content', 'c')->select("c, n, cl", 'nl')
                ->join("c.node", 'n')
                ->join("n.node_lang", 'nl')
                ->leftJoin('c.content_lang', 'cl', \Doctrine\ORM\Query\Expr\Join::WITH, 'cl.lang = :lang')
                ->andWhere("nl.lang = :lang")
                ->setMaxResults(1);
        $queryBd->andWhere("c.id = :id");
        $queryBd->setParameter('id', $id);
        $queryBd->setParameter('lang', $this->lang);

        if ($onlyActive || $this->only_active) {
            $queryBd->andWhere("cl.status = :statusContent")->andWhere("nl.status = :statusNode");
            $queryBd->setParameter('statusContent', \App\Models\Entity\Content::STATUS_ACTIVE);
            $queryBd->setParameter('statusNode', \App\Models\Entity\Node::STATUS_ACTIVE);
        }
        $single = $queryBd->getQuery()->getOneOrNullResult();
        return $single;
    }

    public function findOneBy($criteria = array()) {
        $queryBd = $this->em->createQueryBuilder()->from('App\\Models\\Entity\\Content', 'c')->select("c, n, cl, nl")
                        ->join("c.node", 'n')
                        ->join("n.node_lang", 'nl')
                        ->leftJoin('c.content_lang', 'cl', \Doctrine\ORM\Query\Expr\Join::WITH, 'cl.lang = :lang')
                        ->andWhere("nl.lang = :lang")
                        ->andWhere("cl.lang = :lang")->setMaxResults(1);
        $queryBd->setParameter('lang', $this->lang);
        $this->_addCondtitions($queryBd, $criteria);
        $result = $queryBd->getQuery()->getOneOrNullResult();
        return $result;
    }

    public function setLang($lang) {
        $this->em->clear();
        $this->options['lang'] = $lang;
        $this->lang = $lang;
    }

    public function getLang() {
        return $this->lang;
    }

    public function checkPermissionToDeleteNode($node) {
        $queryBd = $this->em->createQueryBuilder()->from('App\\Models\\Entity\\Content', 'c')->select("c, cl")
                ->join('c.content_lang', 'cl');
        $queryBd->andWhere("(c.nod_id = :nodeId)");
        $queryBd->setParameter('nodeId', $node->id);
        $results = $queryBd->getQuery()->getResult();
        if (!empty($results)) {
            return $results;
        } else {
            return false;
        }
    }

    public function remove($content) {
        $contentLang = $content->getContentLang();
        $node = $content->node;
        $pluginConfigNode = $this->pluginConfig[$node->plugin];
        if (isset($pluginConfigNode['is_search']) && $pluginConfigNode['is_search']) {
            if ($content instanceof SearchInterface) {
                if (!empty($this->options['searchMapper'])) {
                    $searchMapper = $this->options['searchMapper'];
                    $searchMapper->deleteFromSearch($content);
                }
            }
        }

        $this->em->remove($contentLang);
        $this->em->flush();
    }

    public function getContentsByIds($ids, $onlyActive = false) {
        $queryBd = $this->em->createQueryBuilder()->from('App\\Models\\Entity\\Content', 'c')->select("c, cl")
                ->join('c.content_lang', 'cl')
                ->andWhere($this->em->createQueryBuilder()->expr()->in('c.id', $ids));
        $queryBd->andWhere("cl.lang = :lang");
        $queryBd->setParameter('lang', $this->lang);

        if ($onlyActive || $this->only_active) {
            $queryBd->andWhere("cl.status = :statusNode");
            $queryBd->setParameter('statusNode', \App\Models\Entity\Content::STATUS_ACTIVE);
        }

        $results = $queryBd->getQuery()->getResult();
        $newArray = array();
        foreach ($results as $result) {
            $newArray[$result->id] = $result;
        }
        return $newArray;
    }

    public function setsort($arraySort, $page, $limit) {
        $ids = array_values($arraySort);
        $contents = $this->getContentsByIds($ids);
        $i = 1;
        $startValue = ($page - 1) * $limit;
        foreach ($arraySort as $id) {
            $contentTmp = $contents[$id];
            $contentLang = $contentTmp->getContentLang();
            $contentLang->sort = $startValue + $i;
            $this->em->persist($contentLang);
            $i++;
        }
        $this->em->flush();
    }

}
