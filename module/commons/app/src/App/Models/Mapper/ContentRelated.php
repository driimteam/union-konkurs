<?php

namespace App\Models\Mapper;

use Gedmo\Tree\Entity\Repository\NestedTreeRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Pagination\Paginator as ORMpaginator;
use Zend\Paginator\Paginator;
use Zend\Stdlib\Hydrator\HydratorInterface;
use App\Models\Entity\File as File;
use App\Models\Entity\FileObject as EntityFileObject;

class ContentRelated
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;
    protected $lang;
    const ENTITY_CLASS='\App\Models\Entity\ContentRelated';

    public function __construct(EntityManager $em, $options = array()) {
        $this->lang = !empty($options['lang']) ? $options['lang'] : 'pl';
        $this->em = $em;
    }
    
    private function _addCondtitions($query, $params){
        if(!empty($params['filetype'])){
             $query->andWhere("f.type = :filetype");
             $query->setParameter('filetype', $params['filetype']);
        }
        
    }
    
    public function getContentsByRelatedObject($object, $type, $params=array()){
        $queryBd = $this->em->createQueryBuilder()->from('App\\Models\\Entity\\ContentRelated', 'cr')->select("cr,c,cl")
                ->join("cr.content", 'c')
                ->join("c.content_lang", 'cl');
        $queryBd->andWhere("cr.object_id = :id");
        $queryBd->andWhere("cr.object_name = :name");
        $queryBd->andWhere("cl.lang = :lang");
        $queryBd->addOrderBy("cr.sort",'asc');
        $queryBd->addOrderBy("cr.create_date",'desc');
        $queryBd->setParameter('id',$object->id);
        $queryBd->setParameter('lang',$this->lang);
        $queryBd->setParameter('name',get_class($object));
        $this->_addCondtitions($queryBd, $params);
        
        $result = $queryBd->getQuery()->getResult();
        return $result;
       
    }
    
    
    public function find($id){
        $queryBd = $this->em->createQueryBuilder()->from('App\\Models\\Entity\\ContentRelated', 'fo')->select("cr, c, cl")
                 ->join("cr.content", 'c')
                ->join("c.content_lang", 'cl')->setFirstResult(0)->setMaxResults(1);
        $queryBd->andWhere("cr.object_id = :id");
        $queryBd->andWhere("cr.object_name = :name");
        $queryBd->andWhere("cl.lang = :lang");
        $queryBd->andWhere("cr.id = :id");
        $queryBd->setParameter('lang',$this->lang);
        $queryBd->setParameter('id',$id);
        $single = $queryBd->getQuery()->getSingleResult();
        return $single;
       
    }
    
    public function remove($object){
        $res = $this->em->remove($object);
        $this->em->flush();
        return $res;
        
    }
    
    public function save($object){
        $res = $this->em->persist($object);
        $this->em->flush();
        return $res;
    }
    
    public function saveRelated($relatedObject){
        $this->em->getConnection()->beginTransaction(); // suspend auto-commit
        try {
            
            $contentRelated = new ContentRelated();
            $contentRelated->object_id = $relatedObject->id;
            $contentRelated->object_name = get_class($relatedObject);
            $contentRelated->file_id = $file->id;
            $contentRelated->create_date = date('Y-m-d H:i:s');
            $contentRelated->lang = $this->lang;
            
            $contentRelated->setContent($relatedObject);
            $this->em->persist($contentRelated);
            $this->em->flush();
            
            $this->em->getConnection()->commit();
        } catch (Exception $e) {
            $this->em->getConnection()->rollback();
            $this->em->close();
            throw $e;
        }
    }
    
    public function getRelatedContentByIds($ids) {
        $queryBd = $this->em->createQueryBuilder()->from('App\\Models\\Entity\\ContentRelated', 'cr')->select("cr")
                ->andWhere($this->em->createQueryBuilder()->expr()->in('cr.id', $ids));
        
        $results = $queryBd->getQuery()->getResult();
        $newArray = array();
        foreach ($results as $result) {
            $newArray[$result->id] = $result;
        }
        return $newArray;
    }
    
    public function setsort($arraySort) {
        $ids = array_values($arraySort);
        $relatedObject = $this->getRelatedContentByIds($ids);
        $i = 1;
        foreach ($arraySort as $id) {
            $relatedObjectTmp = $relatedObject[$id];
            $relatedObjectTmp->sort = $i;
            $this->em->persist($relatedObjectTmp);
            $i++;
        }
        $this->em->flush();
        //$this->em->getRepository(static::ENTITY_CLASS)->getChildren($parentNode,'sort');
    }
}