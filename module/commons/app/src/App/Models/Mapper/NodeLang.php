<?php

namespace App\Models\Mapper;

use Gedmo\Tree\Entity\Repository\NestedTreeRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Pagination\Paginator as ORMpaginator;
use Zend\Paginator\Paginator;
use Zend\Stdlib\Hydrator\HydratorInterface;

class NodeLang
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;
    protected $lang,$options;
    const ENTITY_CLASS='\App\Models\Entity\NodeLang';

    public function __construct(EntityManager $em, $options=array())
    {
        $this->em      = $em;
        $this->options=$options;
        $this->lang = !empty($options['lang']) ? $options['lang'] : 'pl';
    }
    public function find($id) {
        $queryBd = $this->em->createQueryBuilder()->from('App\\Models\\Entity\\NodeLang', 'nl')->select("nl")
                        ->setFirstResult(0)->setMaxResults(1);
        $queryBd->andWhere("nl.lang = :lang");
        $queryBd->andWhere("n.id = :id");
        $queryBd->setParameter('id', $id);
        $queryBd->setParameter('lang', $this->lang);
        $single = $queryBd->getQuery()->getOneOrNullResult();
        return $single;
    }
    
    public function setLang($lang){
        $this->em->clear();
        $this->options['lang']=$lang;
        $this->lang = $lang;
    }
    public function save(\App\Models\Entity\NodeLang $nodeLang) {
        //var_dump($nodeLang);exit;
            
            $this->em->persist($nodeLang);
            $this->em->flush();
    }
}