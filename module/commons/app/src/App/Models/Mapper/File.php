<?php

namespace App\Models\Mapper;

use Gedmo\Tree\Entity\Repository\NestedTreeRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Pagination\Paginator as ORMpaginator;
use Zend\Paginator\Paginator;
use Zend\Stdlib\Hydrator\HydratorInterface;

class File
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;
    const ENTITY_CLASS='\App\Models\Entity\File';

    public function __construct(EntityManager $em)
    {
        $this->em      = $em;
    }
    
    
    
}