<?php

namespace App\Models\Mapper;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Pagination\Paginator as ORMpaginator;
use Zend\Paginator\Paginator;
use Zend\Stdlib\Hydrator\HydratorInterface;

class ContentLang
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;
    const ENTITY_CLASS ='\App\Models\Entity\ContentLang';

    public function __construct(EntityManager $em)
    {
        //$result = $this->getEntityManager()->getRepository('Cms\Entity\Content');//->findAll() ;
        
        //$tablePrefix = new \Gedmo\TablePrefix('cnt_');
        //$evm = $em->getEventManager();
        //$evm->addEventListener(\Doctrine\ORM\Events::loadClassMetadata, $tablePrefix);   
        $this->em      = $em;
    }
}