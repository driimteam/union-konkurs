<?php

namespace App\Models\Mapper;

use Gedmo\Tree\Entity\Repository\NestedTreeRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Pagination\Paginator as ORMpaginator;
use Zend\Paginator\Paginator;
use Zend\Stdlib\Hydrator\HydratorInterface;
use App\Models\Entity\File as File;
use App\Models\Entity\FileObject as EntityFileObject;

class FileObject
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;
    protected $lang;
    const ENTITY_CLASS='\App\Models\Entity\FileObject';

    public function __construct(EntityManager $em, $options = array()) {
        $this->lang = !empty($options['lang']) ? $options['lang'] : 'pl';
        $this->em = $em;
    }
    
    private function _addCondtitions($query, $params){
        if(!empty($params['filetype'])){
             $query->andWhere("f.type = :filetype");
             $query->setParameter('filetype', $params['filetype']);
        }
        
    }
    
    public function getFilesByRelatedObject($object, $type=null, $params=array()){
        $queryBd = $this->em->createQueryBuilder()->from('App\\Models\\Entity\\FileObject', 'fo')->select("f,fo")
                ->join("fo.file", 'f');
        $queryBd->andWhere("fo.object_id = :id");
        $queryBd->andWhere("fo.object_name = :name");
        
        $queryBd->andWhere("fo.lang = :lang");
        $queryBd->setParameter('lang',$this->lang);
        
        $queryBd->addOrderBy("fo.sort",'asc');
        $queryBd->addOrderBy("fo.create_date",'desc');
        $queryBd->setParameter('id',$object->id);
        $queryBd->setParameter('name',get_class($object));
        if($type){ 
            $params['filetype']= $type;
        }
        $this->_addCondtitions($queryBd, $params);
        
        $result = $queryBd->getQuery()->getResult();
        return $result;
       
    }
    
    public function getFilesByRelatedObjects($objectIds, $objectName, $params=array()){
        $queryBd = $this->em->createQueryBuilder()->from('App\\Models\\Entity\\FileObject', 'fo')->select("fo,f")
                ->join("fo.file", 'f');
        $queryBd->andWhere($this->em->createQueryBuilder()->expr()->in('fo.object_id', $objectIds));
        $queryBd->andWhere("fo.object_name = :name");
        $queryBd->andWhere("fo.lang = :lang");
        $queryBd->setParameter('lang',$this->lang);
        
        $queryBd->addOrderBy("fo.sort",'asc');
        $queryBd->addOrderBy("fo.create_date",'desc');
        $queryBd->setParameter('name',$objectName);
        $this->_addCondtitions($queryBd, $params);
        $result = $queryBd->getQuery()->getResult();
        return $result;
       
    }
    
    
    public function find($id){
        $queryBd = $this->em->createQueryBuilder()->from('App\\Models\\Entity\\FileObject', 'fo')->select("fo, f")
                ->join("fo.file", 'f')->setFirstResult(0)->setMaxResults(1);
        $queryBd->where("fo.id = :id");
        $queryBd->andWhere("fo.lang = :lang");
        $queryBd->setParameter('lang',$this->lang);
        $queryBd->setParameter('id',$id);
        $single = $queryBd->getQuery()->getSingleResult();
        return $single;
       
    }
    
    public function remove($object){
        $res = $this->em->remove($object);
        $this->em->flush();
        return $res;
        
    }
    
    public function save($object){
        $res = $this->em->persist($object);
        $this->em->flush();
        return $res;
    }
    
    public function savefile($relatedObject, $objectfile, $type){
        $this->em->getConnection()->beginTransaction(); // suspend auto-commit
        try {
            $file = new File();
            $file->name = $objectfile->getName();
            $file->path = $objectfile->getFilePath();
            $file->create_date = date('Y-m-d H:i:s');
            $file->type = $type;
            $this->em->persist($file);
            $this->em->flush();
            
            $fileObject = new EntityFileObject();
            $fileObject->object_id = $relatedObject->id;
            $fileObject->object_name = get_class($relatedObject);
            $fileObject->file_id = $file->id;
            $fileObject->create_date = date('Y-m-d H:i:s');
            $fileObject->lang = $this->lang;
            
            $fileObject->setFile($file);
            $this->em->persist($fileObject);
            $this->em->flush();
            
            $this->em->getConnection()->commit();
        } catch (Exception $e) {
            $this->em->getConnection()->rollback();
            $this->em->close();
            throw $e;
        }
    }
    
    public function getFilesObjectByIds($ids) {
        $queryBd = $this->em->createQueryBuilder()->from('App\\Models\\Entity\\FileObject', 'fo')->select("fo,f")
                ->join("fo.file", 'f')
                ->andWhere($this->em->createQueryBuilder()->expr()->in('fo.id', $ids));
        $queryBd->andWhere("fo.lang = :lang");
        $queryBd->setParameter('lang',$this->lang);
        $results = $queryBd->getQuery()->getResult();
        $newArray = array();
        foreach ($results as $result) {
            $newArray[$result->id] = $result;
        }
        return $newArray;
    }
    
    public function setsort($arraySort) {
        $ids = array_values($arraySort);
        $filesObject = $this->getFilesObjectByIds($ids);
        $i = 1;
        foreach ($arraySort as $id) {
            $fileObjectTmp = $filesObject[$id];
            $fileObjectTmp->sort = $i;
            $this->em->persist($fileObjectTmp);
            $i++;
        }
        $this->em->flush();
        //$this->em->getRepository(static::ENTITY_CLASS)->getChildren($parentNode,'sort');
    }
}