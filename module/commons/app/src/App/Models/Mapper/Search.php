<?php

namespace App\Models\Mapper;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Doctrine\ORM\EntityManager;
use App\Models\Interfaces\SearchInterface;
use Doctrine\ORM\Tools\Pagination\Paginator as ORMpaginator;
use Zend\Paginator\Paginator;
use Zend\Stdlib\Hydrator\HydratorInterface;

class Search
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;
    protected $lang;
    protected  $pluginConfig;
    protected  $structure;
    
    const ENTITY_CLASS ='\App\Models\Entity\Search';

    public function __construct(EntityManager $em, $options=array())
    { 
        $this->em      = $em;
        $this->lang = !empty($options['lang']) ? $options['lang'] : 'pl';
        $this->pluginConfig = !empty($options['pluginConfig']) ? $options['pluginConfig'] : false;
        $this->structure = !empty($options['structure']) ? $options['structure'] : \App\Models\Entity\Node::STRUCTURE_APPLICATION;
    }
    public function search($keywords, $page, $limit=100){
                
        $offset = $limit * ($page - 1);
        $status = \App\Models\Entity\Search::STATUS_ACTIVE;
        $keywords = $this->em->getConnection()->quote($keywords, \PDO::PARAM_STR);
        $objectName = 'App\\\Models\\\Entity\\\Content';
        $sql = 'select * FROM search s where MATCH (s.allcontents) AGAINST ('.$keywords.' IN BOOLEAN MODE)
            and s.status = "'.$status.'" and ((s.parent_status= "'.$status.'") or (s.parent_status is null)) and lang="'.$this->lang.'"';
        
        $sql.= ' and s.structure= "'.$this->structure.'"';
        //$sql.= ' and s.object_name= "'.$objectName.'"';
        
        $rsm = new ResultSetMappingBuilder($this->em);
        $rsm->addRootEntityFromClassMetadata('\App\Models\Entity\Search', 's');
        
        $query  = $this->em->createNativeQuery($sql,$rsm);
        
        //$query->setParameter('status',\App\Models\Entity\Search::STATUS_ACTIVE);
        //$query->setParameter('statusParent',\App\Models\Entity\Search::STATUS_ACTIVE);
        
        $adapter = new \App\Paginator\NativePaginatorAdapter($query);
        $paginator = new Paginator($adapter);
        $paginator->setCurrentPageNumber($page);
        $paginator->setItemCountPerPage($limit);
        return $paginator;
        
    }
    
    public function find($id){
        $queryBd = $this->em->createQueryBuilder()->from('App\\Models\\Entity\\Search', 's')->select("s")->setFirstResult(0)->setMaxResults(1);
        $queryBd->where("s.id = :id");
        $queryBd->andWhere("s.lang = :lang");
        $queryBd->setParameter('lang',$this->lang);
        $queryBd->setParameter('id',$id);
        $single = $queryBd->getQuery()->getOneOrNullResult();
        return $single;
       
    }
    
    public function changeStatusChildren($parent){
        
        //getSearchPath 
        //zmiana wszystkich wezlow podrzednych na niekatywne - przypadek gdy zmieniamy na niekatywne
        
        if($parent->getSearchStatus()!= \App\Models\Entity\Search::STATUS_ACTIVE){
            $connection = $this->em->getConnection();
            //$sql = "update search s set s.parent_status = :parent_status where ((s.parent_name = :parent_name) or (s.object_name = :parent_name)) and (".$parent->id." IN (s.path)) and s.lang= :lang";
            $sql = "update search s set s.parent_status = :parent_status where ((s.parent_name = :parent_name) or (s.object_name = :parent_name)) and FIND_IN_SET(".$parent->id.",s.path) and s.lang= :lang";
            

            $params=array(
                "parent_name"=>get_class($parent),
                "path"=>$parent->getSearchPath(),
                "parent_status"=>$parent->getSearchStatus(),
                "lang"=>$this->lang,
            );
            $result = $connection->executeUpdate($sql, $params);
            
        }else{
            
            $qb = $this->em->createQueryBuilder();
            $query = $qb->update(self::ENTITY_CLASS, 's')
                    ->set('s.parent_status', ":parent_status")
                    ->andWhere('s.parent_id = :parentId')
                    ->andWhere('s.parent_name = :parent_name')
                    ->andWhere('s.lang = :lang')
                    ->setParameter('parent_status', $parent->getSearchStatus())
                    ->setParameter('parentId', $parent->id)
                    ->setParameter('parent_name', get_class($parent))
                    ->setParameter('lang', $this->lang)
                    ->getQuery();
            $result = $query->execute();
            
        }
        
        return $result;
    }
    
    public function deleteFromSearch(SearchInterface $object){
        $objectClassName =  get_class($object);
        $criteria = array('object_id'=>$object->id,'object_name'=>$objectClassName,'lang'=>$this->lang);
        $search = $this->em->getRepository(self::ENTITY_CLASS)->findOneBy($criteria);
        if($search){
            $this->em->remove($search);
            $this->em->flush();
            if($object->isSearchParent()){
                $this->changeStatusChildren($object);
            }
        }
        
    }
    public function refreshSearch(SearchInterface $object){
        $objectClassName =  get_class($object);
        $criteria = array('object_id'=>$object->id,'object_name'=>$objectClassName,'lang'=>$this->lang);
        $search = $this->em->getRepository(self::ENTITY_CLASS)->findOneBy($criteria);
        if(!$search){
            $className =self::ENTITY_CLASS;
            $search = new $className();
            $search->create_date = date('Y-m-d H:i:s');
        }
        $search->title = $object->getSearchTitle();
        $search->lead = $object->getSearchLead();
        $search->text = $object->getSearchText();
        $search->allcontents = $object->getAllContents($this->pluginConfig);
        $search->status = $object->getSearchStatus();
        
        $search->object_id = $object->id;
        $search->object_name = $objectClassName;
        $search->parent_id = $object->getSearchParentId();
        $search->parent_name = $object->getSearchParentName();
        $search->parent_status = $object->getSearchParentStatus();
        $search->update_date = date('Y-m-d H:i:s');
        $search->path = $object->getSearchPath();
        $search->lang = $this->lang;
        $search->structure = $this->structure;
        
        $this->em->persist($search);
        $this->em->flush();
        
        if($object->isSearchParent()){
            $this->changeStatusChildren($object);
        }
        /*if($search->status == ){
            $this->changeStatusChildren($object, \App\Models\Entity\Search::STATUS_ACTIVE);
        }*/
    }
    public function findAllPager($page, $limit, $params=array())
    {
        $offset = $limit * ($page - 1);
        //$results = $this->em->getRepository(static::ENTITY_CLASS)->findBy($params,null,$page,$offset);
        $queryBd = $this->em->createQueryBuilder();
        $queryBd->from('App\\Models\\Entity\\Search', 's')->select("s")
                 ->andWhere("s.lang = :lang");
        $queryBd->setParameter('lang',$this->lang);
        
        //$this->_addCondtitions($queryBd, $params);
        $queryBd->setMaxResults($limit)->setFirstResult($offset);
        
        $paginator = new ORMpaginator($queryBd,false);
        
        $adapter = new \App\Paginator\DoctrinePaginatorAdapter($paginator);
        $paginator = new Paginator($adapter);
        $paginator->setCurrentPageNumber($page);
        $paginator->setItemCountPerPage($limit);
        
        return $paginator;
    }
}