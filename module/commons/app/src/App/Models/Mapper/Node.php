<?php

namespace App\Models\Mapper;

use Gedmo\Tree\Entity\Repository\NestedTreeRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Pagination\Paginator as ORMpaginator;
use Zend\Paginator\Paginator;
use Zend\Stdlib\Hydrator\HydratorInterface;
use App\Models\Entity\Node as NodeObject;
use App\Models\Entity\NodeLang as NodeLang;
use App\Models\Interfaces\SearchInterface;
use DoctrineExtensions\Query\Mysql\FindInSet;

class Node {

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;
    protected $lang;
    protected $structure;
    protected $options;
    protected $only_active;

    const ENTITY_CLASS = '\App\Models\Entity\Node';

    public function __construct(EntityManager $em, $options = array()) {
        $this->lang = !empty($options['lang']) ? $options['lang'] : 'PL';
        $this->structure = !empty($options['structure']) ? $options['structure'] : \App\Models\Entity\Node::STRUCTURE_APPLICATION;
        $this->options = $options;
        $this->only_active = !empty($options['only_active']) ? $options['only_active'] : false;
        $this->em = $em;
    }

    /* public function findAll() {
      $result = $this->em->getRepository(static::ENTITY_CLASS)->findAll();
      return $result;
      } */

    public function setLang($lang) {
        $this->em->clear();
        $this->options['lang'] = $lang;
        $this->lang = $lang;
    }

    public function getLang() {
        return $this->lang;
    }

    public function setStructure($structure) {
        $this->structure = $structure;
    }

    public function getStructure() {
        return $this->structure;
    }

    public function setOnlyActive($boolean) {
        $this->only_active = $boolean;
    }

    public function updateChildrenParams($node, $oldNode) {
        $paramsUpdate = array();
        $result = true;
        if (($node->level != $oldNode['level']) || ($node->root != $oldNode['root'])) {
            $diffLevel = $node->level - $oldNode['level'];
            $paramsUpdate['level'] = $diffLevel;
            if ($node->root == null) {
                $paramsUpdate['root'] = $node->id;
            } else {
                $paramsUpdate['root'] = $node->root;
            }

            $paramsUpdate['oldPath'] = $oldNode['path'];
            $paramsUpdate['newPath'] = $node->path;
            $paramsUpdate['nodeId'] = $node->id;
            $paramsUpdate["lang"] = $this->lang;

            $connection = $this->em->getConnection();
            $sql = "update cms_nodes n, cms_nodes_lang nl set n.level= n.level+:level, n.root = :root, n.path= REPLACE(n.path,:oldPath,:newPath) where n.id=nl.node_id and FIND_IN_SET(" . $node->id . ",n.path) and nl.lang= :lang and n.id!=:nodeId";
            $result = $connection->executeUpdate($sql, $paramsUpdate);
        }
        return $result;
    }

    public function changeStatusChildren($node) {

        $nodeLang = $node->getNodeLang();
        $result = true;


        if ($nodeLang->status != \App\Models\Entity\NodeLang::STATUS_ACTIVE) {
            $connection = $this->em->getConnection();
            $sql = "update cms_nodes n, cms_nodes_lang nl set nl.status = :status where n.id=nl.node_id and FIND_IN_SET(" . $node->id . ",n.path) and nl.lang= :lang ";

            $params = array(
                "status" => $nodeLang->status,
                "lang" => $this->lang,
            );
            $result = $connection->executeUpdate($sql, $params);
        }

        return $result;
    }

    public function findAll($params = array(), $limit = null) {
        $queryBd = $this->em->createQueryBuilder()->from(self::ENTITY_CLASS, 'n')->select("n, nl")
                ->join('n.node_lang', 'nl');
        $queryBd->andWhere("nl.lang = :lang");
        $queryBd->setParameter('lang', $this->lang);

        $this->_addCondtitions($queryBd, $params);
        if ($limit) {
            $queryBd->setMaxResults($limit);
        }
        $result = $queryBd->getQuery()->getResult();

        return $result;
    }

    public function findChildrenByType($node, $type, $params = array()) {
        $params['byRoot'] = $node->root;
        $params['greaterThenLevel'] = $node->level;
        $params['byNodeType'] = $type;
        $params['idInPath'] = $node->id;
        $params['sort'] = array('order' => "c.id", "dir" => 'asc');

        $result = $this->findAll($params);
        return $result;
    }

    public function setFormData($node, $formData = array()) {
        $node->populate($formData);
        //var_dump($formData);exit;
        $node->getNodeLang()->populate($formData);
        $parentId = isset($formData['parent_id']) ? $formData['parent_id'] : null;
        $parentObject = $this->find($parentId);
        $node->setHierarchy($parentObject);
    }

    public function save(\App\Models\Entity\Node $node) {
        $this->em->getConnection()->beginTransaction(); // suspend auto-commit
        try {
            $id = $node->id;
            if (empty($id)) {
                throw new \Exception('id is require');
            }

            $oldNode = $this->em->getUnitOfWork()->getOriginalEntityData($node);
            $nodeLang = $node->getNodeLang();
            $nodeLang->lang = $this->lang;
            $nodeLang->node_id = $node->id;

            $this->em->persist($node);
            $this->em->persist($nodeLang);
            $this->em->flush();

            $this->updateChildrenParams($node, $oldNode);
            $this->changeStatusChildren($node);
            if ($node->type != \App\Models\Entity\Node::TYPE_EMPTY) {
                if ($node instanceof SearchInterface) {
                    if (!empty($this->options['searchMapper'])) {
                        $searchMapper = $this->options['searchMapper'];
                        $searchMapper->refreshSearch($node);
                    }
                }
            }

            $this->em->getConnection()->commit();
        } catch (Exception $e) {
            $this->em->getConnection()->rollback();
            $this->em->close();
            throw $e;
        }
    }

    public function insert(array $formData) {
        $createDate = date('Y-m-d H:i:s');

        $this->em->getConnection()->beginTransaction(); // suspend auto-commit
        try {

            $node = new NodeObject();
            $node->structure = $this->structure;
            $node->create_date = $createDate;

            $parentObject = $this->find((int) $formData['parent_id']);
            $node->populate($formData);
            $node->setHierarchy($parentObject);

            $this->em->persist($node);
            $this->em->flush();

            $nodeLang = new NodeLang($node);
            $nodeLang->lang = $this->lang;
            $nodeLang->create_date = $createDate;
            $nodeLang->populate($formData);
            $this->em->persist($nodeLang);
            $this->em->flush();

            $node->setRoot($node->getParent());
            $node->setPath($node->getParent());
            $this->em->persist($node);
            $this->em->flush();

            $this->em->clear();

            if ($node->type != \App\Models\Entity\Node::TYPE_EMPTY) {
                if ($node instanceof SearchInterface) {
                    $node->setNodeLang($nodeLang);
                    if (!empty($this->options['searchMapper'])) {
                        $searchMapper = $this->options['searchMapper'];
                        $searchMapper->refreshSearch($node);
                    }
                }
            }

            $this->em->getConnection()->commit();
        } catch (Exception $e) {
            $this->em->getConnection()->rollback();
            $this->em->close();
            throw $e;
        }
    }

    
    
    public function find($id, $onlyActive = false, $leftJoin=false) {
        if ($id == null) {
            return null;
        }
        
        $queryBd = $this->em->createQueryBuilder()->from('App\\Models\\Entity\\Node', 'n')->select("n, nl");
        if($leftJoin){
            $queryBd->leftJoin('n.node_lang', 'nl',\Doctrine\ORM\Query\Expr\Join::WITH, 'nl.lang = :lang');
        }else{
            $queryBd->join('n.node_lang', 'nl');
            $queryBd->andWhere("nl.lang = :lang");
        }        
        $queryBd->setFirstResult(0)->setMaxResults(1);
        
        
        $queryBd->andWhere("n.id = :id");
        $queryBd->andWhere("n.structure = :structure");
        $queryBd->setParameter('structure', $this->structure);
        $queryBd->setParameter('id', $id);
        $queryBd->setParameter('lang', $this->lang);

        if ($onlyActive || $this->only_active) {
            $queryBd->andWhere("nl.status = :statusNode");
            $queryBd->setParameter('statusNode', \App\Models\Entity\Node::STATUS_ACTIVE);
        }
        $single = $queryBd->getQuery()->getOneOrNullResult();
        return $single;
    }

    private function _addCondtitions($query, $params) {
        if (!empty($params['onlyActive']) || $this->only_active) {
            $query->andWhere("nl.status = :statusNode");
            $query->setParameter('statusNode', \App\Models\Entity\Node::STATUS_ACTIVE);
        }

        if (!empty($params['byPlugin'])) {
            $query->andWhere("n.plugin = :pluginName");
            $query->setParameter('pluginName', $params['byPlugin']);
            ;
        }

        if (!empty($params['byPlugins'])) {
            $query->andWhere($this->em->createQueryBuilder()->expr()->in('n.plugin', $params['byPlugins']));
        }

        if (!empty($params['byRoot'])) {
            $query->andWhere("n.root = :byRoot");
            $query->setParameter('byRoot', $params['byRoot']);
        }


        if (!empty($params['byNodeType'])) {
            $query->andWhere("n.type = :byNodeType");
            $query->setParameter('byNodeType', $params['byNodeType']);
        }

        if (!empty($params['greaterThenLevel'])) {
            $query->andWhere("n.level > :level");
            $query->setParameter('level', $params['greaterThenLevel']);
        }

        if (!empty($params['idInPath'])) {
            $this->em->getConfiguration()->addCustomStringFunction('FIND_IN_SET', 'DoctrineExtensions\Query\Mysql\FindInSet');
            $query->andWhere('FIND_IN_SET(:idToPath,n.path) != 0');
            $query->setParameter('idToPath', $params['idInPath']);
        }

        if (!empty($params['lessThenLevel'])) {
            $query->andWhere("n.level < :level");
            $query->setParameter('level', $params['lessThenLevel']);
        }
    }

    public function getNodesByHierarchy($nodeId = null, $options = array()) {
        $queryBd = $this->em->createQueryBuilder()->from('App\\Models\\Entity\\Node', 'n')->select("n, nl")
                ->join('n.node_lang', 'nl');
        $queryBd->andWhere("nl.lang = :lang");
        $queryBd->andWhere("n.structure = :structure");
        $queryBd->setParameter('lang', $this->lang);
        $queryBd->setParameter('structure', $this->structure);

        if (empty($options['order'])) {
            $queryBd->addOrderBy('n.path', 'asc');
            $queryBd->addOrderBy('n.sort', 'asc');
        }

        $this->_addCondtitions($queryBd, $options);
        $results = $queryBd->getQuery()->getResult();
        return $results;
    }

    public function getNodesByIds($ids, $onlyActive = false) {
        $queryBd = $this->em->createQueryBuilder()->from('App\\Models\\Entity\\Node', 'n')->select("n, nl")
                ->join('n.node_lang', 'nl')
                ->andWhere($this->em->createQueryBuilder()->expr()->in('n.id', $ids));
        $queryBd->andWhere("nl.lang = :lang");
        $queryBd->andWhere("n.structure = :structure");
        $queryBd->setParameter('structure', $this->structure);
        $queryBd->setParameter('lang', $this->lang);

        if ($onlyActive || $this->only_active) {
            $queryBd->andWhere("nl.status = :statusNode");
            $queryBd->setParameter('statusNode', \App\Models\Entity\Node::STATUS_ACTIVE);
        }

        $results = $queryBd->getQuery()->getResult();
        $newArray = array();
        foreach ($results as $result) {
            $newArray[$result->id] = $result;
        }
        return $newArray;
    }

    public function getTree($nodeId = null, $level = false) {
        $nodes = $this->getNodesByHierarchy($nodeId);

        $newNodes = array();
        foreach ($nodes as $node) {
            $newNodes[$node->id] = $node;
        }
        $tree = $this->buildTree($newNodes, $nodeId, $level);
        return $tree;
    }

    function buildTree($nodes, $root = null, $level = false) {
        $return = array();
        foreach ($nodes as $node) {
            if ($node->parent_id == $root && (!$level || $node->level < $level)) {
                unset($nodes[$node->id]);
                $return[] = array(
                    'node' => $node,
                    'children' => $this->buildTree($nodes, $node->id, $level)
                );
            }
        }
        return empty($return) ? null : $return;
    }

    public function setsort($arraySort) {
        $ids = array_values($arraySort);
        $nodes = $this->getNodesByIds($ids);
        $i = 1;
        foreach ($arraySort as $id) {
            $nodeTmp = $nodes[$id];
            $nodeTmp->sort = $i;
            $this->em->persist($nodeTmp);
            $i++;
        }
        $this->em->flush();
    }

    public function getPathNodes($node, $onlyActive = false) {
        $queryBd = $this->em->createQueryBuilder()->from('App\\Models\\Entity\\Node', 'n')->select("n, nl")
                ->join('n.node_lang', 'nl');
        $queryBd->andWhere("nl.lang = :lang");

        $queryBd->orderBy('n.sort', 'asc');
        $queryBd->setParameter('lang', $this->lang);

        $pathIds = explode(',', $node->path);
        $queryBd->andWhere($queryBd->expr()->in('n.id', $pathIds));
        $queryBd->orderBy('n.level', 'asc');

        if ($onlyActive || $this->only_active) {
            $queryBd->andWhere("nl.status = :statusNode");
            $queryBd->setParameter('statusNode', \App\Models\Entity\Node::STATUS_ACTIVE);
        }
        $results = $queryBd->getQuery()->getResult();
        $newArray = array();
        foreach ($results as $result) {
            $newArray[$result->id] = $result;
        }
        return $newArray;
    }

    public function checkPermissionToDelete($node) {
        $queryBd = $this->em->createQueryBuilder()->from('App\\Models\\Entity\\Node', 'n')->select("n, nl")
                ->join('n.node_lang', 'nl');
        $queryBd->andWhere("n.parent_id = :nodeId");
        $queryBd->setParameter('nodeId', $node->id);
        $results = $queryBd->getQuery()->getResult();
        if (!empty($results)) {
            return $results;
        } else {
            return false;
        }
    }

    public function checkPermissionToActive($node) {
        $nodeLang = $node->getNodeLang();
        if ($nodeLang->status != \App\Models\Entity\NodeLang::STATUS_ACTIVE) {
            return true;
        }
        $pathNodeIds = explode(',', $node->path);

        $pathIds = array();
        foreach ($pathNodeIds as $idNode) {
            if ($node->id != $idNode) {
                $pathIds[] = $idNode;
            }
        }
        $countParents = count($pathIds);
        if ($countParents > 0) {
            $queryBd = $this->em->createQueryBuilder()->from('App\\Models\\Entity\\Node', 'n')->select("n")->join('n.node_lang', 'nl');
            $queryBd->andWhere($queryBd->expr()->in('n.id', $pathIds));
            $queryBd->andWhere("n.id != :nodeId");
            $queryBd->andWhere("nl.status = :status");
            $queryBd->setParameter('nodeId', $node->id);
            $queryBd->setParameter('status', \App\Models\Entity\NodeLang::STATUS_ACTIVE);
            $results = $queryBd->getQuery()->getResult();
            $activeCounts = count($results);
            if ($countParents == $activeCounts) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    public function remove($node) {
        $nodeLang = $node->getNodeLang();
        if ($node->type != \App\Models\Entity\Node::TYPE_EMPTY) {
            if ($node instanceof SearchInterface) {
                if (!empty($this->options['searchMapper'])) {
                    $searchMapper = $this->options['searchMapper'];
                    $searchMapper->deleteFromSearch($node);
                }
            }
        }
        $this->em->remove($nodeLang);
        $this->em->flush();
    }

}
