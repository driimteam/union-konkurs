<?php

namespace App\Models\Interfaces;

use Doctrine\ORM\Mapping as ORM;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface; 

/**
 * A Content.
 */
interface SearchInterface
{
    
    public function getSearchTitle();
    public function getSearchLead();
    public function getSearchText();
    public function getSearchStatus();
    public function getSearchParentName();
    public function getSearchParentId();
    public function getSearchParentStatus();
    public function getSearchPath();
    public function getAllContents($array);
    
}