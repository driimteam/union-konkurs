<?php

namespace App\Models\Entity;

use Doctrine\ORM\Mapping as ORM;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

/**
 * A Node.
 *
 * @ORM\Table(name="cms_nodes")
 * @property string $title
 * @property string $create_date
 * @property int $id
 */
class NodeLang extends Base implements InputFilterAwareInterface {

    const STATUS_ACTIVE = "ACTIVE";
    const STATUS_INACTIVE = "INACTIVE";

    protected $inputFilter;
    protected $node_id;
    protected $create_date;
    protected $title;
    protected $status;
    protected $lang;
    protected $idnode;
    protected $text_1;
    protected $text_2;

    public function __construct($node = null) {
        if (!empty($node)) {
            $this->node = $node;
            $this->node_id = $node->id;
        }
    }

    public function getInputFilter() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();

            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'title',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 200,
                                ),
                            ),
                        ),
            )));


            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public static function getStatusList() {
        return array(self::STATUS_ACTIVE, self::STATUS_INACTIVE);
    }

    public function getTitle() {
        $title = $this->title;
        if (empty($title)) {
            return "brak";
        } else {
            return $title;
        }
    }

    public function getNode() {
        return $this->node;
    }

    public function setNode($node) {
        $this->node = $node;
    }

}
