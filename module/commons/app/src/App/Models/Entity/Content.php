<?php

namespace App\Models\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Models\Entity\ContentLang as ContentLang;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use App\Models\Interfaces\SearchInterface;

/**
 * A Content.
 */
class Content extends Base implements InputFilterAwareInterface, SearchInterface
{
    const STATUS_ACTIVE="ACTIVE";
    const STATUS_INACTIVE="INACTIVE";
    ///const STATUS_NEW="NEW";
    
    protected $inputFilter;

    protected $id;
    protected $nod_id;
    protected $thumb;
    protected $node;
    protected $content_lang;
   
    public function __construct() {
        //$this->content_lang = new ContentLang();
    }
    
    /*public function __get($property) 
    {
        $tmpproperty = $property;
        if(!$this->hasProperty($property)){
            $contentLang = $this->getContentLang();
            if($contentLang->hasProperty($property)){
                $tmpproperty = $contentLang->$property;
            }else{
                $tmpproperty = $this->$property;
            }
        }else{
            $tmpproperty = $this->$property;
        }
        return $tmpproperty;
    }

    public function __set($property, $value) 
    {
        if(!$this->hasProperty($property)){
            $contentLang = $this->getContentLang();
            if($contentLang->hasProperty($property)){
                $contentLang->$property = $value;
            }else{
                $this->$property=$value;
            }
        }else{
            $this->$property=$value;
        }
    }*/
    
    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();

            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                'name'       => 'id',
                'required'   => false,
                'filters' => array(
                    array('name'    => 'Int'),
                ),
            )));

            $inputFilter->add($factory->createInput(array(
                'name'     => 'title',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 1,
                            'max'      => 200,
                        ),
                    ),
                ),
            )));

            $inputFilter->add($factory->createInput(array(
                'name'     => 'lead',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 1,
                            'max'      => 256,
                        ),
                    ),
                ),
            )));
            
            $inputFilter->add($factory->createInput(array(
                'name'     => 'nod_id',
                'required' => true,
                'filters'  => array(),
                'validators' => array(),
            )));
            
            $inputFilter->add($factory->createInput(array(
                'name'     => 'text',
                'required' => true,
                'filters'  => array(),
                'validators' => array(
                    array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8'
                        ),
                    ),
                ),
            )));

            $this->inputFilter = $inputFilter;        
        }

        return $this->inputFilter;
    }
    
    public static function getStatusList(){
        return array(self::STATUS_ACTIVE, self::STATUS_INACTIVE);
    }
    
    public function setNode($node){
        $this->node = $node;
    }
    public function getContentLang(){
        if($this->content_lang){
            return $this->content_lang;
        }else{
            $this->content_lang = new ContentLang();
            return $this->content_lang;
        }
        
    }
    public function setContentLang($contentLang){
        $this->content_lang=$contentLang;
    }
    
    public function getSearchTitle(){
        return $this->getContentLang()->title;
    }
    public function getSearchLead(){
        return  $this->getContentLang()->lead;
    }
    public function getSearchText(){
        return  $this->getContentLang()->text;
    }
    
    public function getSearchStatus(){
        return  $this->getContentLang()->status;
    }
    public function getSearchParentName(){
        $parent = $this->node;
        return get_class($parent);
    }

    public function getSearchParentId(){
        return $this->node->id;
    }
    
    public function getSearchParentStatus(){
        return $this->node->getNodeLang()->status;
    }
    
    public function getSearchPath(){
        return $this->node->path;
    }
    
    public function isSearchParent(){
        return false;
    }
    
    public function getUrlName(){
        $nodeLang = $this->node->getNodeLang();
        $contentLang = $this->getContentLang();
        //$title = \Tools_File::prepareUrl($nodeLang->title).'/'.\Tools_File::prepareUrl($contentLang->title);
        $title = \Tools_File::prepareUrl($contentLang->title);
        return $title;
    }
    
    public function getAllContents($pluginConfig) {
        $plugin = $this->node->plugin;
        $pluginConf = $pluginConfig[$plugin];
        $search='';
        $contentLang = $this->getContentLang();
        if(!empty($pluginConf['search_fields'])){
            foreach($pluginConf['search_fields'] as $field){
                $search.=' ';
                $search.=$contentLang->$field;
            }
        }
        return $search;
    }
    
    
    /*public function populate($data){
        parent::populate($data);
        $contentLang = $this->getContentLang();
        $contentLang->populate($data);
    }*/
    
}
