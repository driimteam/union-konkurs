<?php

namespace App\Models\Entity;

use Doctrine\ORM\Mapping as ORM;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface; 

/**
 * A Content.
 */
class File extends Base implements InputFilterAwareInterface
{
    
    const TYPE_FILE = "FILE";
    const TYPE_PICTURE = "PICTURE";
    protected $inputFilter;

    protected $id;
    protected $name;
    protected $description;
    protected $type;
    protected $path;
    
    
    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();

            $factory = new InputFactory();
            $this->inputFilter = $inputFilter;        
        }

        return $this->inputFilter;
    }
    
    
    
    
}