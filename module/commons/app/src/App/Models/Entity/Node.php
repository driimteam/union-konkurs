<?php

namespace App\Models\Entity;
use Doctrine\ORM\Mapping as ORM;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface; 
use App\Models\Interfaces\SearchInterface;
use App\Models\Entity\NodeLang as NodeLang;

/**
 * A Node.
 *
 * @ORM\Table(name="cms_nodes")
 * @property string $title
 * @property string $create_date
 * @property int $id
 */
class Node  extends Base implements InputFilterAwareInterface, SearchInterface
{
    const TYPE_PAGE = "PAGE";
    const TYPE_EMPTY = "EMPTY";
    const TYPE_LIST = "LIST";
    const STATUS_ACTIVE="ACTIVE";
    const STATUS_INACTIVE="INACTIVE";
    
    const STRUCTURE_APPLICATION = "APPLICATION";
    const STRUCTURE_MODULE = "MODULE";
    
    protected $inputFilter;
    protected $id;
    protected $parent_id;
    protected $children;
    protected $parent;
    protected $root;
    protected $level;
    protected $path;
    protected $type;
    protected $sort;
    protected $plugin;
    protected $node_lang;
    protected $structure;
    protected $thumb_1;
    protected $thumb_2;
    protected $background;
    
    public function __construct() {
        $this->children = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();

            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                'name'       => 'id',
                'required'   => true,
                'filters' => array(
                    array('name'    => 'Int'),
                ),
            )));

            $inputFilter->add($factory->createInput(array(
                'name'     => 'title',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 1,
                            'max'      => 200,
                        ),
                    ),
                ),
            )));
            
            $inputFilter->add($factory->createInput(array(
                'name'     => 'parent_id',
                'required' => false,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                )
            )));
            
            $this->inputFilter = $inputFilter;        
        }

        return $this->inputFilter;
    }
    
    public static function getTypes(){
        return array(self::TYPE_EMPTY, self::TYPE_PAGE, self::TYPE_LIST);
    }
    
    public static function getStatusList(){
        return array(self::STATUS_ACTIVE, self::STATUS_INACTIVE);
    }
    
    public function getSpaceByLevel($sep){
        $level = $this->level; 
        $str='';
        if($level>=1){
            for($i=0; $i<$level; $i++){
                $str.=$sep;
            }
        }
        return $str;
    }
    
    
    public function setPath($parentObject){
        if($parentObject===null){
            $this->path = $this->id;
        }else{
            $parentpath = $parentObject->getPath();
            if(empty($parentpath)){
                $this->path = $parentObject->id;
            }else{
                $this->path = $parentObject->path.','.$this->id;
            }
        }
    }
    
    public function setRoot($parentObject){
        if($parentObject==null){
            $this->root = $this->id;
        }else{
            $root = $parentObject->getRoot();
            if(empty($root)){
                $root = $parentObject->id;
            }
            $this->root = $root;
        }
    }
    
    public function setLevel($parentObject){
        if($parentObject==null){
            $this->level = 0;
        }else{
            $levelParent = $parentObject->getLevel();
            $this->level = $levelParent + 1;
        }
    }
    
    public function setParentId($parentObject){
        if($parentObject==null){
            $this->parent_id = null;
        }else{
            $this->parent_id = $parentObject->id;
        }
    }
    
    
    public function setHierarchy($parentObject){
        $this->setPath($parentObject);
        $this->setRoot($parentObject);
        $this->setLevel($parentObject);
        $this->setParentId($parentObject);
        $this->setParent($parentObject);
    }
    
    public function getPath(){
       return $this->path;
    }
    
    public function getLevel(){
       return $this->level;
    }
    
    public function getRoot(){
       return $this->root;
    }
    public function setParent($parent){
       $this->parent= $parent;
    }
    
    public function getParent(){
       return $this->parent;
    }
    public function getPlugin(){
       return $this->plugin;
    }
    
    public function setChildren($parent){
       $this->children= $parent;
    }
    
    public function getChildren($parent){
       return $this->children;
    }
    
    public function getNodeLang(){
        if($this->node_lang){
            return $this->node_lang;
        }else{
            $this->node_lang = new NodeLang();
            return $this->node_lang;
        }
    }
    public function setNodeLang($contentLang){
        $this->node_lang=$contentLang;
    }
    
    public function setPlugin($plugin){
        $this->plugin = $plugin;
    }
    
    public function getSearchTitle(){
        return $this->getNodeLang()->title;
    }
    public function getSearchLead(){
        return  '';
    }
    public function getSearchText(){
        return  '';
    }
    public function getSearchStatus(){
        return  $this->getNodeLang()->status;
    }
    
    public function getSearchParentName(){
        return null;
    }

    public function getSearchParentId(){
        return null;
    }
    
    public function getSearchParentStatus(){
        return null;
    }
    
    public function getSearchPath(){
        return $this->path;
    }
    public function isSearchParent(){
        return true;
    }
    public function getAllContents($pluginConfig) {
        $nodeLang = $this->getNodeLang();
        return $nodeLang->title.' '.$nodeLang->text_1.' '.$nodeLang->text_2;
    }
    public function getUrlName(){
        $nodeLang = $this->getNodeLang();
        $title = \Tools_File::prepareUrl($nodeLang->title);
        return $title;
    }
}