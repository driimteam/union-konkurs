<?php

namespace App\Models\Entity;

use Doctrine\ORM\Mapping as ORM;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface; 

/**
 * A Content.
 */
class ContentRelated extends Base
{
    
    protected $inputFilter;

    protected $id;
    protected $content_id;
    protected $object_id;
    protected $object_name;
    protected $create_date;
    
    protected $content;
    protected $lang;
    protected $sort;
   
     public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();

            $factory = new InputFactory();
            $this->inputFilter = $inputFilter;        
        }

        return $this->inputFilter;
    }
    
    public function setContent($content){
        $this->content = $content;
    }
            
}