<?php

namespace App\Models\Entity;

use Doctrine\ORM\Mapping as ORM;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;  

/**
 * A Content.
 */
class Search extends Base
{
    const STATUS_ACTIVE="ACTIVE";
    const STATUS_INACTIVE="INACTIVE";

    protected $id;
    protected $title;
    protected $lead;
    protected $text;
    protected $create_date;
    protected $update_date;
    protected $object_id;
    protected $object_name;
    protected $lang;
    
    protected $parent_name;
    protected $parent_id;
    protected $status;
    protected $parent_status;
    protected $path;
    protected $allcontents;
    protected $structure;
    
    public function getSearchUrlName(){
        $title = \Tools_File::prepareFileName($this->title);
        
        return $title;
    }
    
}