<?php

namespace App\Models\Entity;
use Doctrine\ORM\Mapping as ORM;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface; 

/**
 * A Node.
 *
 * @ORM\Table(name="cms_nodes")
 * @property string $title
 * @property string $create_date
 * @property int $id
 */
abstract class Base
{
    
   
    /**
     * Magic getter to expose protected properties.
     *
     * @param string $property
     * @return mixed
     */
    public function __get($property) 
    {
        return $this->$property;
    }

    /**
     * Magic setter to save protected properties.
     *
     * @param string $property
     * @param mixed $value
     */
    public function __set($property, $value) 
    {
        $this->$property = $value;
    }

    /**
     * Convert the object to an array.
     *
     * @return array
     */
    public function getArrayCopy() 
    {
        $properties=$this->getNameProperties();
        $arrayProperties = array();
        foreach($properties as $nameproperty=>$property){
            $arrayProperties[$nameproperty]=$this->$nameproperty;
        } 
        return $arrayProperties;
    }
    
    public function getNameProperties() 
    {
        return get_class_vars(get_class($this));
    }
    
    public function hasProperty($property){
         $properties=array_keys($this->getNameProperties());
         return in_array($property, $properties) ? true : false ;
    }
    
    public function populate($data = array()) 
    {
        $properties=$this->getNameProperties();
        
        foreach($data as $keyproperty=>$property){
            if(in_array($keyproperty,  array_keys($properties))){
                
                $this->$keyproperty = $property;
            }
        } 
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }
    
    public function getInputFilter()
    {
        throw new \Exception("Not defined");
    }

    
    
    
    
}