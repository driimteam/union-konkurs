<?php

namespace App\Models\Entity;

use Doctrine\ORM\Mapping as ORM;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface; 

/**
 * A Content.
 */
class ContentLang extends Base implements InputFilterAwareInterface
{
    const STATUS_ACTIVE="ACTIVE";
    const STATUS_INACTIVE="INACTIVE";
    ///const STATUS_NEW="NEW";
    
    protected $inputFilter;

    protected $idcontent;
    protected $content_id;
    protected $title;
    protected $lead;
    protected $text;
    protected $create_date;
    protected $update_date;
    protected $publication_date;
    protected $is_published;
    protected $status;
    protected $main_picture;
    protected $author;
    protected $lang;
    
    protected $string_1;
    protected $string_2;
    protected $string_3;
    protected $int_1;
    protected $int_2;
    protected $int_3;
    protected $category_id;
    protected $category_name;
    protected $related_id;
    protected $sort;
   
    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();

            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                'name'       => 'id',
                'required'   => false,
                'filters' => array(
                    array('name'    => 'Int'),
                ),
            )));

            $inputFilter->add($factory->createInput(array(
                'name'     => 'title',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 1,
                            'max'      => 200,
                        ),
                    ),
                ),
            )));

            $inputFilter->add($factory->createInput(array(
                'name'     => 'lead',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 1,
                            'max'      => 256,
                        ),
                    ),
                ),
            )));
            
            $inputFilter->add($factory->createInput(array(
                'name'     => 'nod_id',
                'required' => true,
                'filters'  => array(),
                'validators' => array(),
            )));
            
            $inputFilter->add($factory->createInput(array(
                'name'     => 'text',
                'required' => true,
                'filters'  => array(),
                'validators' => array(
                    array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8'
                        ),
                    ),
                ),
            )));

            $this->inputFilter = $inputFilter;        
        }

        return $this->inputFilter;
    }
    
    public static function getStatusList(){
        return array(self::STATUS_ACTIVE, self::STATUS_INACTIVE);
    }
    
    
    
}
