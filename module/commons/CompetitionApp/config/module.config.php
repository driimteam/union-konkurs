<?php
namespace CompetitionApp;
return array(    
    'doctrine' => array(
        'driver' => array(
            __NAMESPACE__ . '_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\XmlDriver',
                'cache' => 'array',
                'paths' => array(__DIR__ . '/xml/orm')
            ),
            'orm_default' => array(
                'drivers' => array(
                    __NAMESPACE__ . '\Models\Entity' => __NAMESPACE__ . '_driver',
                )
            )
        )
    )
);