<?php

namespace CompetitionApp\Models\Mapper;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Pagination\Paginator as ORMpaginator;
use Zend\Paginator\Paginator;
use Zend\Stdlib\Hydrator\HydratorInterface;
use CompetitionApp\Models\Entity\CompetitionApplication as CompetitionApplicationEntity;

class CompetitionApplication
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;

    const ENTITY_CLASS = '\CompetitionApp\Models\Entity\CompetitionApplication';

    public function __construct(EntityManager $em, $options = array())
    {
        $this->em = $em;
    }

    public function find($id)
    {
        $find = $this->em->find(self::ENTITY_CLASS, $id);
        return $find;
    }

    public function save($entity)
    {
        $this->em->persist($entity);
        $this->em->flush();
    }

    public function findBy($params = array())
    {
        $entity = $this->em->getRepository(self::ENTITY_CLASS)->findBy($params);
        return $entity;
    }

    public function _addCondtitions($query, $params)
    {

        if (!empty($params['sort'])) {
            $query->addOrderBy($params['sort']['order'], $params['sort']['dir']);
        }
        if (!empty($params['addsort'])) {
            $query->addOrderBy($params['addsort']['order'],
                $params['addsort']['dir']);
        }

        if (array_key_exists('byStatus', $params) && !empty($params['byStatus'])) {
            $query->andWhere('ca.status=:status');
            $query->setParameter('status', $params['byStatus']);
        }

        if (array_key_exists('byType', $params) && !empty($params['byType'])) {
            $query->andWhere('ca.type=:type');
            $query->setParameter('type', $params['byType']);
        }

        if (array_key_exists('byNotType', $params) && !empty($params['byNotType'])) {
            $query->andWhere('ca.type!=:notType');
            $query->setParameter('notType', $params['byNotType']);
        }
        
        if (array_key_exists('bySection', $params) && !empty($params['bySection'])) {
            $query->andWhere('ca.section=:section');
            $query->setParameter('section', $params['bySection']);
        }
        

    }

    public function getFillOutFormByUser($userId)
    {
        $queryBd = $this->em->createQueryBuilder();
        $queryBd->from(self::ENTITY_CLASS, 'tc')->select("tc")
            ->andWhere('tc.userId=:userId');
        $queryBd->setParameter('userId', $userId);
        $result  = $queryBd->getQuery()->getOneOrNullResult();
        return $result;
    }

    public function getByToken($token)
    {
        $queryBd = $this->em->createQueryBuilder();
        $queryBd->from(self::ENTITY_CLASS, 'ca')->select("ca")
            ->andWhere('ca.token=:token');
        $queryBd->setParameter('token', $token);
        $result  = $queryBd->getQuery()->getOneOrNullResult();
        return $result;
    }


    public function findByPager($page, $limit, $params = array())
    {
        $offset  = $limit * ($page - 1);
        $queryBd = $this->em->createQueryBuilder();
        $queryBd->from(self::ENTITY_CLASS, 'ca')->select("ca");
        $this->_addCondtitions($queryBd, $params);
        $queryBd->setMaxResults($limit)->setFirstResult($offset);

        $paginator = new ORMpaginator($queryBd);
        $adapter   = new \App\Paginator\DoctrinePaginatorAdapter($paginator);
        $paginator = new Paginator($adapter);
        $paginator->setCurrentPageNumber($page);
        $paginator->setItemCountPerPage($limit);
        
        return $paginator;
    }

    public function checkRecommendEmail($email)
    {
        $queryBd = $this->em->createQueryBuilder();
        $queryBd->from(self::ENTITY_CLASS, 'ca')->select("count(ca.id) as counter")
            ->andWhere('ca.recommendEmail=:email');
        $queryBd->setParameter('email', $email);
        $result  = $queryBd->getQuery()->getSingleScalarResult();
        return $result;
    }

    public function checkUserEmail($email)
    {
        $queryBd = $this->em->createQueryBuilder();
        $queryBd->from(self::ENTITY_CLASS, 'ca')->select("count(ca.id) as counter")
            ->andWhere('ca.userEmail=:email')
            ->andWhere('ca.status!=:status');
        $queryBd->setParameter('email', $email);
        $queryBd->setParameter('status', CompetitionApplicationEntity::STATUS_NEW);
        $result  = $queryBd->getQuery()->getSingleScalarResult();
        return $result;
    }

    public function checkExistPhone($phone)
    {
        $queryBd = $this->em->createQueryBuilder();
        $queryBd->from(self::ENTITY_CLASS, 'ca')->select("count(ca.id) as counter")
            ->andWhere('ca.userPhone=:phone')
            ->andWhere('ca.status!=:status');
        $queryBd->setParameter('phone', $phone);
        $queryBd->setParameter('status', CompetitionApplicationEntity::STATUS_NEW);
        $result  = $queryBd->getQuery()->getSingleScalarResult();
        return $result;
    }

}
