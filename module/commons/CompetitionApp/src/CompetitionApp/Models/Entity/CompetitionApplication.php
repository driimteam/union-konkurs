<?php

namespace CompetitionApp\Models\Entity;

/**
 * CompetitionApplication
 */
class CompetitionApplication
{
    
    const TYPE_CHILDREN = "CHILDREN";
    const TYPE_FRIENDS = "FRIENDS";
    
    const STATUS_NEW = "NEW";
    const STATUS_CONFIRMED = "CONFIRMED";
    const STATUS_ACTIVE = "ACTIVE";
    const STATUS_REJECTED = "REJECTED";
    const STATUS_TO_VERIFY = "TO_VERIFY";
    
    const SECTION_TEXT = 'TEXT';
    const SECTION_YOUTUBE = 'YOUTUBE';

    static function getTypes(){
        return array(
            self::TYPE_CHILDREN,
            self::TYPE_FRIENDS
        );
    }
    
    /**
     * @var string
     */
    private $type;

    /**
     * @var string
     */
    private $section;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var string
     */
    private $userName;

    /**
     * @var string
     */
    private $userFirstname;

    /**
     * @var string
     */
    private $userLastname;

    /**
     * @var string
     */
    private $userEmail;

    /**
     * @var string
     */
    private $userAddress;

    /**
     * @var string
     */
    private $userPhone;

    /**
     * @var string
     */
    private $userPostalcode;

    /**
     * @var string
     */
    private $userCity;

    /**
     * @var string
     */
    private $userCountry;

    /**
     * @var string
     */
    private $userSocialData;

    /**
     * @var string
     */
    private $userSocialLink;

    /**
     * @var string
     */
    private $filePath;

    /**
     * @var string
     */
    private $youtubeLink;

    /**
     * @var integer
     */
    private $userId;

    /**
     * @var string
     */
    private $status;

    /**
     * @var string
     */
    private $token;

    /**
     * @var \DateTime
     */
    private $confirmedDate;

    /**
     * @var integer
     */
    private $updateAdminId;

    /**
     * @var boolean
     */
    private $rule1;

    /**
     * @var boolean
     */
    private $rule2;

    /**
     * @var boolean
     */
    private $rule3;

    /**
     * @var integer
     */
    private $id;


    /**
     * Set type
     *
     * @param string $type
     *
     * @return CompetitionApplication
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set section
     *
     * @param string $section
     *
     * @return CompetitionApplication
     */
    public function setSection($section)
    {
        $this->section = $section;
    
        return $this;
    }

    /**
     * Get section
     *
     * @return string
     */
    public function getSection()
    {
        return $this->section;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return CompetitionApplication
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set userName
     *
     * @param string $userName
     *
     * @return CompetitionApplication
     */
    public function setUserName($userName)
    {
        $this->userName = $userName;
    
        return $this;
    }

    /**
     * Get userName
     *
     * @return string
     */
    public function getUserName()
    {
        return $this->userName;
    }

    /**
     * Set userFirstname
     *
     * @param string $userFirstname
     *
     * @return CompetitionApplication
     */
    public function setUserFirstname($userFirstname)
    {
        $this->userFirstname = $userFirstname;
    
        return $this;
    }

    /**
     * Get userFirstname
     *
     * @return string
     */
    public function getUserFirstname()
    {
        return $this->userFirstname;
    }

    /**
     * Set userLastname
     *
     * @param string $userLastname
     *
     * @return CompetitionApplication
     */
    public function setUserLastname($userLastname)
    {
        $this->userLastname = $userLastname;
    
        return $this;
    }

    /**
     * Get userLastname
     *
     * @return string
     */
    public function getUserLastname()
    {
        return $this->userLastname;
    }

    /**
     * Set userEmail
     *
     * @param string $userEmail
     *
     * @return CompetitionApplication
     */
    public function setUserEmail($userEmail)
    {
        $this->userEmail = $userEmail;
    
        return $this;
    }

    /**
     * Get userEmail
     *
     * @return string
     */
    public function getUserEmail()
    {
        return $this->userEmail;
    }

    /**
     * Set userAddress
     *
     * @param string $userAddress
     *
     * @return CompetitionApplication
     */
    public function setUserAddress($userAddress)
    {
        $this->userAddress = $userAddress;
    
        return $this;
    }

    /**
     * Get userAddress
     *
     * @return string
     */
    public function getUserAddress()
    {
        return $this->userAddress;
    }

    /**
     * Set userPhone
     *
     * @param string $userPhone
     *
     * @return CompetitionApplication
     */
    public function setUserPhone($userPhone)
    {
        $this->userPhone = $userPhone;
    
        return $this;
    }

    /**
     * Get userPhone
     *
     * @return string
     */
    public function getUserPhone()
    {
        return $this->userPhone;
    }

    /**
     * Set userPostalcode
     *
     * @param string $userPostalcode
     *
     * @return CompetitionApplication
     */
    public function setUserPostalcode($userPostalcode)
    {
        $this->userPostalcode = $userPostalcode;
    
        return $this;
    }

    /**
     * Get userPostalcode
     *
     * @return string
     */
    public function getUserPostalcode()
    {
        return $this->userPostalcode;
    }

    /**
     * Set userCity
     *
     * @param string $userCity
     *
     * @return CompetitionApplication
     */
    public function setUserCity($userCity)
    {
        $this->userCity = $userCity;
    
        return $this;
    }

    /**
     * Get userCity
     *
     * @return string
     */
    public function getUserCity()
    {
        return $this->userCity;
    }

    /**
     * Set userCountry
     *
     * @param string $userCountry
     *
     * @return CompetitionApplication
     */
    public function setUserCountry($userCountry)
    {
        $this->userCountry = $userCountry;
    
        return $this;
    }

    /**
     * Get userCountry
     *
     * @return string
     */
    public function getUserCountry()
    {
        return $this->userCountry;
    }

    /**
     * Set userSocialData
     *
     * @param string $userSocialData
     *
     * @return CompetitionApplication
     */
    public function setUserSocialData($userSocialData)
    {
        $this->userSocialData = $userSocialData;
    
        return $this;
    }

    /**
     * Get userSocialData
     *
     * @return string
     */
    public function getUserSocialData()
    {
        return $this->userSocialData;
    }

    /**
     * Set userSocialLink
     *
     * @param string $userSocialLink
     *
     * @return CompetitionApplication
     */
    public function setUserSocialLink($userSocialLink)
    {
        $this->userSocialLink = $userSocialLink;
    
        return $this;
    }

    /**
     * Get userSocialLink
     *
     * @return string
     */
    public function getUserSocialLink()
    {
        return $this->userSocialLink;
    }

    /**
     * Set filePath
     *
     * @param string $filePath
     *
     * @return CompetitionApplication
     */
    public function setFilePath($filePath)
    {
        $this->filePath = $filePath;
    
        return $this;
    }

    /**
     * Get filePath
     *
     * @return string
     */
    public function getFilePath()
    {
        return $this->filePath;
    }

    /**
     * Set youtubeLink
     *
     * @param string $youtubeLink
     *
     * @return CompetitionApplication
     */
    public function setYoutubeLink($youtubeLink)
    {
        $this->youtubeLink = $youtubeLink;
    
        return $this;
    }

    /**
     * Get youtubeLink
     *
     * @return string
     */
    public function getYoutubeLink()
    {
        return $this->youtubeLink;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return CompetitionApplication
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    
        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return CompetitionApplication
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set token
     *
     * @param string $token
     *
     * @return CompetitionApplication
     */
    public function setToken($token)
    {
        $this->token = $token;
    
        return $this;
    }

    /**
     * Get token
     *
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Set confirmedDate
     *
     * @param \DateTime $confirmedDate
     *
     * @return CompetitionApplication
     */
    public function setConfirmedDate($confirmedDate)
    {
        $this->confirmedDate = $confirmedDate;
    
        return $this;
    }

    /**
     * Get confirmedDate
     *
     * @return \DateTime
     */
    public function getConfirmedDate()
    {
        return $this->confirmedDate;
    }

    /**
     * Set updateAdminId
     *
     * @param integer $updateAdminId
     *
     * @return CompetitionApplication
     */
    public function setUpdateAdminId($updateAdminId)
    {
        $this->updateAdminId = $updateAdminId;
    
        return $this;
    }

    /**
     * Get updateAdminId
     *
     * @return integer
     */
    public function getUpdateAdminId()
    {
        return $this->updateAdminId;
    }

    /**
     * Set rule1
     *
     * @param boolean $rule1
     *
     * @return CompetitionApplication
     */
    public function setRule1($rule1)
    {
        $this->rule1 = $rule1;
    
        return $this;
    }

    /**
     * Get rule1
     *
     * @return boolean
     */
    public function getRule1()
    {
        return $this->rule1;
    }

    /**
     * Set rule2
     *
     * @param boolean $rule2
     *
     * @return CompetitionApplication
     */
    public function setRule2($rule2)
    {
        $this->rule2 = $rule2;
    
        return $this;
    }

    /**
     * Get rule2
     *
     * @return boolean
     */
    public function getRule2()
    {
        return $this->rule2;
    }

    /**
     * Set rule3
     *
     * @param boolean $rule3
     *
     * @return CompetitionApplication
     */
    public function setRule3($rule3)
    {
        $this->rule3 = $rule3;
    
        return $this;
    }

    /**
     * Get rule3
     *
     * @return boolean
     */
    public function getRule3()
    {
        return $this->rule3;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    private $content;


    /**
     * Set content
     *
     * @param string $content
     *
     * @return CompetitionApplication
     */
    public function setContent($content)
    {
        $this->content = $content;
    
        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }
    /**
     * @var boolean
     */
    private $rule4;

    /**
     * @var boolean
     */
    private $rule5;

    /**
     * @var boolean
     */
    private $rule6;


    /**
     * Set rule4
     *
     * @param boolean $rule4
     *
     * @return CompetitionApplication
     */
    public function setRule4($rule4)
    {
        $this->rule4 = $rule4;
    
        return $this;
    }

    /**
     * Get rule4
     *
     * @return boolean
     */
    public function getRule4()
    {
        return $this->rule4;
    }

    /**
     * Set rule5
     *
     * @param boolean $rule5
     *
     * @return CompetitionApplication
     */
    public function setRule5($rule5)
    {
        $this->rule5 = $rule5;
    
        return $this;
    }

    /**
     * Get rule5
     *
     * @return boolean
     */
    public function getRule5()
    {
        return $this->rule5;
    }

    /**
     * Set rule6
     *
     * @param boolean $rule6
     *
     * @return CompetitionApplication
     */
    public function setRule6($rule6)
    {
        $this->rule6 = $rule6;
    
        return $this;
    }

    /**
     * Get rule6
     *
     * @return boolean
     */
    public function getRule6()
    {
        return $this->rule6;
    }
}
