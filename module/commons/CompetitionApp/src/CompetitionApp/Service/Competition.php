<?php

namespace CompetitionApp\Service;

use Zend\Form\Form;
use Zend\ServiceManager\ServiceManagerAwareInterface;
use Zend\ServiceManager\ServiceManager;
use Zend\Crypt\Password\Bcrypt;
use Zend\Stdlib\Hydrator;
use Zend\Authentication\Storage\Session as StorageSession;
use Zend\Stdlib\DispatchableInterface;
use CompetitionApp\Models\Entity\CompetitionApplication;

class Competition implements ServiceManagerAwareInterface
{

    /**
     * @var ServiceManager
     */
    protected $serviceManager;
    protected $eventManager;
    protected $user_service;

    /**
     * Mappers
     */
    protected $competition_application_mapper;

    /**
     * Retrieve service manager instance
     *
     * @return ServiceManager
     */
    public function getServiceManager()
    {
        return $this->serviceManager;
    }

    /**
     * Set service manager instance
     *
     * @param ServiceManager $serviceManager
     * @return User
     */
    public function setServiceManager(ServiceManager $serviceManager)
    {
        $this->serviceManager = $serviceManager;
        return $this;
    }

    public function getEntityManager()
    {
        return $this->getServiceManager()->get('Doctrine\ORM\EntityManager');
    }

    public function getUserService()
    {
        if ($this->user_service === null) {
            $this->user_service = $this->getServiceManager()->get('user_service');
        }
        return $this->user_service;
    }

    public function getCompetitionApplicationMapper()
    {
        if ($this->competition_application_mapper === null) {
            $this->competition_application_mapper = $this->getServiceManager()->get('competition_application_mapper');
        }
        return $this->competition_application_mapper;
    }

    public function setCompetitionApplicationMapper($competition_application_mapper)
    {
        $this->competition_application_mapper = $competition_application_mapper;
        return $this;
    }

    public function getListPagerBySection($page, $limit,$section){
        $params['bySection'] = $section;//CompetitionApplication::SECTION_YOUTUBE;
        $params['byStatus'] = CompetitionApplication::STATUS_ACTIVE;
        $params['sort']['order'] ='ca.createdAt';
        $params['sort']['dir'] ='desc';
        return $this->getCompetitionApplicationMapper()->findByPager($page, $limit,$params);
    }

}
