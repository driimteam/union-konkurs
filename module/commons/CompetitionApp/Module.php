<?php

namespace CompetitionApp;

use CompetitionApp\Module as CompetitionApp;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use Doctrine\ORM\Mapping\Driver\XmlDriver;
use Zend\ModuleManager\ModuleManager;

class Module
{

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__.'/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__.'/src/'.__NAMESPACE__,
                ),
            ),
        );
    }

    public function getServiceConfig()
    {
        $module              = $this;
        return array(
            'invokables' => array(
                'competitionapp_service' => 'CompetitionApp\Service\Competition',
            ),
            'factories' => array(
                'competition_application_mapper' => function ($sm) {
                    $em     = $sm->get('Doctrine\ORM\EntityManager');
                    $mapper = new \CompetitionApp\Models\Mapper\CompetitionApplication($em);
                    return $mapper;
                }
            ),
        );
    }

    public function getConfig()
    {
        return include __DIR__.'/config/module.config.php';
    }
}
