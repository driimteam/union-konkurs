<?php
class Tools_File {
	
    static public function checkFileLifeTime ($file, $time) {
        return (mktime()-filemtime($file)) < $time;
    }


    static function getFileNameFromPath($path) {
        $path = str_replace("\\", "/", $path);
        $e = explode("/", $path);
        return $e[count($e) - 1];
    }

    function downloadFile($file_source, $file_target) {
        $rh = fopen($file_source, 'rb');
        $wh = fopen($file_target, 'wb');
        if ($rh === false || $wh === false) {
            // error reading or opening file
            return true;
        }
        while (!feof($rh)) {
            if (fwrite($wh, fread($rh, 1024)) === FALSE) {
                // 'Download error: Cannot write to file ('.$file_target.')';
                return true;
            }
        }
        fclose($rh);
        fclose($wh);
        // No error
        return false;
    }

    static function serializeHash($input) {
        $strtrArr = array (
            "\n" => ':n:',
            "\r" => ':r:'
            );
            foreach ($input as $key => $val) {
                $input[$key] = strtr($val, $strtrArr);
            }

            return serialize($input);
    }
    

    static function fileExt($oryginalFileName) {
        $pos = strrpos($oryginalFileName, ".");
        $ext = '';
        if ($pos) {
            $ext = substr($oryginalFileName, $pos+1);
        }
        return strtolower($ext);

    }

    static function prepareFileName($oryginalFileName, $addPrefix = false, $addUniq = false) {

        $pos = strrpos($oryginalFileName, ".");

        $ext = '';
        if ($pos) {
            $ext = substr($oryginalFileName, $pos);
            $fname = substr($oryginalFileName, 0, $pos);
        } else {
            $fname = $oryginalFileName;
        }

        $fname = self :: clearUrlSpecChars($fname);

        if ($addPrefix) {
            $fname = $addPrefix . '-' . $fname;
        }

        if ($addUniq) {
            $fname = $fname . '-' . uniqid();
        }

        return strtolower($fname . $ext);
    }

    static function reFileName($path, $fileName) {

        $pos = strrpos($fileName, ".");

        $ext = '';
        if ($pos) {
            $ext = substr($fileName, $pos);
            $fname = substr($fileName, 0, $pos);
        } else {
            $fname = $fileName;
        }

        for ($i = 0; $i < 10; $i++) {
            $nFileName = ($i ? $fname . '-(' . $i . ')' : $fname) . $ext;
            if (!is_file($path . '/' . $nFileName)) {
                return $nFileName;
            }
        }
        return $fileName . '-' . uniqid();

    }

    static function prepareUrl($title) {
        $title = mb_convert_case($title, MB_CASE_LOWER, "UTF-8");
        $title = strtr($title, array (
            "'" => '',
            '"' => '',
            ',' => '-',
            '?' => '',
            '!' => '',
            '.' => '',
            '\\' => '',
            '[' => '',
            ']' => '',
            '/' => '',
            ' '=>'-',
            ":"=>"-",
            ";"=>"-",
            "<"=>"-",
            ">"=>"-",
            "+"=>"-",
            "@"=>"",
            "!"=>"",
            "("=>"",
            ")"=>"",
            "*"=>"",
            "#"=>"",
            "^"=>"",
            "&"=>"",
            ));
            //$title = preg_replace("/[^a-zA-Ząćęńłóśźż0-9]/", "-", $title);
            //$title = preg_replace("/[ ]/", "-", $title);
            $title = preg_replace("/([-])+/", "-", $title);
            return $title;
    }

    static function clearUrlSpecChars($oryginalString) {
        $oryginalString = trim($oryginalString);
        $oryginalString = self :: utf2NonUml($oryginalString);
        $oryginalString = str_replace(" ", "-", $oryginalString);
        $oryginalString = str_replace("\n", "-", $oryginalString);
        $oryginalString = str_replace('/', "-", $oryginalString);
        $oryginalString = preg_replace("/[^A-Za-z0-9_-]/", "", $oryginalString);
        $oryginalString = strtolower($oryginalString);

        return $oryginalString;
    }

    static function utf2NonUml($text) {

        $trans = array (

                "\xC4\x81" => "a", // ?
        "\xC4\x82" => "a", // ?
        "\xC4\x83" => "a", // ?
        "\xC4\x84" => "a", // ?
        "\xC4\x85" => "a", // ?
        "\xC4\x86" => "c", //  ?
        "\xC4\x87" => "c", //  ?
        "\xC4\x8c" => "c", //  ?
        "\xC4\x8d" => "c", //  ?
        "\xC4\x8e" => "d", //  ?
        "\xC4\x8f" => "d", //  ?
        "\xC4\x90" => "d", //  ?
        "\xC4\x91" => "d", //  ?
        "\xC4\x98" => "e", //  ?
        "\xC4\x99" => "e", //  ?
        "\xC4\x9a" => "e", //  ?
        "\xC4\x9b" => "e", //  ?
        "\xC4\xb9" => "l", //  ?
        "\xC4\xba" => "l", //  ?
        "\xC4\xbd" => "l", //  ?
        "\xC4\xbe" => "l", //  ?
        "\xC3\x81" => "a", //?
        "\xC3\x82" => "a", //?
        "\xC3\x84" => "a", //?
        "\xC3\x87" => "c", //?
        "\xC3\x89" => "e", //?
        "\xC3\x8b" => "ee", //?
        "\xC3\x8d" => "i", // ?
        "\xC3\x8e" => "i", // ?
        "\xC3\x93" => "o", // ?
        "\xC3\x94" => "o", // ?
        "\xC3\x96" => "oe", // ?
        "\xC3\x9a" => "u", // ?
        "\xC3\x9c" => "ue", // ?
        "\xC3\x9d" => "y", // ?
        "\xC3\x9f" => "ss", // ?
        "\xC3\xa0" => "a", // a
        "\xC3\xa1" => "a", // ?
        "\xC3\xa2" => "a", // ?
        "\xC3\xa4" => "c", // ?
        "\xC3\xa7" => "c", // ?
        "\xC3\xa8" => "e", // e
        "\xC3\xa9" => "e", // ?
        "\xC3\xaa" => "e", // e
        "\xC3\xab" => "ee", // ?
        "\xC3\xac" => "i", // i
        "\xC3\xad" => "i", // ?
        "\xC3\xae" => "i", // ?
        "\xC3\xb3" => "o", // ?
        "\xC3\xb4" => "o", // ?
        "\xC3\xb6" => "oe", // ?
        "\xC3\xba" => "u", // ?
        "\xC3\xbc" => "ue", // ?
        "\xC3\xbd" => "y", // ?
        "\xC5\x81" => "l", //  ?
        "\xC5\x82" => "l", //  ?
        "\xC5\x83" => "n", //  ?
        "\xC5\x84" => "n", //  ?
        "\xC5\x87" => "n", //  ?
        "\xC5\x88" => "n", //  ?
        "\xC5\x90" => "o", //  ?
        "\xC5\x91" => "o", //  ?
        "\xC5\x94" => "r", //  ?
        "\xC5\x95" => "r", //  ?
        "\xC5\x98" => "r", //  ?
        "\xC5\x99" => "r", //  ?
        "\xC5\x9a" => "s", //  ?
        "\xC5\x9b" => "s", //  ?
        "\xC5\x9e" => "s", //  ?
        "\xC5\x9f" => "s", //  ?
        "\xC5\xa0" => "s", //  ?
        "\xC5\xa1" => "s", //  ?
        "\xC5\xa2" => "t", //  ?
        "\xC5\xa3" => "t", //  ?
        "\xC5\xa4" => "t", //  ?
        "\xC5\xa5" => "t", //  ?
        "\xC5\xae" => "u", //  ?
        "\xC5\xaf" => "u", //  ?
        "\xC5\xb0" => "u", //  ?
        "\xC5\xb1" => "u", //  ?
        "\xC5\xb9" => "z", //  ?
        "\xC5\xba" => "z", //  ?
        "\xC5\xbb" => "z", //  ?
        "\xC5\xbc" => "z", //  ?
        "\xC5\xbd" => "z", //  ?
        "\xC5\xbe" => "z" //  ?

        );
        return strtr($text, $trans);
    }

    /**
     * Is editable upload image
     */
    static function isEAUploadImage(&$uploadedFile) {
        return !empty($uploadedFile['size'])
        && $uploadedFile['error'] == 0
        && preg_match('/image\/pjpg|image\/pjpeg|image\/jpeg|image\/jpeg|image\/gif|image\/png/i', $uploadedFile['type']);
    }

    static function isDocDocument($file) {
    	$allowedArr = array("application/msword","application/pdf","application/octet-stream");
    	if(in_array($file['type'],$allowedArr)){
    		return true;
    	}
    	return false;
    }

    static function getMediaMineType($ext) {

        $types = array ();
        $types['mp3'] = 'audio/x-mp3';
        $types['wav'] = 'audio/x-wav';
        $types['mpega'] = 'audio/x-mpeg';
        $types['mpa2'] = 'audio/x-mpeg-2';
        $types['mp2a'] = 'audio/x-mpeg-2';
        $types['mmid'] = 'x-music/x-midi';
        $types['mid'] = 'x-music/x-midi';
        $types['wav'] = 'audio/x-wav';
        $types['wav'] = 'audio/x-wav';

        $types['mpeg'] = 'video/mpeg';
        $types['mpg'] = 'video/mpeg';
        $types['mpe'] = 'video/mpeg';
        $types['mpv2'] = 'video/mpeg-2';
        $types['mp2v'] = 'video/mpeg-2';
        $types['mov'] = 'video/quicktime';
        $types['qt'] = 'video/quicktime';
        $types['avi'] = 'video/x-msvideo';
        $types['movie'] = 'video/x-sgi-movie';
        $types['mpeg'] = 'video/mpeg';
        $types['mpeg'] = 'video/mpeg';

        $types['jpeg'] = 'image/jpeg';
        $types['jpg'] = 'image/jpeg';
        $types['gif'] = 'image/gif';
        $types['png'] = 'image/png';

        return !empty ($types[$ext]) ? $types[$ext] : 'application/octet-stream';
    }

    static function resizeImage($sourcefile, $dest_x_org, $dest_y_org, $targetfile, $jpegqual = 100, $filter = '',$fit=false) {

        $picsize = @getimagesize("$sourcefile");
        $source_x = $picsize[0];
        $source_y = $picsize[1];

        if (preg_match("/png/i", $picsize['mime'])) {
            $source_id = @imagecreatefrompng("$sourcefile");
        }
        elseif (preg_match("/gif/", $picsize['mime'])) {
            $source_id = @imagecreatefromgif("$sourcefile");
        } else {
            $source_id = @imagecreatefromjpeg("$sourcefile");
        }


        $wsp = min($dest_x_org / $source_x, $dest_y_org / $source_y);
        $dest_x = $wsp * $source_x;
        $dest_y = $wsp * $source_y;

        $target_id = @imagecreatetruecolor($dest_x, $dest_y);

        if (!@ imagecopyresampled($target_id, $source_id, 0, 0, 0, 0, $dest_x, $dest_y, $source_x, $source_y))
        return false;

        @ imagedestroy($source_id);


        if(!empty($filter)){

            switch ($filter) {
                case 'grayscale':
                    $target_id = self::filterImageGrayscale($target_id);
                    break;
                default:
                    break;
            }
        }

        if($fit){

            $source_id = $target_id;
            $target_id = imagecreatetruecolor($dest_x_org, $dest_y_org);
            $color = self::getRgbFromGd($fit);

            $color = @imagecolorallocate($target_id,$color[0],$color[1],$color[2]);
            @imagefill($target_id,0,0,$color);

            //checking where to start
            $start_x = 0;
            $start_y = 0;
            if($dest_x_org > $dest_x){
                $start_x = round(($dest_x_org - $dest_x)/2);
            } else {
                $start_y = round(($dest_y_org - $dest_y)/2);
            }


            if (!@ imagecopy($target_id, $source_id, $start_x, $start_y, 0, 0, $dest_x, $dest_y))
            return false;

            @ imagedestroy($source_id);

        }


        if (preg_match("/png/i", $picsize['mime'])) {
            ob_start();
            header("Content-type: image/png");
            @imagepng($target_id);
            $imagevariable = ob_get_contents();
            ob_end_clean();
            if (($fd = fopen($targetfile, "w"))) {
                fwrite($fd, $imagevariable);
                fclose($fd);
            } else
            return false;
        }
        elseif (preg_match("/gif/", $picsize['mime'])) {
            if (function_exists("imagegif")) {
                ob_start();
                header("Content-type: image/gif");
                @imagegif($target_id);
                $imagevariable = ob_get_contents();
                ob_end_clean();
            } else {
                ob_start();
                header("Content-type: image/png");
                @imagepng($target_id);
                $imagevariable = ob_get_contents();
                ob_end_clean();
            }
            if (($fd = fopen($targetfile, "w"))) {
                fwrite($fd, $imagevariable);
                fclose($fd);
            } else
            return false;
        } else {
           $res = imagejpeg($target_id, $targetfile, $jpegqual);
        }
        
        return array (
        $dest_x,
        $dest_y
        );
    }

    static function cropImage($sourcefile, $dest_x, $dest_y, $targetfile, $jpegqual = 100, $startCrop = 'center', $filter='') {

        $picsize = @getimagesize("$sourcefile");
        $source_x = $picsize[0];
        $source_y = $picsize[1];
        $source_id = null;
        if (preg_match("/png/i", $picsize['mime'])) {
            $source_id = @imagecreatefrompng("$sourcefile");
        }
        elseif (preg_match("/gif/i", $picsize['mime'])) {
            $source_id = @imagecreatefromgif("$sourcefile");
        } else {
            $source_id = @imagecreatefromjpeg("$sourcefile");
        }

        $target_id = @imagecreatetruecolor($dest_x, $dest_y);

        if (!$target_id)
        return false;

        if ($source_y < $source_x) {
            $ratio = (double) ($source_y / $dest_y);

            $cpyWidth = round($dest_x * $ratio);
            if ($cpyWidth > $source_x) {
                $ratio = (double) ($source_x / $dest_x);
                $cpyWidth = $source_x;
                $cpyHeight = round($dest_y * $ratio);
                $xOffset = 0;
                $yOffset = round(($source_y - $cpyHeight) / 2);
            } else {
                $cpyHeight = $source_y;
                $xOffset = round(($source_x - $cpyWidth) / 2);
                $yOffset = 0;
            }

        } else {
            $ratio = (double) ($source_x / $dest_x);

            $cpyHeight = round($dest_y * $ratio);
            if ($cpyHeight > $source_y) {
                $ratio = (double) ($source_y / $dest_y);
                $cpyHeight = $source_y;
                $cpyWidth = round($dest_x * $ratio);
                $xOffset = round(($source_x - $cpyWidth) / 2);
                $yOffset = 0;
            } else {
                $cpyWidth = $source_x;
                $xOffset = 0;
                $yOffset = round(($source_y - $cpyHeight) / 2);
            }
        }
        if($startCrop == 'topleft') {
            $xOffset = 0;
            $yOffset = 0;
        } elseif($startCrop == 'top') {
            $yOffset = 0;
        } elseif($startCrop == 'left') {
            $xOffset = 0;
        } else {
            // do nothing $startCrop = 'center'
        }
        //echo "$xOffset, $yOffset, $dest_x, $dest_y, $cpyWidth, $cpyHeight";
        //exit;
        if (!@ imagecopyresampled($target_id, $source_id, 0, 0, $xOffset, $yOffset, $dest_x, $dest_y, $cpyWidth, $cpyHeight))
        return false;

        @ imagedestroy($source_id);

        if(!empty($filter)){

            switch ($filter) {
                case 'grayscale':
                    $target_id = self::filterImageGrayscale($target_id);
                    break;
                default:
                    break;
            }
        }
        if (preg_match("/png/i", $picsize['mime'])) {
            ob_start();
            header("Content-type: image/png");
            @imagepng($target_id);
            $imagevariable = ob_get_contents();
            ob_end_clean();
            if (($fd = fopen($targetfile, "w"))) {
                fwrite($fd, $imagevariable);
                fclose($fd);
            } else
            return false;
        }
        elseif (preg_match("/gif/i", $picsize['mime'])) {
            if (function_exists("imagegif")) {
                ob_start();
                header("Content-type: image/gif");
                @imagegif($target_id);
                $imagevariable = ob_get_contents();
                ob_end_clean();
            } else {
                ob_start();
                header("Content-type: image/png");
                @imagepng($target_id);
                $imagevariable = ob_get_contents();
                ob_end_clean();
            }
            if (($fd = fopen($targetfile, "w"))) {
                fwrite($fd, $imagevariable);
                fclose($fd);
            } else
            return false;
        } else {
            @imagejpeg($target_id, $targetfile, $jpegqual);
        }
        return array (
        $dest_x,
        $dest_y
        );

    }

    static function getRgbFromGd($color_hex) {

        return array_map('hexdec', explode('|', wordwrap($color_hex, 2, '|', 1)));

    }

    static function checkDirsExistAndCreate ($dir, $baseDir) {
        if(!is_dir($dir) && is_dir($baseDir)) {
            @mkdir ( $dir, 0777, true);
            chmod($dir, 0777);
        }
        return is_dir($dir);
    }

    static function checkDirExistAndCreate($dir) {
        if (!is_dir($dir)) {
            @ mkdir($dir, 0777,true);
            @ chmod($dir, 0777);
        }
        return is_dir($dir);
    }

    static function getFileDirPath($path) {
        $path = str_replace("\\", "/", $path);
        $e = explode("/", $path);
        $file = $e[count($e) - 1];
        $path = str_replace('/' . $file, '', $path);
        return $path;
    }
    
    static function filterImageGrayscale($resource){

        $x = imagesx($resource);
        $y = imagesy($resource);

        for($i=0; $i<$y; $i++)
        {
            for($j=0; $j<$x; $j++)
            {
                $pos = imagecolorat($resource, $j, $i);
                $f = imagecolorsforindex($resource, $pos);
                $gst = $f['red']*0.15 + $f['green']*0.5 + $f['blue']*0.35;
                $col = imagecolorresolve($resource, $gst, $gst, $gst);
                imagesetpixel($resource, $j, $i, $col);
            }
        }

        return $resource;
    }


}
?>