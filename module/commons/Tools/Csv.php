<?php
/**
 * This class provides comma separated values files manage
 *
 * @author Grzegorz Ciepiela
 *
 */


class Tools_Csv {

	/**
	 * This file contains csv data
	 * @private
	 */
	private $arrFileContent = array();

	/**
	 * Delimiter separate each record in row
	 * @private
	 */
	private $strDelimiter;

	/**
	 * Enclousure for string records
	 *
	 * @private
	 */
	private $strEnclosure;

	/**
	 * Max length of row in CSV file
	 *
	 * @private
	 */
	private $intRowLength;

	/**
	 * Rows counter
	 *
	 * @private
	 */
 	private $intRowCount = 0;

 	/**
 	 * Columns names
 	 *
 	 * @private
 	 */
 	private $arrColumnNames = null;

	/**
	 * First row type
	 * @see class consts
	 */
	private $intType = 1;

	/**
	 * First row constains real data
	 */
	const CSV_FIRST_ROW_DATA = 1;

	/**
	 * First row contains column names
	 */
	const CSV_FIRST_ROW_HEADER = 2;

	/**
	 * First row contaion useless data
	 */
	const CSV_FIRST_ROW_SKIP = 3;

	public function __construct($intRowLength = 4096, $strDelimiter = ',',$strEnclosure = '"')
	{
		$this->strDelimiter = $strDelimiter;
		$this->strEnclosure = $strEnclosure;
		$this->intRowLength = $intRowLength;
	}

	/**
	 * Load data from file
	 *
	 * @param string $strFileName
	 * @param boolean $blnFirstRowColsName First row contains (or not) column names
	 * @param integer $intKeyColumn Index of key column
	 */
	public function load ($strFileName, $intFirstRowType = self::CSV_FIRST_ROW_DATA,$strKeyColumn = null) {

		$this->arrFileContent = array();

		if (!is_file($strFileName)) {
			throw new Exception('Unable to find file: '. $strFileName);
		}

		$resFile = fopen($strFileName,'r');
		if (!$resFile) {
			throw new Exception('Unable to read file: '. $strFileName);
		}

		$this->setType($intFirstRowType);

		while (($arrData = fgetcsv($resFile, $this->intRowLength, $this->strDelimiter, $this->strEnclosure)) !== FALSE)  {
			if (empty($strKeyColumn) && $strKeyColumn !== 0) {
				$this->arrFileContent[] = $arrData;
			} else {
				$this->arrFileContent[$arrData[$strKeyColumn]] = $arrData;
			}
		}

		switch ($intFirstRowType) {
			case self::CSV_FIRST_ROW_HEADER:
				$this->arrColumnNames = reset($this->arrFileContent);
				unset($this->arrFileContent[0]);
				break;
			case self::CSV_FIRST_ROW_SKIP:
				array_shift($this->arrFileContent);
				break;
		}

		fclose ($resFile);

		$this->intRowCount = count($this->arrFileContent);

	}

	/**
	 * Add row
	 *
	 * @param array Data row
	 */
	public function addRow($arrData)
	{
		$this->arrFileContent[] = $arrData;
	}

	/**
	 * Add many rows
	 */
	public function addRows($arrRows)
	{
		foreach($arrRows as $arrRow) {
			$this->addRow($arrRow);
		}
	}

	/**
	 * Update row of given index
	 *
	 * @param integer $intRowId
	 * @param array $arrNewValue New row value
	 */
	public function updateRow($intRowId, $arrNewValue)
	{
		if (!empty($this->arrFileContent[$intRowId])) {
			$this->arrFileContent[$intRowId] = $arrNewValue;
			return true;
		}
		return false;
	}

	public function getRowByKey($intKey)
	{
		$arrCurrent = $this->arrFileContent[$intKey];
//		var_dump(array_keys($this->arrFileContent));
//		exit;
		if ($this->arrColumnNames && is_array($arrCurrent)) {
			return array_combine($this->arrColumnNames, $arrCurrent);
		} else {
			return $arrCurrent;
		}
	}

	/**
	 * Create cvs file
	 *
	 * if $strFilename param is empty - method return generated file as string
	 */
	public function create($strFileName = null) {
		if ($strFileName) {
			$resFile = fopen($strFileName,'w');
			if (!$resFile) {
				throw new Exception('Unable to create file');
			}
		} else {
			$resFile = fopen('php://memory','r+');
			if (!$resFile) {
				throw new Exception('Unable to store data in memory');
			}
		}

		foreach($this->arrFileContent as & $arrRow) {
			fputcsv($resFile,$arrRow,$this->strDelimiter,$this->strEnclosure);
		}

		if ($strFileName) {
			fclose($resFile);
			return true;
		} else {
			rewind($resFile);
			$strResult = stream_get_contents($resFile);
			fclose($resFile);
			return $strResult;
		}
	}
	/**
	 * Set data header type
	 *
	 * @param integer $intType
	 * @throws Exception Unknown type
	 */
	public function setType($intType)
	{
		switch ($intType) {
			case self::CSV_FIRST_ROW_DATA:
			case self::CSV_FIRST_ROW_HEADER:
			case self::CSV_FIRST_ROW_SKIP:
				$this->intType = $intType;
				return;
				break;
		}
		throw new Exception('Unknown first row type !: '.$intType);
	}

	public function getType()
	{
		return $this->intType;
	}

	public function rewind()
	{
		reset($this->arrFileContent);
	}

	public function current()
	{
		$arrCurrent = current($this->arrFileContent);
		if ($this->arrColumnNames && is_array($arrCurrent)) {
			return array_combine($this->arrColumnNames, $arrCurrent);
		} else {
			return $arrCurrent;
		}
	}


	public function key()
	{
		return $this->arrColumnNames;
	}

	public function next()
	{
		$arrNext = next($this->arrFileContent);
		if ($this->arrColumnNames && is_array($arrNext)) {
			return array_combine($this->arrColumnNames,$arrNext);
		} else {
			return $arrNext;
		}
	}

	public function valid()
	{
		return $this->current() !== false;
	}

	public function getRow()
	{
		$arrDesc = reset($this->arrFileContent);
		$arrR = array();
		foreach($arrDesc as $strKey) {
			$arrR[$strKey] = "";
		}
		return $arrR;
	}

	public function count()
	{
		return count($this->arrFileContent);
	}
}
?>