<?php

namespace Console;

use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use Doctrine\ORM\Mapping\Driver\XmlDriver;
use Console\Module as Console;


use Zend\ModuleManager\ModuleManager;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ModuleManager\Feature\ServiceProviderInterface;
use Zend\Stdlib\Hydrator\ClassMethods;
use Zend\Mvc\MvcEvent;
use Zend\ModuleManager\Feature\ConsoleBannerProviderInterface;
use Zend\Console\Adapter\AdapterInterface as ConsoleAdapterInterface;
use Zend\ModuleManager\Feature\ConsoleUsageProviderInterface;

class Module implements ServiceProviderInterface, ConsoleBannerProviderInterface, ConsoleUsageProviderInterface {

    public function onBootstrap(MvcEvent $e) {
        $eventManager = $e->getApplication()->getEventManager();
    }

    public function init(ModuleManager $moduleManager) {
        //$sharedEvents = $moduleManager->getEventManager()->getSharedManager();
    }

    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getControllerPluginConfig() {
        
    }

    public function getServiceConfig() {
        return array(
            'invokables' => array(
                //'quiz_console_service' => 'Console\Service\Quiz',
            ),
            'factories' => array(
                'quiz_console_service' =>'Console\Factory\QuizFactory',
                'broadcast_service' => 'Console\Factory\BroadcastFactory',
                'question_calibration_service' => 'Console\Factory\QuestionCalibrationFactory',
                'console_module_options' => 'Console\Factory\ModuleOptionsFactory',
            ),
        );
    }

    public function getConfig() {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getConsoleBanner(ConsoleAdapterInterface $console) {
        return
                "==------------------------------------------------------==\n" .
                "                           QUIZ\n" .
                "==------------------------------------------------------==\n" .
                "Version 0.0.1\n"
        ;
    }

    public function getConsoleUsage(ConsoleAdapterInterface $console) {
        return array(
            'quiz generate' => 'Quiz wil be generated'
        );
    }

}

