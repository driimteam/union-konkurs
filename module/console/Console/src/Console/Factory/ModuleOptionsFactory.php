<?php

namespace Console\Factory;

use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\FactoryInterface;
use Console\Options\ModuleOptions;

class ModuleOptionsFactory implements FactoryInterface {

    public function createService(ServiceLocatorInterface $serviceLocator) {
        return new ModuleOptions($serviceLocator->get('Config')['console_options']);
    }

}
