<?php
$env = 'development';
return array(
    'modules' => array(
        'DoctrineModule',
        'DoctrineORMModule',
        'DoctrineExtensions',
        'ZfcBase',
        'ZfcUser',
        'ZfcUserDoctrineORM',
        'app',
        'UserApp',
        'QuizApp',
        'Console',
        'PaymentApp'
    ),
    'module_listener_options' => array(
        'config_glob_paths'    => array(
            '../../../../config/console/autoload/{,*.}{global,local}.php',
            '../../../../config/backend/autoload/{,*.}{global,local}.php',
            '../../../../config/backend/autoload/{,*.}' . $env. '.php',
            '../../../../config/backend/autoload/application/{,*.}{global,local}.php',
        ),
        'module_paths' => array(
            '../../../../module/console',
            '../../../../module/commons',
            '../../../../vendor/doctrine',
            '../../../../vendor/zf-commons',
            '../../../../vendor',
        ),
    ),
);