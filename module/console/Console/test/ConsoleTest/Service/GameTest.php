<?php

namespace ConsoleTest\Service;

use ConsoleTest\Bootstrap;
use PHPUnit_Framework_TestCase;
use Console\Options\ModuleOptions;
use QuizApp\Models\Entity\AppQuiz;
use QuizApp\Models\Entity\AppTable;
use QuizApp\Models\Entity\AppQuestion;
use QuizApp\Models\Entity\AppQuizQuestion;
use QuizApp\Models\Entity\AppSettings;
use QuizApp\Models\Entity\AppAnswer;

class GameTest extends PHPUnit_Framework_TestCase
{
    protected $serviceManager;
    protected $consoleQuizService;
    protected $quizAppService;
    protected $broadcastService;
    protected $moduleOptions;
    protected $tableMapper;
    protected $schemequestionMapper;
    protected $quiz;
    protected $schemequestions;
    protected $questionMapper;
    protected $quizMapper;
    protected $quizUserMapper;
    protected $quizQuestionMapper;
    protected $answerMapper;
    protected $questionEntity;
    protected $quizQuestionUserMapper;
    protected $quizSettingsMapper;

    protected function setUp()
    {

        $this->serviceManager     = Bootstrap::getServiceManager();
        $this->consoleQuizService = $this->serviceManager->get('quiz_console_service');
        $this->quizAppService     = $this->serviceManager->get('quizapp_service');
        $this->broadcastService   = $this->serviceManager->get('broadcast_service');
        $this->moduleOptions      = $this->serviceManager->get('console_module_options');
        $this->moduleOptions->setBroadcastServers(array());

        $this->tableMapper            = $this->getMockBuilder('\QuizApp\Models\Mapper\Table')->disableOriginalConstructor()->getMock();
        $this->quizMapper             = $this->getMockBuilder('\QuizApp\Models\Mapper\Quiz')->disableOriginalConstructor()->getMock();
        $this->schemequestionMapper   = $this->getMockBuilder('\QuizApp\Models\Mapper\Schemequestion')->disableOriginalConstructor()->getMock();
        $this->questionMapper         = $this->getMockBuilder('\QuizApp\Models\Mapper\Question')->disableOriginalConstructor()->getMock();
        $this->answerMapper           = $this->getMockBuilder('\QuizApp\Models\Mapper\Answer')->disableOriginalConstructor()->getMock();
        $this->quizQuestionMapper     = $this->getMockBuilder('\QuizApp\Models\Mapper\QuizQuestion')->disableOriginalConstructor()->getMock();
        $this->quizQuestionUserMapper = $this->getMockBuilder('\QuizApp\Models\Mapper\QuizQuestionUser')->disableOriginalConstructor()->getMock();
        $this->quizUserMapper         = $this->getMockBuilder('\QuizApp\Models\Mapper\QuizUser')->disableOriginalConstructor()->getMock();
        $this->quizSettingsMapper     = $this->getMockBuilder('\QuizApp\Models\Mapper\Settings')->disableOriginalConstructor()->getMock();

        $this->quizAppService->setTableMapper($this->tableMapper);
        $this->quizAppService->setQuizMapper($this->quizMapper);
        $this->quizAppService->setSchemequestionMapper($this->schemequestionMapper);
        $this->quizAppService->setQuestionMapper($this->questionMapper);
        $this->quizAppService->setAnswerMapper($this->answerMapper);
        $this->quizAppService->setQuizQuestionMapper($this->quizQuestionMapper);
        $this->quizAppService->setQuizQuestionUserMapper($this->quizQuestionUserMapper);
        $this->quizAppService->setQuizUserMapper($this->quizUserMapper);
        $this->quizAppService->setQuizSettingsMapper($this->quizSettingsMapper);

        $this->consoleQuizService->setQuizAppService($this->quizAppService);


        $this->prepareObjects();
        $this->prepareMockMappersMethods();
    }

    protected function prepareObjects()
    {
        $startTime             = 2;
        $this->table           = $this->prepareTable();
        $this->quiz            = $this->prepareQuiz($this->table, $startTime);
        $this->schemequestions = $this->prepareSchemes($this->table->getQuestionsLimit());
        $this->quiz->setSchemeQuestions($this->schemequestions);

        $this->questionEntity = $this->prepareQuestionEntity();
    }

    protected function prepareMockMappersMethods()
    {
        $this->prepareMockQuestionMapper($this->questionEntity);
        $this->prepareMockQuizQuestionMapper($this->questionEntity, $this->quiz,
            1);
        $this->prepareMockQuizQuestionUserMapper($this->questionEntity,
            $this->quiz);
        $this->prepareMockQuizUserMapper();
        $this->prepareMockQuizSettingsMapper();
    }

    protected function prepareMockQuizSettingsMapper()
    {

        $settingsEntity = new AppSettings();
        $settingsEntity->setCreateNewCumulationQuiz(false);
        $settingsEntity->setCreateNewFreeQuiz(false);
        $settingsEntity->setCreateNewQuiz(false);

        $this->quizSettingsMapper->expects($this->any())
            ->method('getSettings')
            ->will($this->returnValue($settingsEntity));
    }

    protected function prepareMockQuizUserMapper()
    {
        $this->quizUserMapper->expects($this->any())
            ->method('getNrUsersInQuiz')
            ->with($this->quiz->getId())
            ->will($this->returnValue(12));

        $this->quizUserMapper->expects($this->any())
            ->method('save')
            ->with($this->quiz)
            ->will($this->returnValue(true));

        $this->quizUserMapper->expects($this->any())
            ->method('getAllUsersByQuiz')
            ->with($this->quiz->getId())
            ->will($this->returnValue(array()));
    }

    protected function prepareMockQuizMapper()
    {
        $this->quizMapper->expects($this->any())
            ->method('save')
            ->with($this->quiz)
            ->will($this->returnValue(true));
    }

    //$this->getQuizQuestionUserMapper()->countCorrectAnswers($this->quiz->getId(), $this->questionEntity->getId());

    protected function prepareMockQuizQuestionUserMapper($questionEntity, $quiz)
    {
        $this->quizQuestionUserMapper->expects($this->any())
            ->method('countCorrectAnswers')
            ->with($quiz->getId(), $questionEntity->getId())
            ->will($this->returnValue(12));

        $this->quizQuestionUserMapper->expects($this->any())
            ->method('updateNrOfLosers')
            ->will($this->returnValue(true));

        $this->quizQuestionUserMapper->expects($this->any())
            ->method('getUsersByQuizAndQuestionId')
            ->with(1, 1)
            ->will($this->returnValue(array()));
    }

    protected function prepareMockQuizQuestionMapper($questionEntity, $quiz,
                                                     $position)
    {
        $dateObject   = new \DateTime('now');
        $quizQuestion = new AppQuizQuestion();
        $quizQuestion->setQuestion($questionEntity);
        $quizQuestion->setQuiz($quiz);
        $quizQuestion->setCreatedAt($dateObject);
        $quizQuestion->setPosition($position);

        $this->quizQuestionMapper->expects($this->any())
            ->method('createRelation')
            ->will($this->returnValue($quizQuestion));

        $this->quizQuestionMapper->expects($this->any())
            ->method('save')
            ->will($this->returnValue(true));


        $dateObject   = new \DateTime('now');
        $quizQuestion = new AppQuizQuestion();
        $quizQuestion->setQuestion($questionEntity);
        $quizQuestion->setQuiz($quiz);
        $quizQuestion->setCreatedAt($dateObject);
        $quizQuestion->setPosition(3);

        $this->quizQuestionMapper->expects($this->any())
            ->method('getWinnerQuizQuestion')
            ->with($this->quiz)
            ->will($this->returnValue($quizQuestion));
    }

    protected function prepareQuestionEntity()
    {
        $scheme = $this->getMockBuilder('\QuizApp\Models\Entity\AppSchemequestion')->disableOriginalConstructor()->getMock();

        $questionEntity = new AppQuestion();
        $questionEntity->setId(1);
        $questionEntity->setStatus("ACTIVE");
        $questionEntity->setTitle("Tytul pytania:");
        $questionEntity->setDifficulty(1);

        $this->questionMapper->expects($this->any())
            ->method('getQuestionByScheme')
            ->will($this->returnValue($questionEntity));

        $answers = array();
        for ($i = 0; $i < 4; $i++) {
            $answerEntity = new AppAnswer();
            $answerEntity->setId($i + 1);
            $answerEntity->setQuestion($questionEntity);
            $answerEntity->setTitle("Title: nr ".$i + 1);
            if ($i == 0) {
                $answerEntity->setIsCorrect(1);
            }
            $answers[] = $answerEntity;
        }
        $questionEntity->setAnswers($answers);

        $this->answerMapper->expects($this->any())
            ->method('findBy')
            ->with(array('question' => $questionEntity), "sort asc, id asc")
            ->will($this->returnValue($answers));

        return $questionEntity;
    }

    protected function prepareMockQuestionMapper($questionEntity)
    {

        
    }

    protected function prepareSchemes($nrSchemes)
    {
        $arr = array();
        for ($i = 0; $i < $nrSchemes; $i++) {
            $category = $this->getMockBuilder('QuizApp\Models\Entity\AppCategory')->getMock();
            $category->expects($this->any())
                ->method('getName')
                ->will($this->returnValue("Historia"));

            $schemeObject = $this->getMockBuilder('QuizApp\Models\Entity\AppSchemequestion')->getMock();
            $schemeObject->expects($this->any())
                ->method('getCategory')
                ->will($this->returnValue($category));

            $arr[] = $schemeObject;
        }
        return $arr;
    }

    protected function prepareQuiz($table, $startTime)
    {
        $timestamp = time();

        $quiz = new AppQuiz();
        $quiz->setId(1);
        $quiz->setType($table->getType());
        $quiz->setTable($table);
        $quiz->setCreateDatetime(new \DateTime(date('Y-m-d H:i:s', $timestamp)));
        $quiz->setStartDatetime(new \DateTime(date('Y-m-d H:i:s',
                $timestamp -2)));
        $quiz->setStatus(AppQuiz::QUIZ_STATUS_WAITING);
        return $quiz;
    }

    protected function prepareTable()
    {
        $table = new AppTable();
        $table->setId(1);
        $table->setType("NORMAL");
        $table->setStatus("ACTIVE");
        $table->setQuestionInterval(3);
        $table->setRespondTime(2);
        $table->setQuestionsLimit(7);
        $table->setNrLastSequence(2);
        $table->setStartToNextQuiz(1);
        return $table;
    }

    public function testStartNormalGame()
    {
        //nic nie moze zapisywac sie do bazy
        //$this->quiz->getSecondsToStart()
        //$this->quiz = $quiz;
        //$this->table = $this->quiz->getTable();
        //$this->schemeQuestions = $this->quiz->getSchemeQuestions();
        //$this->startToNextGame = $this->table->getStartToNextQuiz();
        //$this->respondTime = $this->table->getRespondTime();
        //$this->questionIntervalTime = $this->table->getQuestionInterval();
        //this->timeToStartNextQuestion = $this->respondTime + $this->questionIntervalTime;
        //this->questionsLimit = $this->table->getQuestionsLimit();
        $game = new \Console\Service\Game($this->quiz,
            $this->consoleQuizService, $this->quizAppService,
            $this->broadcastService);
        $game->init();
        return $game;
    }
}