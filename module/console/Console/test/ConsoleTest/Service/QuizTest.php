<?php

namespace ConsoleTest\Service;

use ConsoleTest\Bootstrap;
use PHPUnit_Framework_TestCase;
use Console\Options\ModuleOptions;
use QuizApp\Models\Entity\AppQuiz;
class QuizTest extends PHPUnit_Framework_TestCase
{

    protected $serviceManager;
    protected $consoleQuizService;
    protected $quizAppService;
    protected $broadcastService;
    protected $moduleOptions;
    protected $tableMapper;
    protected $schemequestionMapper;

    protected function setUp()
    {
        $this->serviceManager = Bootstrap::getServiceManager();
        $this->consoleQuizService = $this->serviceManager->get('quiz_console_service');
        
        //$this->quizAppService = $this->getMockBuilder('\QuizApp\Service\Quiz')->getMock();
        $this->quizAppService = $this->serviceManager->get('quizapp_service');
        
        $this->tableMapper = $this->getMockBuilder('\QuizApp\Models\Mapper\Table')->disableOriginalConstructor()->getMock();
        $this->quizMapper = $this->getMockBuilder('\QuizApp\Models\Mapper\Quiz')->disableOriginalConstructor()->getMock();
        $this->schemequestionMapper = $this->getMockBuilder('\QuizApp\Models\Mapper\Schemequestion')->disableOriginalConstructor()->getMock();
        
        
        $this->quizAppService->setTableMapper($this->tableMapper);
        $this->quizAppService->setQuizMapper($this->quizMapper);
        $this->quizAppService->setSchemequestionMapper($this->schemequestionMapper);
        
        $this->consoleQuizService->setQuizAppService($this->quizAppService);
        
        $this->broadcastService = $this->serviceManager->get('broadcast_service');
        $this->moduleOptions = new ModuleOptions($this->serviceManager->get('Config')['console_options']);
    }
    
   /* public function testSetGameOptionsAction()
    {
        $optionsArray = $this->moduleOptions->toArray();
        $this->assertTrue(is_array($optionsArray));
        $this->assertTrue(!empty($optionsArray));
    }
    
    public function testGenerateQuiz(){
       $secToStart=100;
       $typeGame = AppQuiz::TYPE_NORMAL;
       $appQuiz =  $this->getMockBuilder('QuizApp\Models\Entity\AppQuiz')->getMock();       
       $appTable =  $this->getMockBuilder('QuizApp\Models\Entity\AppTable')->getMock();
        
       $this->tableMapper->expects($this->any())
                ->method('getTableToQuiz')
                ->with($typeGame)
                ->will($this->returnValue($appTable));
       
        $this->quizMapper->expects($this->any())
                ->method('createQuiz')
                ->with($appTable, $secToStart, $typeGame)
                ->will($this->returnValue($appQuiz));
        
        $schemquestions = array(1);
        $this->schemequestionMapper->expects($this->any())
                ->method('getFullSchemeQuestions')
                ->with($appTable)
                ->will($this->returnValue($schemquestions));
       
        $appQuiz->expects($this->any())
                ->method('setSchemeQuestions')
                ->with($schemquestions);
        
        $quiz = $this->consoleQuizService->generateQuiz($secToStart, $typeGame);
        
        $this->assertTrue(is_object($quiz));
    }*/
    
   /* public function testStartNormalGame(){
        
        $sec=100;
        $appQuiz =  $this->getMockBuilder('QuizApp\Models\Entity\AppQuiz')->getMock();       
        $appTable =  $this->getMockBuilder('QuizApp\Models\Entity\AppTable')->getMock();
        
        $appQuiz->expects($this->any())
                ->method('getTable')
                ->will($this->returnValue($appTable));
        
        $mockConsoleService =  $this->getMockBuilder('quiz_console_service')->getMock();       
        
        $mockConsoleService->expects($this->any())
                ->method('generateQuiz')
                ->will($this->returnValue($appQuiz));
        
        $mockGame = $this->getMockBuilder('\Console\Service\Game')
                ->setConstructorArgs(array($appQuiz,$mockConsoleService, $this->quizAppService, $this->broadcastService))
                ->getMock();
        
        $mockGame->expects($this->any())
                ->method('init')
                ->will($this->returnValue(true));
        
        $mockConsoleService->expects($this->any())
                ->method('startNormalGame')
                ->with(100);
         
        //$game = $this->consoleQuizService->startNormalGame($sec);
        $this->assertInstanceOf('\Console\Service\Game', $mockGame);
    }*/
    
   /* public function testExecCommandShell(){
        $secToStart=100;
        
        
        
        
        $mockConsoleService =  $this->getMockBuilder('quiz_console_service')->getMock();       
        
        $mockConsoleService->expects($this->any())
                ->method('generateQuiz')
                ->will($this->returnValue($appQuiz));
        
        $mockGame = $this->getMockBuilder('\Console\Service\Game')
                ->setConstructorArgs(array($appQuiz,$mockConsoleService, $this->quizAppService, $this->broadcastService))
                ->getMock();
                
        $mockConsoleService->expects($this->any())
                ->method('startNormalGame')
                ->will($this->returnValue($mockGame));
        
//        
//        
//        $mockGame->expects($this->any())
//                ->method('init')
//                ->will($this->returnValue(true));
//         
        $this->assertInstanceOf('\Console\Service\Game', $mockGame);
        $this->assertEquals($type, $mockGame->getType(), "typy takie same");
        
        //$this->assertInstanceOf('\Console\Service\Game', $object);
    }*/

}