<?php
namespace Admin;
return array(
    'doctrine' => array(
        'driver' => array(
             'zfcuser_driver' =>array(
                'class' => 'Doctrine\ORM\Mapping\Driver\XmlDriver',
                'cache' => 'array',
                'paths' => array(__DIR__ .'/xml/orm')
            ),

            'orm_default' =>array(
                'drivers' => array(
                    'Admin\Models\Entity'  =>  'zfcuser_driver',
                    'ZfcUserDoctrineORM\Entity'  =>  'zfcuser_driver'
                )
            )
        )
    ),
    'controllers' => array(
        'invokables' => array(
            'Admin\Controller\Index' => 'Admin\Controller\IndexController',
            'Admin\Controller\Admin' => 'Admin\Controller\AdminController',
            'Admin\Controller\Roles' => 'Admin\Controller\RolesController',
            'Admin\Controller\Permissions' => 'Admin\Controller\PermissionsController',
        ),
    ),
    'service_manager' => array(
        'factories' => array(
            'Admin\Authorization\RbacListener' => 'Admin\Factory\RbacListenerFactory',
            
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'zfcuser' => __DIR__ . '/../view',
        ),
    ),
    
    'view_manager' => array(
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
     'view_helpers' => array(
        'invokables' => array(
            'printPermissionsTree' => '\Admin\View\Helper\NodePermissionsTree'
        )
    ),
);

