delimiter $$

CREATE TABLE `cms_admins` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `display_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `state` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `UNIQ_90160A77F85E0677` (`username`),
  UNIQUE KEY `UNIQ_90160A77E7927C74` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci$$

CREATE TABLE `rbac_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_487801545E237E06` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci$$

CREATE TABLE `rbac_admin_role` (
  `admin_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`admin_id`,`role_id`),
  KEY `IDX_E7AFD763642B8210` (`admin_id`),
  KEY `IDX_E7AFD763D60322AC` (`role_id`),
  CONSTRAINT `FK_E7AFD763642B8210` FOREIGN KEY (`admin_id`) REFERENCES `cms_admins` (`user_id`),
  CONSTRAINT `FK_E7AFD763D60322AC` FOREIGN KEY (`role_id`) REFERENCES `rbac_roles` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci$$


CREATE TABLE `rbac_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci$$

CREATE TABLE `rbac_role_permission` (
  `role_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`role_id`,`permission_id`),
  KEY `IDX_C31A0CF0D60322AC` (`role_id`),
  KEY `IDX_C31A0CF0FED90CCA` (`permission_id`),
  CONSTRAINT `FK_C31A0CF0D60322AC` FOREIGN KEY (`role_id`) REFERENCES `rbac_roles` (`id`),
  CONSTRAINT `FK_C31A0CF0FED90CCA` FOREIGN KEY (`permission_id`) REFERENCES `rbac_permissions` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci$$


#haslo: testowe
INSERT INTO `cms_admins` (`username`, `email`, `display_name`, `password` , `state`) VALUES ('Admin', 'admin@s4.pl', 'Admin', '$2y$12$x3E76SUCO73492XFWFbvouB1c/lUlJV.rMH8sdSkX6PYYlTc0aA8G', 1)$$
INSERT INTO `rbac_roles` (`name`) VALUES ('guest'),('admin')$$
INSERT INTO `rbac_admin_role` (`admin_id`, `role_id`) VALUES ('1', '2')$$
