<?php

namespace Admin;

use Admin\Module as Admin;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use Doctrine\ORM\Mapping\Driver\XmlDriver;
use Zend\ModuleManager\ModuleManager;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ModuleManager\Feature\ServiceProviderInterface;
use Zend\Stdlib\Hydrator\ClassMethods;
use Zend\Mvc\MvcEvent;
use Zend\View\Helper;
use Zend\Mvc\ModuleRouteListener;

class Module implements ServiceProviderInterface {

    public function onBootstrap(MvcEvent $e) {
        $eventManager = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
        $sharedEvents = $eventManager->getSharedManager();
        
        $rbacListener = $e->getApplication()->getServiceManager()->get('Admin\Authorization\RbacListener');
        $sharedEvents->attach(
                'Zend\View\Helper\Navigation\AbstractHelper', 'isAllowed', array($rbacListener, 'accept')
        );
        $t = $e->getTarget();
        $t->getEventManager()->attach(
                $t->getServiceManager()->get('ZfcRbac\View\Strategy\RedirectStrategy')
        );

        
        $sharedEvents->attach('ZfcUser', MvcEvent::EVENT_DISPATCH, array($this, 'switchLayout'), 4);
    }

    public function init(ModuleManager $moduleManager) {
        $sharedEvents = $moduleManager->getEventManager()->getSharedManager();
        $sharedEvents->attach(__NAMESPACE__, MvcEvent::EVENT_DISPATCH, array($this, 'initSubmenu'), 2);
    }

    public function initSubmenu(MvcEvent $e) {
        $service = $e->getApplication()->getServiceManager()->get('application_service');
        $service->setSubmenu(null, 'admin');
    }

    public function switchLayout(MvcEvent $e) {
        $match = $e->getRouteMatch();
        if ($match->getMatchedRouteName() == 'zfcuser/login') {
            $model = $e->getViewModel();
            $model->setTemplate('layout/empty');
        }

        return;
    }

    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getControllerPluginConfig() {
        return array(
            'factories' => array(
            ),
        );
    }

    public function getServiceConfig() {
        $module = $this;
        return array(
            'invokables' => array(
                'Admin\Authentication\Adapter\Db' => 'Admin\Authentication\Adapter\Db',
                'admin_service' => 'Admin\Service\Admin'
            ),
            'aliases' => array(
            ),
            'factories' => array(
                'zfcuser_user_mapper' => function ($sm) {
                    $options = $sm->get('zfcuser_module_options');
                    $options->setEnableDefaultEntities(false);
                    $em = $sm->get('Doctrine\ORM\EntityManager');
                    $mapper = new \Admin\Models\Mapper\Admin($em, $options);
                    return $mapper;
                },
                'admin_role_mapper' => function ($sm) {
                    $em = $sm->get('Doctrine\ORM\EntityManager');
                    $options = array();
                    $mapper = new \Admin\Models\Mapper\Role($em, $options);
                    return $mapper;
                },
                'admin_permission_mapper' => function ($sm) {
                    $em = $sm->get('Doctrine\ORM\EntityManager');
                    $options = array();
                    $mapper = new \Admin\Models\Mapper\Permission($em, $options);
                    return $mapper;
                },
            ),
            'initializers' => array(
                'ZfcRbac\Initializer\AuthorizationServiceInitializer'
            )
        );
    }

    public function getConfig() {
        return include __DIR__ . '/config/module.config.php';
    }

}