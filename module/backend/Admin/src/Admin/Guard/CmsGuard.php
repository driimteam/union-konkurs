<?php
namespace Admin\Guard;

use Zend\Http\Request as HttpRequest;
use Zend\Mvc\MvcEvent;
use ZfcRbac\Guard\AbstractGuard;

class CmsGuard extends AbstractGuard
{
    const EVENT_PRIORITY = 100;

    public function __construct()
    {
        
    }

    /**
     * @param  MvcEvent $event
     * @return bool
     */
    public function isGranted(MvcEvent $event)
    {
        $request = $event->getRequest();

        if (!$request instanceof HttpRequest) {
            return true;
        }

       //echo "dSA";exit;

        return true;
    }
}