<?php

namespace Admin\Controller;

use Zend\Mvc\Controller\AbstractActionController,
    Zend\View\Model\ViewModel,
    Doctrine\ORM\EntityManager,
    Admin\Form\RoleForm,
    Admin\Models\Entity\Role;

class RolesController extends AbstractActionController {

    
    protected $adminService;
    protected $application_service;
    protected $zfcUserService;

    public function setEventManager(\Zend\EventManager\EventManagerInterface $events) {
        parent::setEventManager($events);
        $this->getApplicationService()->setLeftMenu('roles');
        return $this;
    }
    
    public function getZfcUserService()
    {
        if (!$this->zfcUserService) {
            $this->zfcUserService = $this->getServiceLocator()->get('zfcuser_user_service');
        }
        return $this->zfcUserService;
    }

    public function getApplicationService()
    {
        if (!$this->application_service) {
            $this->application_service = $this->getServiceLocator()->get('application_service');
        }
        return $this->application_service;
    }

    public function getAdminService()
    {
        if (!$this->adminService) {
            $this->adminService = $this->getServiceLocator()->get('admin_service');
        }
        return $this->adminService;
    }
    
    public function indexAction(){
        $roleMapper=$this->getAdminService()->getRoleMapper();
        $roles = $roleMapper->findBy();
        
        return new ViewModel(array('roles' => $roles));
    }
    
    public function addAction(){
        $viewModel= new ViewModel();
        $request = $this->getRequest();
        $serviceAdmin = $this->getAdminService();
        $mapperRole = $serviceAdmin->getRoleMapper();  
        $role = new Role();
        $form = new RoleForm($this->getServiceLocator());
        $form->bind($role);
        
        
        if ($request->isPost()) {
                $form->setData($request->getPost());
                if ($form->isValid()) {
                    $dataObject = $form->getData();
                    $mapperRole->save($role);
                    $this->flashMessenger()->addSuccessMessage('Rola została dodana poprawnie');
                    return $this->redirect()->toRoute('admin/roles',array());
                } else {
                    $this->flashMessenger()->addErrorMessage('Rola nie została dodana poprawnie');
                }
            }
        $viewModel->form =$form;
        return $viewModel;
        
    }
    
    public function editAction(){
         if ($roleId = $this->getEvent()->getRouteMatch()->getParam('roleId')) {
                $viewModel= new ViewModel();
                $request = $this->getRequest();
                $serviceAdmin = $this->getAdminService();
                
                
                $mapperRole = $serviceAdmin->getRoleMapper();  
                $role = $mapperRole->find($roleId);
                if($role){
                    $form = new RoleForm($this->getServiceLocator());
                    $form->bind($role);
                    if ($request->isPost()) {
                            $form->setData($request->getPost());
                            if ($form->isValid()) {
                                $mapperRole->save($role);
                                $this->flashMessenger()->addSuccessMessage('Rola została wyedytowana poprawnie');
                                return $this->redirect()->toRoute('admin/roles',array('action'=>'edit','roleId'=>$roleId));
                            } else {
                                $this->flashMessenger()->addErrorMessage('Rola nie została wyedytowana poprawnie');
                            }
                        }
                    $viewModel->role =$role;
                    $viewModel->form =$form;
                    return $viewModel;
                }else{
                    throw new \Exception('Role object dose not exist with id');
                }
         }else{
             throw new \Exception('Role id is require');
         }
        
    }
    
    public function deleteAction(){
        echo "usuń TODO";exit;
    }
    

}