<?php

namespace Admin\Controller;

use Zend\Mvc\Controller\AbstractActionController,
    Zend\View\Model\ViewModel,
    Doctrine\ORM\EntityManager;

class AdminController extends AbstractActionController {

    protected $adminService;
    
    public function getAdminService()
    {
        if (!$this->adminService) {
            $this->adminService = $this->getServiceLocator()->get('zfcuser_user_service');
        }
        return $this->adminService;
    }
    
    public function indexAction(){
        
        
    }


}