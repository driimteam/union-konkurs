<?php

namespace Admin\Controller;

use Zend\Mvc\Controller\AbstractActionController,
    Zend\View\Model\ViewModel,
    Doctrine\ORM\EntityManager,
    Admin\Form\RoleForm,
    Admin\Models\Entity\Role;

class PermissionsController extends AbstractActionController {

    protected $adminService;
    protected $zfcUserService;

    public function setEventManager(\Zend\EventManager\EventManagerInterface $events) {
        parent::setEventManager($events);
        $this->getCmsService()->setLeftMenu('roles');
        return $this;
    }

    public function getZfcUserService() {
        if (!$this->zfcUserService) {
            $this->zfcUserService = $this->getServiceLocator()->get('zfcuser_user_service');
        }
        return $this->zfcUserService;
    }

    public function getAdminService() {
        if (!$this->adminService) {
            $this->adminService = $this->getServiceLocator()->get('admin_service');
        }
        return $this->adminService;
    }

    public function indexAction() {
        if ($roleId = $this->getEvent()->getRouteMatch()->getParam('roleId')) {
            $viewModel = new ViewModel();
            $request = $this->getRequest();
            $serviceAdmin = $this->getAdminService();
            $permissionMapper = $serviceAdmin->getPermissionMapper();
            $mapperRole = $serviceAdmin->getRoleMapper();

            $role = $mapperRole->find($roleId);
            if ($role) {
                $permissions = $role->getPermissions();
                $viewModel->permissions = $permissions;
                $mapperNode = $serviceCms->getNodeMapper();
                $treenodes = $mapperNode->getTree();
                $pageContents = $serviceCms->getNavigationPage('contents');
                $pageContents->tree = $treenodes;
                return $viewModel;
            } else {
                throw new \Exception('Role object dose not exist with id');
            }
        } else {
            throw new \Exception('Role id is require');
        }
    }

}