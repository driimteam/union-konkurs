<?php

namespace Admin\Form\Fieldset;

use Zend\Form\Element;
use Zend\Form\Form;
use Application\Form\Fieldset\Basic as BasicFieldset;
use Admin\Models\Entity\Role;
use Zend\InputFilter\InputFilterProviderInterface;

//use Zend\Stdlib\Hydrator\ClassMethods as ClassMethodsHydrator;

class RoleFieldset extends BasicFieldset implements InputFilterProviderInterface {

    public function __construct($sm, $options = array()) {
        parent::__construct('role', new Role(), $sm, $options);

        $this->add($this->getHiddenElement('id'));
        $this->add($this->getTextElement('name', 'Nazwa')->setAttribute('class', 'required'));
        
    }

    /**
     * Define InputFilterSpecifications
     *
     * @access public
     * @return array
     */
    public function getInputFilterSpecification() {
        return array(
            'name' => array(
                'required' => true,
                'filters' => array(
                    array('name' => 'StringTrim'),
                    array('name' => 'StripTags'),
                    array('name' => 'App\\Filter\\HtmlSpecialchars')
                ),
                'properties' => array(
                    'required' => true
                )
            )
        );
    }

}
