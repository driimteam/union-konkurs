<?php

namespace Admin\Form;

use Zend\Form\Element;
use Zend\Form\Form;
use Application\Form\BasicForm as BasicForm;
use Admin\Form\Fieldset\RoleFieldset;
use Zend\InputFilter\InputFilter;

class RoleForm extends BasicForm {

    public function __construct($sm, $options = array()) {
        parent::__construct('role', $sm, $options);
        $elementsObject = new RoleFieldset($this->sm);
        $elementsObject->setUseAsBaseFieldset(true); //, array('options' => array('use_as_base_fieldset' => true)));
        $this->add($elementsObject);
    }

}
