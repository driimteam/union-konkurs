<?php

namespace Admin\Form\Filter;

use Zend\InputFilter\InputFilter;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\EventManager;
use App\Filter\HtmlSpecialchars;

class RoleFilter extends InputFilter
{
    public function __construct($options=array())
    {
        
        $factory = $this->getFactory();
        $this->add($factory->createInput(array(
                'name'       => 'id',
                'required'   => false,
                'filters' => array(
                    array('name'    => 'Int'),
                ),
            )));
        
        $this->add($this->getFactory()->createInput(array(
            'name'       => 'name',
            'required'   => true,
            'filters'  => array(
                array('name' => 'StringTrim'),
                array('name' => 'App\\Filter\\HtmlSpecialchars'),
            ),
            'validators' => array(),
        )));
    }
}