<?php
namespace Admin\Form;
use Zend\Form\Element;
use Zend\Form\Form;
use Cms\Form\BasicForm as BasicForm;
use Admin\Form\Filter\RoleFilter;

class RoleForm extends BasicForm
{
    protected $formElements;
    protected $filter;
    protected $options;
    public function __construct($roleId=null,$options=array())
    {
        parent::__construct($options);
        $this->options = $options;
        $this->add($this->getHiddenElement('id')->setValue($roleId));
        $this->add($this->getTextElement('name','Nazwa')->setAttribute('class','required'));
        //$this->add($this->getButtonElement('submit', 'Zapisz'));
        $inputFilter = $this->getFilter();
        $this->setInputFilter($inputFilter);

    }
    
    public function getFilter() {
        if($this->filter==null){
            $this->filter = new RoleFilter($this->options);
        }
        return $this->filter;
        
    }
    
}
