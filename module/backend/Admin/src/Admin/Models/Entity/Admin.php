<?php

namespace Admin\Models\Entity;

use ZfcRbac\Identity\IdentityInterface;
use Doctrine\Common\Collections\ArrayCollection;
use ZfcUserDoctrineORM\Entity\User as OrmUserEntity;

class Admin extends OrmUserEntity implements IdentityInterface
{
    protected $roles;

    public function __construct()
    {
        $this->roles = new ArrayCollection();
    }

    public function getRoles()
    {
        return $this->roles;
    }

    public function addRole($role)
    {
        $this->roles[] = $role;
    }
}
