<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/zf2 for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Admin\View\Helper;
use Zend\View\Helper\AbstractHtmlElement as AbstractHtmlElement;
/**
 * Helper for ordered and unordered lists
 */
class NodePermissionsTree extends AbstractHtmlElement
{
    protected $partial;
    /**
     * Generates a 'List' element.
     *
     * @param  array $items   Array with the elements of the list
     * @param  bool  $ordered Specifies ordered/unordered list; default unordered
     * @param  array $attribs Attributes for the ol/ul tag.
     * @param  bool  $escape  Escape the items.
     * @return string The list XHTML.
     */
    public function __invoke($tree, $partial)
    {
        $this->partial = $partial;
        $this->printTree($tree);
    }
    public function sortTree($a, $b){
        $anode = $a['node'];
        $bnode = $b['node'];
        
        $asort = is_null($anode->sort) ?  0 : $anode->sort;
        $bsort = is_null($bnode->sort) ?  0 : $bnode->sort;
        if ($asort == $bsort) {
            return 0;
        }
        return ($asort < $bsort) ? -1 : 1;
    }
    
    public function printTree($tree) {
        if(!is_null($tree) && count($tree) > 0) {
            uasort($tree, array($this,'sortTree'));
            echo '<ul>';
            foreach($tree as $node) {
                $nodeObj = $node['node'];
                echo '<li id="sort_'.$nodeObj->id.'">'.$this->view->render($this->partial, array('nodetree'=>$node));;
                $this->printTree($node['children']);
                echo '</li>';
            }
            echo '</ul>';
        }
    }
}
