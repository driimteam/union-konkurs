<?php
namespace Competition;
$competitionOptions = [
    
];
return array(
    "competition_module_options"=>$competitionOptions,
    'service_manager' => array(
        'factories' => array(
                'competition_service' => "Competition\Factory\CompetitionServiceFactory",
                'competition_options' => "Competition\Factory\ModuleOptionsFactory"
            ),
     ),
    'controllers' => array(
        'invokables' => array(
            'Competition\Controller\Index' => 'Competition\Controller\IndexController'
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
);