<?php

namespace Competition\Controller;

use Zend\Mvc\Controller\AbstractActionController,
    Zend\View\Model\ViewModel,
    Doctrine\ORM\EntityManager,
    CompetitionApp\Models\Entity\CompetitionApplication as CompetitionApplicationEntity;

class IndexController extends AbstractActionController
{

    protected $appService;
    protected $statService;
    protected $competition_session_service;
    protected $competitionapp_service;
    protected $competition_service;
    protected $recommendapp_service;

    public function getAppService()
    {
        if (!$this->appService) {
            $this->appService = $this->getServiceLocator()->get('application_service');
        }
        return $this->appService;
    }

    public function getCompetitionAppService()
    {
        if (!$this->competitionapp_service) {
            $this->competitionapp_service = $this->getServiceLocator()->get('competitionapp_service');
        }
        return $this->competitionapp_service;
    }

    public function getCompetitionService()
    {
        if (!$this->competitionapp_service) {
            $this->competitionapp_service = $this->getServiceLocator()->get('competitionapp_service');
        }
        return $this->competitionapp_service;
    }

    public function indexAction()
    {
        $viewModel = new ViewModel();
        $startLimit = 40;
        $page = $this->getEvent()->getRouteMatch()->getParam('page', 1);
        $limit = $this->getEvent()->getRouteMatch()->getParam('limit', $startLimit);

        $mapper = $this->getCompetitionAppService()->getCompetitionApplicationMapper();

        $params = array(
            'sort' => array('order' => "ca.id", "dir" => 'desc')
        );
        $paginator = $mapper->findByPager($page, $limit, $params);
        $viewModel->paginator = $paginator;
        $viewModel->mainRouteName = 'competitions';
        $viewModel->params = array('page' => $page, "limit" => $limit);
        return $viewModel;
    }

    public function verificationsAction()
    {
        $viewModel = new ViewModel();
        $startLimit = 40;
        $page = $this->getEvent()->getRouteMatch()->getParam('page', 1);
        $limit = $this->getEvent()->getRouteMatch()->getParam('limit', $startLimit);

        $mapper = $this->getCompetitionAppService()->getCompetitionApplicationMapper();

        $params = array(
            'sort' => array('order' => "ca.id", "dir" => 'desc'),
        );
        $paginator = $mapper->findByPager($page, $limit, $params);
        $viewModel->paginator = $paginator;
        $viewModel->mainRouteName = 'verifications';
        $viewModel->params = array('page' => $page, "limit" => $limit);
        return $viewModel;
    }

    public function showAction()
    {
        $viewModel = new ViewModel();
        if ($id = $this->getEvent()->getRouteMatch()->getParam('id', null)) {
            $mapper = $this->getCompetitionAppService()->getCompetitionApplicationMapper();
            $competitionApplicationEntity = $mapper->find($id);
            $viewModel->competitionApplicationEntity = $competitionApplicationEntity;
        }
        return $viewModel;
    }

    public function confirmAction()
    {
        try {
            if ($id = $this->getEvent()->getRouteMatch()->getParam('id')) {
                $mapper = $this->getCompetitionAppService()->getCompetitionApplicationMapper();
                $entity = $mapper->find($id);

                if (is_object($entity)) {
                    $entity->setStatus(CompetitionApplicationEntity::STATUS_ACTIVE);
                    $mapper->save($entity);

                    //$this->getCompetitionService()->sendEmailAboutConfirmed($entity);
                    echo "OK";
                    exit;
                }
            }
        } catch (Exception $e) {
            echo "ERROR";
            exit;
        }
    }

    public function rejectAction()
    {
        try {
            if ($id = $this->getEvent()->getRouteMatch()->getParam('id')) {
                $mapper = $this->getCompetitionAppService()->getCompetitionApplicationMapper();
                $entity = $mapper->find($id);

                if (is_object($entity)) {
                    $entity->setStatus(CompetitionApplicationEntity::STATUS_REJECTED);
                    $mapper->save($entity);
                    $this->getCompetitionService()->sendEmailAboutRejected($entity);
                    echo "OK";
                    exit;
                }
            }
        } catch (Exception $e) {
            echo "ERROR";
            exit;
        }
    }

}
