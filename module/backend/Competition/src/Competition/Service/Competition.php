<?php

namespace Competition\Service;

use Zend\ServiceManager\ServiceManagerAwareInterface;
use Zend\ServiceManager\ServiceManager;
use Zend\Stdlib\DispatchableInterface;
use Competition\Options\ModuleOptions;
use CompetitionApp\Models\Entity\CompetitionApplication as CompetitionApplicationEntity;

class Competition implements ServiceManagerAwareInterface
{

    /**
     * @var ServiceManager
     */
    protected $serviceManager;
    protected $eventManager;
    protected $competition_session_service;

    /**
     * Mappers
     */
    protected $questionnaireapp_service;
    protected $competitionapp_service;
    protected $auth_service;
    protected $recommendapp_service;
    protected $mail_service;

    public function __construct(ModuleOptions $options)
    {
        $this->options = $options;
    }

    public function getOptions()
    {
        return $this->options;
    }

    public function setOptions(ModuleOptions $options)
    {
        $this->options = $options;
        return $this;
    }

    /**
     * Retrieve service manager instance
     *
     * @return ServiceManager
     */
    public function getServiceManager()
    {
        return $this->serviceManager;
    }

    /**
     * Set service manager instance
     *
     * @param ServiceManager $serviceManager
     * @return User
     */
    public function setServiceManager(ServiceManager $serviceManager)
    {
        $this->serviceManager = $serviceManager;
        return $this;
    }

    public function getEntityManager()
    {
        return $this->getServiceManager()->get('Doctrine\ORM\EntityManager');
    }

    public function getCompetitionAppService()
    {
        if ($this->competitionapp_service === null) {
            $this->competitionapp_service = $this->getServiceManager()->get('competitionapp_service');
        }
        return $this->competitionapp_service;
    }

    public function getAuthService()
    {
        if ($this->auth_service === null) {
            $this->auth_service = $this->getServiceManager()->get('zfcuser_auth_service');
        }
        return $this->auth_service;
    }

    public function getMailService()
    {
        if (!$this->mail_service) {
            $this->mail_service = $this->getServiceManager()->get('MtMail\Service\Mail');
        }
        return $this->mail_service;
    }

    public function sendEmailAboutConfirmed($competitionApplication)
    {
        $translator = $this->getServiceManager()->get('translator');
        $headers = array(
            'to' => $competitionApplication->getUserEmail(),
            'subject' => $translator->translate('mail_title_confirmed')
        );
        $params['competitionApplication'] = $competitionApplication;
        $message = $this->getMailService()->compose($headers, 'mail/confirmed.phtml', $params);
        return $this->getMailService()->send($message);
    }

    public function sendEmailAboutRejected($competitionApplication)
    {
        $translator = $this->getServiceManager()->get('translator');
        $headers = array(
            'to' => $competitionApplication->getUserEmail(),
            'subject' => $translator->translate('mail_title_rejected')
        );
        $params['competitionApplication'] = $competitionApplication;
        $message = $this->getMailService()->compose($headers, 'mail/rejected.phtml', $params);
        return $this->getMailService()->send($message);
    }

}
