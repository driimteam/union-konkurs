<?php
namespace Application;
return array(
    'controllers' => array(
        'invokables' => array(
          'Application\Controller\Index' => 'Application\Controller\IndexController',
        ),
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => array(
            'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'layout/onlycontent'           => __DIR__ . '/../view/layout/onlycontent.phtml',
            'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
            'paginator-slide' => __DIR__ . '/../view/layout/slidePaginator.phtml',
            'navigation' => __DIR__ . '/../view/layout/navigation.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    ),
    'service_manager' => array(
        'factories' => array(
            'top_right_menu' => 'Application\Navigation\Service\TopRightMenu',
            
        ),
    ),
    'view_helpers' => array(
        'invokables'=> array(
            'isSupportedMultiLang'=>'\Application\View\Helper\IsSupportedMultiLang',
            'getSupportedLanguages'=>'\Application\View\Helper\GetSupportedLanguages',
            'getCurrentLang'=>'\Application\View\Helper\GetCurrentLang',
            'getCmsService'=>'\Application\View\Helper\GetCmsService'
        )
    ),
);