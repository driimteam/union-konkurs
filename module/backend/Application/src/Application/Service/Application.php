<?php

namespace Application\Service;

use Zend\Form\Form;
use Zend\View\Model\ViewModel;
use Zend\ServiceManager\ServiceManagerAwareInterface;
use Zend\ServiceManager\ServiceManager;
use Zend\Crypt\Password\Bcrypt;
use Zend\Stdlib\Hydrator;
use Zend\Authentication\Storage\Session as StorageSession;
use Zend\Stdlib\DispatchableInterface;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;

class Application implements ServiceManagerAwareInterface {

    /**
     * @var pluginMapper
     */
    protected $translator;
    protected $headTitle = array();
    protected $auth;
    protected $em;

    /**
     * @var ServiceManager
     */
    protected $serviceManager;
    protected $eventManager;

    /**
     * @var UserServiceOptionsInterface
     */

    /**
     * @var Hydrator\ClassMethods
     */
    protected $formHydrator;

    /**
     * Retrieve service manager instance
     *
     * @return ServiceManager
     */
    public function getServiceManager() {
        return $this->serviceManager;
    }

    /**
     * Set service manager instance
     *
     * @param ServiceManager $serviceManager
     * @return User
     */
    public function setServiceManager(ServiceManager $serviceManager) {
        $this->serviceManager = $serviceManager;
        return $this;
    }

    public function setEntityManager(EntityManager $em) {
        $this->em = $em;
    }

    public function getEntityManager() {
        if (null === $this->em) {
            $this->em = $this->getServiceManager()->get('Doctrine\ORM\EntityManager');
        }
        return $this->em;
    }

    public function getTranslator() {
        if (null === $this->translator) {
            $this->translator = $this->getServiceManager()->get('translator');
        }
        return $this->translator;
    }

    public function setHeadTitle($title,$routeName=null) {
        $titleHelper = $this->getServiceManager()->get('viewhelpermanager')->get('headTitle');
        $titleHelper()->prepend($title);
        if($routeName){
        $activepage=$this->getNavigationPage($routeName);
        $activepage->setLabel($title);
        }
    }

    public function setLeftMenu($pageId = null, $params = array()) {
        $page = $this->setSubmenu(null, $pageId, 'leftMenu');
        $page->setParams($params);
        if (!empty($params['parentLabel'])) {
            $parent = $page->getParent();
            $parent->setParams($params);
            $parent->setLabel($params['parentLabel']);
        }
        foreach ($page->getPages() as $childPage) {
            $childPage->setParams($params);
        }
        return $page;
    }

    public function setSubmenu($page = null, $pageId = null, $navigationName = 'navigationSubmenu') {
        $navigationHelper = $this->getServiceManager()->get('viewhelpermanager')->get('navigation');
        $navigation = $navigationHelper('Navigation');
        $container = $navigation->getContainer();
        if ($page == null) {
            if ($pageId == null) {
                $page = $container->findBy('active', 'true');
            } else {
                $page = $container->findBy('id', $pageId);
                //var_dump($page);exit;
            }
            $page->active = 1;
        }
        $application = $this->getServiceManager()->get('Application');

        $viewModel = $application->getMvcEvent()->getViewModel();
        $viewModel->$navigationName = $page;
        return $page;
        //$this->setHeadTitles($page,$container);
    }

    public function getNavigationPage($pageId = null, $container = 'Navigation') {
        $navigationHelper = $this->getServiceManager()->get('viewhelpermanager')->get('navigation');
        $navigation = $navigationHelper($container);
        $container = $navigation->getContainer();
        if(!$pageId) {
            $page = $container->findOneBy('active', 'true');
        } else {
            $page = $container->findOneBy('id', $pageId);
        }
        return $page;
    }

    public function setSubmenuById($pageId) {
        $navigationHelper = $this->getServiceManager()->get('viewhelpermanager')->get('navigation');
        $navigation = $navigationHelper('Navigation');
        $container = $navigation->getContainer();
        $page = $container->findBy('active', 'true');
        $page->active = true;


        $application = $this->getServiceManager()->get('Application');

        $viewModel = $application->getMvcEvent()->getViewModel();
        $viewModel->navigationSubmenu = $page;
        //$this->setHeadTitles($page,$container);
    }

    public function setNameAuthSessionStorage($nameStorage) {
        $auth = $this->getAuth();
        $auth->setStorage(new StorageSession($nameStorage));
    }

    public function getIdentity() {
        /* $user = new \Application\Models\Entity\User();
          $user->agent_id = 444444;
          $user->group_id = 1;
          $user->competition_id = 2;
          return $user; */
        if ($this->getAuth()->hasIdentity()) {
            return $this->getAuth()->getIdentity();
        }
        return false;
        ;
    }

    public function getConfig() {
        $config = $this->getServiceManager()->get('Configuration');
        return $config;
    }

    public function getNavigationContainerById($idName) {
        $navigationHelper = $this->getServiceManager()->get('viewhelpermanager')->get('navigation');
        $navigation = $navigationHelper('Navigation');
        $container = $navigation->getContainer();
        $competitionsPage = $container->findOneBy('id', $idName);
        return $competitionsPage;
    }

    public function initNavigation($navigationSection) {
        $navigationHelper = $this->getServiceManager()->get('viewhelpermanager')->get('navigation');
        $navigation = $navigationHelper($navigationSection);
        $container = $navigation->getContainer();
        $page = $container->findBy('active', 'true');
        $this->setHeadTitles($page, $container);
    }

    public function isSupportedMultiLang() {
        $config = $this->getServiceManager()->get('Configuration');
        return isset($config['multilang']) ? $config['multilang'] : false;
    }

    public function getSupportedLanguages() {
        $config = $this->getServiceManager()->get('Configuration');
        return $config['languages'];
    }

    public function setHeadTitles($active, $container) {
        $titleHelper = $this->getServiceManager()->get('viewhelpermanager')->get('headTitle');
        while (is_object($active) && ($parent = $active->getParent())) {
            $titleHelper($active->getLabel());
            if ($parent === $container) {
                break;
            }
            $active = $parent;
        }
    }

    public function getNewHydrator() {
        $em = $this->getEntityManager();
        return new DoctrineHydrator($em);
    }

}
