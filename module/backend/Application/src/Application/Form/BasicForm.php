<?php

namespace Application\Form;

use Zend\Form\Form;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;

class BasicForm extends Form implements ServiceLocatorAwareInterface {
    use ServiceLocatorAwareTrait;

    protected $options;
    protected $sm;
    protected $applicationService;
    protected $em;

    public function getApplicationService() {
        if (!$this->applicationService) {
            $this->applicationService = $this->sm->getServiceLocator()->get('application_service');
        }
        return $this->applicationService;
    }
    
    public function __construct($name=null,$sm=null,$options = array()) {
        // we want to ignore the name passed
        parent::__construct($name);
        $this->setAttribute('method', 'post');
        $this->options = $options;
        if($sm){
            $this->sm = $this->setServiceLocator($sm);
            $hydrator = $this->getApplicationService()->getNewHydrator();
            $this->setHydrator($hydrator);
        }
        
    }

    public function getTextElement($name, $label) {
        $element = new \Zend\Form\Element\Text($name);
        $element->setOptions(array(
            'label' => $label,
            'partial' => "formpartial/text"
        ));
        return $element;
    }

    public function getHiddenElement($name) {
        $element = new \Zend\Form\Element\Hidden($name);

        $element->setOptions(array(
            'partial' => null
        ));
        return $element;
    }

    public function getLeadElement($name, $label) {
        $element = new \Zend\Form\Element\Text($name);
        $element->setOptions(array(
            'label' => $label,
            'partial' => "formpartial/lead"
        ));
        return $element;
    }

    public function getDateElement($name, $label) {
        $element = new \Zend\Form\Element\Text($name);
        $element->setOptions(array(
            'label' => $label,
            'partial' => "formpartial/date"
        ));
        return $element;
    }

    public function getMainPictureElement($name, $label) {
        $element = new \Zend\Form\Element\Hidden($name);
        $element->setOptions(array(
            'label' => $label,
            'partial' => "formpartial/filemanager"
        ));
        return $element;
    }

    public function getButtonElement($name,$label) {
        $element = new \Zend\Form\Element\Button($name);
        $element->setOptions(array(
            'label' => $label,
            'partial' => "formpartial/button"
        ));
        $element->setValue($label);
        return $element;
    }

    public function getTinymceElement($name, $label) {
        $element = new \Zend\Form\Element\Textarea($name);
        $element->setOptions(array(
            'label' => $label,
            'partial' => "formpartial/tinymce"
        ));
        return $element;
    }
    
    public function getRadioElement($name, $label, $options) {

        $radioStatus = new \Zend\Form\Element\Radio($name);
        $radioStatus->setLabel($label);
        $radioStatus->setValueOptions($options);
        
        $radioStatus->setOptions(array('partial' => 'formpartial/multiradio'));
        return $radioStatus;
    }
    
    public function getSelectElement($name, $label, $options) {

        $element = new \Zend\Form\Element\Select($name);
        $element->setLabel($label);
        $element->setValueOptions($options);
        $element->setOptions(array('partial' => 'formpartial/select'));
        return $element;
    }

}
