<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController,
    Zend\View\Model\ViewModel,
    Doctrine\ORM\EntityManager;

class IndexController extends AbstractActionController {

    
    protected $appService;

    public function getAppliactionService() {
        if (!$this->appService) {
            $this->appService = $this->getServiceLocator()->get('application_service');
        }
        return $this->appService;
    }
    
    public function indexAction(){
        
    }


}