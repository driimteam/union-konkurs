<?php

namespace Application\Controller\Plugin;

use Zend\Mvc\Controller\AbstractActionController,
    Zend\View\Model\ViewModel;
use Zend\Mvc\Controller\Plugin\AbstractPlugin;

class EntityPlugin extends AbstractPlugin {

    protected $applicationService;
    protected $currentService;

    public function getApplicationService() {
        if (!$this->applicationService) {
            $this->applicationService = $this->getController()->getServiceLocator()->get('application_service');
        }
        return $this->applicationService;
    }

    public function getCurrentService() {
        if (!$this->currentService) {
            $this->currentService = $this->getController()->getServiceLocator()->get($this->getController()->current_service);
        }
        return $this->currentService;
    }

    public function getCurrentMapper() {
        $currentMapper = $this->getController()->getServiceLocator()->get($this->getController()->current_mapper); //TableMapper();
        return $currentMapper;
    }

    public function getCurrentForm() {
        $formName = $this->getController()->current_form;
        $form = new $formName($this->getController()->getServiceLocator());
        return $form;
    }

    public function manageEntity($entity, $template, $redirectUrl) {
        $controller = $this->getController();
        $viewModel = new ViewModel();
        $mapper = $this->getCurrentMapper();
        $form = $this->getCurrentForm();
        $form->bind($entity);
        $request = $controller->getRequest();
        if ($request->isPost()) {
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $mapper->save($entity);
                $controller->flashMessenger()->addSuccessMessage('Dane zostały zapisane poprawnie');
                return $controller->redirect()->toUrl($redirectUrl);
            } else {
                $controller->flashMessenger()->addErrorMessage('Dane nie zostały zapisane poprawnie');
            }
        }
        $viewModel->form = $form;
        $viewModel->mainRouteName = $this->getController()->mainRouteName;
        $viewModel->setTemplate($template);
        return $viewModel;
    }

    public function addEntity() {
        $controller = $this->getController();
        $entity = $controller->getCurrentEntity();
        $routeName = $controller->redirect_route_add;
        return $this->manageEntity($entity, 'application/entities/add', $controller->url()->fromRoute($routeName));
    }

    public function editEntity() {
        $controller = $this->getController();
        if ($id = $controller->getEvent()->getRouteMatch()->getParam('id')) {
            $entity = $controller->getCurrentEntity($id);
            $routeName = $controller->redirect_route_edit;
            return $this->manageEntity($entity, 'application/entities/edit', $controller->url()->fromRoute($routeName, array('id' => $id)));
        } else {
            throw new \Exception('Object id is require');
        }
    }

    public function listEntities() {
        $viewModel = new ViewModel();
        $controller = $this->getController();
        $currentMapper = $this->getCurrentMapper();
        $list = $currentMapper->findBy();
        $viewModel->setTemplate('application/entities/list');
        $viewModel->list = $list;
        $viewModel->mainRouteName = $this->getController()->mainRouteName;
        return $viewModel;
    }

}
