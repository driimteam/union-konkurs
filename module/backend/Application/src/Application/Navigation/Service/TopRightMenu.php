<?php 
namespace Application\Navigation\Service;

use Zend\Navigation\Service\DefaultNavigationFactory;

class TopRightMenu extends DefaultNavigationFactory
{
    protected function getName()
    {
        return 'top_right_menu';
    }
}