<?php

namespace PaymentTest\Controller;

use PaymentTest\Bootstrap;
use Zend\Mvc\Router\Http\TreeRouteStack as HttpRouter;
use Zend\Http\Request;
use Zend\Http\Response;
use Zend\Mvc\MvcEvent;
use Zend\Mvc\Router\RouteMatch;
use PHPUnit_Framework_TestCase;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Quiz\Controller\RankingController;
use Payment\Controller\IndexController;
use UserApp\Models\Entity\User as UserIdentity;
use PaymentApp\Models\Entity\AppWallet;
use PaymentApp\Models\Entity\AppPaymentdata;
use PaymentApp\Models\Entity\AppTransaction;
use Doctrine\ORM\Tools\Pagination\Paginator as ORMpaginator;

class IndexControllerTest extends PaymentTestAbstractController
{

    protected function setUp()
    {
        $this->controller = new IndexController();
        $this->routeMatch = new RouteMatch(array('controller' => 'Index'));
        parent::setUp();
    }

    protected function prepareMockTransactionMapper()
    {
        
    }

    public function testIndexAction()
    {
        $this->mockLogin();

        $this->routeMatch->setParam('action', 'index');
        $result   = $this->controller->dispatch($this->request);
        $response = $this->controller->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
    }
}