<?php

namespace PaymentTest\Controller;

use PaymentTest\Bootstrap;
use Zend\Mvc\Router\Http\TreeRouteStack as HttpRouter;
use Zend\Http\Request;
use Zend\Http\Response;
use Zend\Mvc\MvcEvent;
use Zend\Mvc\Router\RouteMatch;
use PHPUnit_Framework_TestCase;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Payment\Controller\IndexController;
use Admin\Models\Entity\Admin as UserIdentity;
use Admin\Models\Entity\Role;
use PaymentApp\Models\Entity\AppWallet;
use PaymentApp\Models\Entity\AppTransaction;
use PaymentApp\Models\Entity\AppUserWithdrawals;
use PaymentApp\Models\Entity\AppPaymentdata;

abstract class PaymentTestAbstractController extends AbstractHttpControllerTestCase
{
    protected $controller;
    protected $request;
    protected $response;
    protected $routeMatch;
    protected $event;
    protected $authService;
    protected $pluginAuth;
    protected $paymentAppService;
    protected $serviceManager;
    protected $userIdentity;
    protected $przelewy24Service;
    protected $pluginPrzelewy24;
    protected $zfcUserService;

    protected function setUp()
    {
        $this->setApplicationConfig(
            include 'TestConfig.php'
        );
        parent::setUp();
        
        $this->serviceManager = Bootstrap::getServiceManager();

        $this->request = new Request();
        $this->event   = new MvcEvent();
        $config        = $this->serviceManager->get('Config');
        $routerConfig  = isset($config['router']) ? $config['router'] : array(
        );
        $router        = HttpRouter::factory($routerConfig);

        $this->event->setRouter($router);
        $this->event->setRouteMatch($this->routeMatch);
        $this->controller->setEvent($this->event);
        $this->controller->setServiceLocator($this->serviceManager);

        $this->authService = $this->getMock('Zend\Authentication\AuthenticationService');
        $this->serviceManager->setAllowOverride(true);
        $this->serviceManager->setService('Zend\Authentication\AuthenticationService',
            $this->authService);

        $this->pluginAuth = $this->getMockBuilder('ZfcUser\Controller\Plugin\ZfcUserAuthentication')->getMock();
        $this->controller->getPluginManager()->setService('zfcUserAuthentication',
            $this->pluginAuth);

        $this->controller->getPluginManager()->setService('zfcUserAuthentication',
            $this->pluginAuth);


        $urlPlugin = $this->getMockBuilder('Zend\Mvc\Controller\Plugin\Url')->getMock();
        $redirect  = $this->getMockBuilder('Zend\Mvc\Controller\Plugin\Redirect')->getMock();
        $this->controller->getPluginManager()->setService('url', $urlPlugin);
        $this->controller->getPluginManager()->setService('redirect', $redirect);

        $this->zfcUserService       = $this->serviceManager->get('zfcuser_user_service');
        
        $this->paymentAppService = $this->serviceManager->get('paymentapp_service');
        $this->statisticsService = $this->serviceManager->get('statistics_service');

        $this->userMapper            = $this->getMockBuilder('\UserApp\Models\Mapper\User')->disableOriginalConstructor()->getMock();
        $this->walletMapper          = $this->getMockBuilder('\PaymentApp\Models\Mapper\Wallet')->disableOriginalConstructor()->getMock();
        $this->transactionMapper     = $this->getMockBuilder('\PaymentApp\Models\Mapper\Transaction')->disableOriginalConstructor()->getMock();
        $this->smsMapper             = $this->getMockBuilder('\PaymentApp\Models\Mapper\Sms')->disableOriginalConstructor()->getMock();
        $this->userWithdrawalsMapper = $this->getMockBuilder('\PaymentApp\Models\Mapper\UserWithdrawals')->disableOriginalConstructor()->getMock();
        $this->paymentdataMapper     = $this->getMockBuilder('\PaymentApp\Models\Mapper\Paymentdata')->disableOriginalConstructor()->getMock();

        $this->paymentAppService->setWalletMapper($this->walletMapper);
        $this->paymentAppService->setTransactionMapper($this->transactionMapper);
        $this->paymentAppService->setSmsMapper($this->smsMapper);
        $this->paymentAppService->setUserWithdrawalsMapper($this->userWithdrawalsMapper);
        $this->paymentAppService->setPaymentdataMapper($this->paymentdataMapper);

        $this->zfcUserService->setUserMapper($this->userMapper);
        
        $this->walletMapper->expects($this->any())
            ->method('save')
            ->will($this->returnValue(true));

        $this->transactionMapper->expects($this->any())
            ->method('save')
            ->will($this->returnValue(true));

        $this->userWithdrawalsMapper->expects($this->any())
            ->method('save')
            ->will($this->returnValue(true));
    }

    protected function prepareTransactionEntity($amount)
    {
        $transaction = new AppTransaction();
        $transaction->setId(23);
        $transaction->setAmount($amount);
        $transaction->setUserId($this->userIdentity->getId());
        $transaction->setStatus("OK");
        return $transaction;
    }

    protected function createUserEntity()
    {
        $userEntity  = new UserIdentity();
        $userEntity->setId(22);
        $userEntity->setEmail("test@driim.com");

        $role = new Role();
        $permissions = ['home', 'admin', 'questions', 'cms', 'questions', 'tables','game','payment','scheme', 'logout'];
        foreach($permissions as $permision){
         $role->addPermission($permision);
        }
        $userEntity->addRole($role);

        $userEntity->setState(1);
        return $userEntity;
    }

    protected function getUserEntity()
    {
        return $this->createUserEntity();
    }

    protected function mockLogin()
    {

        $this->userIdentity = $this->createUserEntity();

        $this->authService->expects($this->any())
            ->method('getIdentity')
            ->will($this->returnValue($this->userIdentity));

        $this->authService->expects($this->any())
            ->method('hasIdentity')
            ->will($this->returnValue(true));


        $this->pluginAuth->expects($this->any())
            ->method('getIdentity')
            ->will($this->returnValue($this->userIdentity));

        $this->pluginAuth->expects($this->any())
            ->method('hasIdentity')
            ->will($this->returnValue(true));
    }
}