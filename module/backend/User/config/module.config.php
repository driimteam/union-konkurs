<?php
namespace User;
$userOptions=[];
return array(
     'user_options'=>$userOptions,
     'controllers' => array(
        'invokables' => array(
            'User\Controller\Index' => 'User\Controller\IndexController',
        ),
    ),
    'service_manager' => array(
        'factories' => array(
            'user_module_options' => "User\Factory\ModuleOptionsFactory",
            'user_service' => "User\Factory\UserServiceFactory",
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    ),
    
);