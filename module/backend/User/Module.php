<?php

namespace User;

use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use Doctrine\ORM\Mapping\Driver\XmlDriver;
use User\Module as Payment;
use Zend\ModuleManager\ModuleManager;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ModuleManager\Feature\ServiceProviderInterface;
use Zend\Stdlib\Hydrator\ClassMethods;
use Zend\Mvc\MvcEvent;
use Zend\View\Helper;
use Zend\Mvc\ModuleRouteListener;

class Module implements ServiceProviderInterface {

    public function onBootstrap(MvcEvent $e) {
        $eventManager = $e->getApplication()->getEventManager();
    }

    public function init(ModuleManager $moduleManager) {
        //$sharedEvents = $moduleManager->getEventManager()->getSharedManager();
    }

    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getControllerPluginConfig() {
        return array(
            'factories' => array(
            ),
        );
    }

    public function getServiceConfig() {
        $module = $this;
        return array(
            'invokables' => array(
            ),
            'factories' => array(
            ),
        );
    }

    public function getConfig() {
        return include __DIR__ . '/config/module.config.php';
    }

}

