<?php

namespace User\Controller;

use Zend\Mvc\Controller\AbstractActionController,
    Zend\View\Model\ViewModel,
    Doctrine\ORM\EntityManager,
    Zend\Form\Annotation\AnnotationBuilder;
use Zend\Mvc\MvcEvent;

class IndexController extends AbstractActionController
{
    protected $userapp_service;
    protected $statService;
    protected $paymentapp_service;

    
    public function getUserAppService()
    {
        if ($this->userapp_service === null) {
            $this->userapp_service = $this->getServiceLocator()->get('userapp_service');
        }
        return $this->userapp_service;
    }

    public function onDispatch(MvcEvent $e)
    {

        $this->viewModel = new ViewModel();
        $appService      = $this->getServiceLocator()->get('application_service');
        $appService->setSubmenu(null, 'users');
        return parent::onDispatch($e);
    }

    public function indexAction()
    {
        $startLimit = 20;
        $page       = $this->getEvent()->getRouteMatch()->getParam('page', 1);
        $limit      = $this->getEvent()->getRouteMatch()->getParam('limit',
            $startLimit);
        $userMapper = $this->getUserAppService()->getUserMapper();

        $params                  = array('sort' => array('order' => 'user.username',
                'dir' => 'asc'));
        //sort
        $paginator               = $userMapper->findByPager($page, $limit,
            $params);
        $dateCurrent             = date('Y-m-d');
        
        return new ViewModel(array('paginator' => $paginator, "params" => $params,));
    }


    public function showAction()
    {
        if ($userId = $this->getEvent()->getRouteMatch()->getParam('userId')) {
            $userMapper = $this->getUserAppService()->getUserMapper();
            $user       = $userMapper->findById($userId);
            $params['user']    = $user;
            return new ViewModel($params);
        } else {
            throw new \Exception('Question id is require');
        }
    }
}