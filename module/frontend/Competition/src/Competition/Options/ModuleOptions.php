<?php

namespace Competition\Options;

use Zend\Stdlib\AbstractOptions;

class ModuleOptions extends AbstractOptions
{
    protected $verification_email_enabled;
    
    public function getVerificationEmailEnabled()
    {
        return $this->verification_email_enabled;
    }
    
    public function setVerificationEmailEnabled($verification_email_enabled)
    {
        $this->verification_email_enabled = $verification_email_enabled;
        return $this;
    }
}
