<?php

namespace Competition\Factory;

use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\FactoryInterface;
use Competition\Options\ModuleOptions;
use Competition\Service\Competition;

class CompetitionServiceFactory implements FactoryInterface
{

    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $moduleOptions = $serviceLocator->get('competition_options');
        return new Competition($moduleOptions);
    }

}
