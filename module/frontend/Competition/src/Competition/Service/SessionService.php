<?php
namespace Competition\Service;

use Zend\Form\Form;
use Zend\ServiceManager\ServiceManagerAwareInterface;
use Zend\ServiceManager\ServiceManager;
use Zend\Crypt\Password\Bcrypt;
use Zend\Stdlib\Hydrator;
use Zend\Authentication\Storage\Session as StorageSession;
use Zend\Session\Container as SessionContainer;
use Zend\Stdlib\DispatchableInterface;

class SessionService implements ServiceManagerAwareInterface {


    const SESSION_NAMESPACE_COMPETITION = 'COMPETITION';
    protected $serviceManager;
    protected $eventManager;
    protected $user_service;

    /**
     * @var UserServiceOptionsInterface
     */

    /**
     * Retrieve service manager instance
     *
     * @return ServiceManager
     */
    public function getServiceManager() {
        return $this->serviceManager;
    }

    /**
     * Set service manager instance
     *
     * @param ServiceManager $serviceManager
     * @return User
     */
    public function setServiceManager(ServiceManager $serviceManager) {
        $this->serviceManager = $serviceManager;
        return $this;
    }

    public function getUserService() {
        if ($this->user_service === null) {
            $this->user_service = $this->getServiceManager()->get('user_service');
        }
        return $this->user_service;
    }

    public function getCompetitionSession($key=null){
        $competitionSession = new SessionContainer(self::SESSION_NAMESPACE_COMPETITION);
        if ($competitionSession->data) {
            if($key){
                if(isset($competitionSession->data[$key])){
                    return $competitionSession->data[$key];
                }else{
                    return null;
                }
            }
            return $competitionSession->data;
        }
        return null;
    }

//    public function setCompetitionSession($data)
//    {
//        $competitionSession = new SessionContainer(self::SESSION_NAMESPACE_COMPETITION);
//        $competitionSession->data = $data;
//        return $competitionSession->data;
//    }

    public function addCompetitionSession($key,$value)
    {
        $competitionSession = new SessionContainer(self::SESSION_NAMESPACE_COMPETITION);
        $data = ($competitionSession->data) ? $competitionSession->data : array();
        $data[$key] = $value;
        $competitionSession->data=$data;
        return $competitionSession->data;
    }

}
