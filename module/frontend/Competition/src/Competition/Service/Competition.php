<?php

namespace Competition\Service;

use Zend\ServiceManager\ServiceManagerAwareInterface;
use Zend\ServiceManager\ServiceManager;
use Zend\Stdlib\DispatchableInterface;
use Competition\Options\ModuleOptions;
use Competition\Form\CompetitionApplicationForm,
    Competition\Form\SmsForm,
    CompetitionApp\Models\Entity\CompetitionApplication;

class Competition implements ServiceManagerAwareInterface
{

    /**
     * @var ServiceManager
     */
    protected $serviceManager;
    protected $eventManager;
    protected $competition_session_service;

    /**
     * Services
     */
    protected $competitionapp_service;
    protected $auth_service;
    protected $mail_service;

    public function __construct(ModuleOptions $options)
    {
        $this->options = $options;
    }

    public function getOptions()
    {
        return $this->options;
    }

    public function setOptions(ModuleOptions $options)
    {
        $this->options = $options;
        return $this;
    }

    /**
     * Retrieve service manager instance
     *
     * @return ServiceManager
     */
    public function getServiceManager()
    {
        return $this->serviceManager;
    }

    /**
     * Set service manager instance
     *
     * @param ServiceManager $serviceManager
     * @return User
     */
    public function setServiceManager(ServiceManager $serviceManager)
    {
        $this->serviceManager = $serviceManager;
        return $this;
    }

    public function getEntityManager()
    {
        return $this->getServiceManager()->get('Doctrine\ORM\EntityManager');
    }

    public function getCompetitionAppService()
    {
        if ($this->competitionapp_service === null) {
            $this->competitionapp_service = $this->getServiceManager()->get('competitionapp_service');
        }
        return $this->competitionapp_service;
    }

    public function getMailService()
    {
        if (!$this->mail_service) {
            $this->mail_service = $this->getServiceManager()->get('MtMail\Service\Mail');
        }
        return $this->mail_service;
    }

    public function getAuthService()
    {
        if ($this->auth_service === null) {
            $this->auth_service = $this->getServiceManager()->get('zfcuser_auth_service');
        }
        return $this->auth_service;
    }

    public function getCompetitionSessionService()
    {
        if (!$this->competition_session_service) {
            $this->competition_session_service = $this->getServiceManager()->get('competition_session_service');
        }
        return $this->competition_session_service;
    }

    public function getFormForChildren($competitionApplication)
    {
        //$dirDestination = date('Y/m/d');
        $form = new CompetitionApplicationForm($this->getServiceManager());
        $form->bind($competitionApplication);
        switch ($competitionApplication->getSection()) {
            case CompetitionApplication::SECTION_TEXT:
                $form->get('competitionFieldset')->prepareBasicFieldsetForText();
                $form->get('competitionFieldset')->get('section')->setValue(CompetitionApplication::SECTION_TEXT);
                break;
            case CompetitionApplication::SECTION_YOUTUBE:
                $form->get('competitionFieldset')->prepareBasicFieldsetForYoutube();
                $form->get('competitionFieldset')->get('section')->setValue(CompetitionApplication::SECTION_YOUTUBE);
                break;
        }
        return $form;
    }

    public function getFormForFriends($competitionApplication)
    {
        $form = new CompetitionApplicationForm($this->getServiceManager());
        $form->bind($competitionApplication);
        switch ($competitionApplication->getSection()) {
            case CompetitionApplication::SECTION_TEXT:
                $form->get('competitionFieldset')->prepareBasicFieldsetForText();
                $form->get('competitionFieldset')->get('section')->setValue(CompetitionApplication::SECTION_TEXT);
                break;
            case CompetitionApplication::SECTION_YOUTUBE:
                $form->get('competitionFieldset')->prepareBasicFieldsetForYoutube();
                $form->get('competitionFieldset')->get('section')->setValue(CompetitionApplication::SECTION_YOUTUBE);
                break;
        }
        return $form;
    }

    public function setSocialData($competitionApplication, $provider = false)
    {
        $competitionSessionService = $this->getCompetitionSessionService();
        if (!$provider) {
            $provider = $competitionSessionService->getCompetitionSession('provider');
        }
        $socialDataSession = $competitionSessionService->getCompetitionSession($provider);
        if (!empty($socialDataSession)) {
            $socialData = serialize($socialDataSession);
            $socialLink = $socialDataSession->profileURL;


            $userName = isset($socialDataSession->displayName) ? $socialDataSession->displayName : null;
            $userEmail = isset($socialDataSession->email) ? $socialDataSession->email : null;
            $userPhone = isset($socialDataSession->phone) ? $socialDataSession->phone : null;
            $userCity = isset($socialDataSession->city) ? $socialDataSession->city : null;
            $userCountry = isset($socialDataSession->country) ? $socialDataSession->country : null;
            $userFirstName = isset($socialDataSession->firstName) ? $socialDataSession->firstName : null;
            $userLastName = isset($socialDataSession->lastName) ? $socialDataSession->lastName : null;


            $competitionApplication->setUserSocialData($socialData);
            $competitionApplication->setUserSocialLink($socialLink);
            $competitionApplication->setUserFirstname($userFirstName);
            $competitionApplication->setUserLastname($userLastName);
            $competitionApplication->setUserName($userName);
            $competitionApplication->setUserEmail($userEmail);
            $competitionApplication->setUserPhone($userPhone);
            $competitionApplication->setUserCity($userCity);
            $competitionApplication->setUserCountry($userCountry);
        }
    }

    public function preapreUploadFileName($dirDestination, $competitionApplicationEntity)
    {
        $fileData = $competitionApplicationEntity->getFilePath();
        if (!empty($fileData)) {
            $fileNameArr = explode('/', $fileData['tmp_name']);
            $newFileName = array_pop($fileNameArr);
            $competitionApplicationEntity->setFilePath($dirDestination . '/' . $newFileName);
        }
    }

    public function getCompetitionForm($competitionApplication)
    {
        $form = null;
        switch ($competitionApplication->getType()) {
            case CompetitionApplication::TYPE_CHILDREN:
                $form = $this->getFormForChildren($competitionApplication);
                break;
            case CompetitionApplication::TYPE_FRIENDS:
                $form = $this->getFormForFriends($competitionApplication);
                break;
        }
        return $form;
    }

    public function generateToken()
    {
        $code = 'competiton-token';
        $token = $code . '_' . microtime();
        return md5($token);
    }

    public function getNewCompetitionApplicationEntity($type)
    {
        $competitionSession = $this->getCompetitionSessionService()->getCompetitionSession();


        $competitionApplication = new CompetitionApplication();
        ;

        if ($this->getAuthService()->hasIdentity()) {
            $user = $this->getAuthService()->getIdentity();
            $competitionApplication->setUserId($user->getId());
        }

        $competitionApplication->setCreatedAt(new \DateTime());
        $competitionApplication->setStatus(CompetitionApplication::STATUS_NEW);

        $token = $this->generateToken();
        $competitionApplication->setToken($token);

        switch ($type) {
            case CompetitionApplication::TYPE_CHILDREN:
                $competitionApplication->setType(CompetitionApplication::TYPE_CHILDREN);
                $this->setSocialData($competitionApplication);
                break;
            case CompetitionApplication::TYPE_FRIENDS:
                $competitionApplication->setType(CompetitionApplication::TYPE_FRIENDS);
                $this->setSocialData($competitionApplication, 'linkedin');
                break;
        }
        return $competitionApplication;
    }

    public function canChangeStatusToVerify($competitionApplication)
    {
        $status = $competitionApplication->getStatus();
        $canChangeForStatus = array(CompetitionApplication::STATUS_NEW);
        if(in_array($status,$canChangeForStatus)){
            return true;
        }
        return false;
    }
    public function changeStatusToVerify($competitionApplication)
    {
        $competitionApplication->setStatus(CompetitionApplication::STATUS_TO_VERIFY);
        $this->getCompetitionAppService()->getCompetitionApplicationMapper()->save($competitionApplication);
    }
    
    public function changeStatusToActive($competitionApplication)
    {
        $competitionApplication->setStatus(CompetitionApplication::STATUS_ACTIVE);
        $this->getCompetitionAppService()->getCompetitionApplicationMapper()->save($competitionApplication);
    }

    public function checkExistEmailToRecommend($email)
    {
        $result = $this->getCompetitionAppService()->getCompetitionApplicationMapper()->checkRecommendEmail($email);
        if ($result > 0) {
            return true;
        }
        return false;
    }

    public function checkExistEmail($email)
    {
        $result = $this->getCompetitionAppService()->getCompetitionApplicationMapper()->checkUserEmail($email);
        if ($result > 0) {
            return true;
        }
        return false;
    }

    public function checkExistPhone($phone)
    {
        $result = $this->getCompetitionAppService()->getCompetitionApplicationMapper()->checkExistPhone($phone);
        if ($result > 0) {
            return true;
        }
        return false;
    }

    public function getCompetitionApplicationByToken($token)
    {
        return $this->getCompetitionAppService()->getCompetitionApplicationMapper()->getByToken($token);
    }

    public function sendToConfirmApplication($competitionApplication)
    {
        $translator = $this->getServiceManager()->get('translator');
        $headers = array(
            'to' => $competitionApplication->getUserEmail(),
        );
        $params['competitionApplication'] = $competitionApplication;

        $message = $this->getMailService()->compose($headers, 'mail/confirm_application.phtml', $params);
        $message->setSubject($translator->translate('mail_to_confirm_appliaction_title'));
        return $this->getMailService()->send($message);
    }

}
