<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/zf2 for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Competition\View\Helper;
use Zend\View\Helper\AbstractHtmlElement as AbstractHtmlElement;
/**
 * Helper for ordered and unordered lists
 */
class YoutubeThumb extends AbstractHtmlElement
{
    CONST YOUTUBE_URL = 'https://i.ytimg.com/vi';
    public static $sizes = array(
        'default'=>'default.jpg',
        'medium'=>'mqdefault.jpg',
        'high'=>'hqdefault.jpg',
        'standard'=>'sddefault.jpg',
        'maxres'=>'maxresdefault.jpg',
    );
    public function __invoke($youtubeUrl, $size='medium')
    {
        $matches=array();
         $rx = '~
            ^(?:https?://)?              # Optional protocol
             (?:www\.)?                  # Optional subdomain
             (?:youtube\.com|youtu\.be)  # Mandatory domain name
             /watch\?v=([^&]+)           # URI with video id as capture group 1
             ~x';

        $has_match = preg_match($rx, $youtubeUrl, $matches);
        if($has_match){
            $videoId = $matches[1];
            return self::YOUTUBE_URL.'/'.$videoId.'/'.self::$sizes[$size];
        }
        return null;
        
    }
}