<?php

namespace Competition\Validator;

use Zend\Validator\AbstractValidator;

class YoutubeValidator extends AbstractValidator
{
    const ERROR_YOUTUBE_URL = 'YOUTUBE_URL_ERROR';

    protected $competition_service;
    protected $messageTemplates = array(
        self::ERROR_YOUTUBE_URL => "error_youtube_url",
    );

    public function __construct($competition_service)
    {
        $this->competition_service = $competition_service;
        parent::__construct();
    }

    public function isValid($value)
    {
        
        $rx = '~
            ^(?:https?://)?              # Optional protocol
             (?:www\.)?                  # Optional subdomain
             (?:youtube\.com|youtu\.be)  # Mandatory domain name
             /watch\?v=([^&]+)           # URI with video id as capture group 1
             ~x';

        $has_match = preg_match($rx, $value, $matches);
        if(empty($has_match)){
            $this->error(self::ERROR_YOUTUBE_URL);
            $valid = false;
        }else{
            $valid = true;
        }
        return $valid;
    }
}
