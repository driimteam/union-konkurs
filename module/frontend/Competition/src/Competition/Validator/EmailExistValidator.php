<?php

namespace Competition\Validator;

use Zend\Validator\AbstractValidator;

class EmailExistValidator extends AbstractValidator
{
    const ERROR_EMAIL_EXIST = 'EMAIL_EXIST';

    protected $competition_service;
    protected $messageTemplates = array(
        self::ERROR_EMAIL_EXIST => "error_email_exist",
    );

    public function __construct($competition_service)
    {
        $this->competition_service = $competition_service;
        parent::__construct();
    }

    public function isValid($value)
    {
        $status = $this->competition_service->checkExistEmail($value);
        if($status){
            $this->error(self::ERROR_EMAIL_EXIST);
            $valid = false;
        }else{
            $valid = true;
        }
        return $valid;
    }
}
