<?php

namespace Competition\Validator;

use Zend\Validator\AbstractValidator;

class PhoneExistValidator extends AbstractValidator
{
    const ERROR_PHONE_EXIST = 'PHONE_EXIST';

    protected $competition_service;
    protected $messageTemplates = array(
        self::ERROR_PHONE_EXIST => "error_phone_exist",
    );

    public function __construct($competition_service)
    {
        $this->competition_service = $competition_service;
        parent::__construct();
    }

    public function isValid($value)
    {
        $status = $this->competition_service->checkExistPhone($value);
        if($status){
            $this->error(self::ERROR_PHONE_EXIST);
            $valid = false;
        }else{
            $valid = true;
        }
        return $valid;
    }
}
