<?php

namespace Competition\Form\Fieldset;

use Zend\Form\Element;
use Zend\Form\Form;
use Application\Form\Fieldset\Basic as BasicFieldset;
use CompetitionApp\Models\Entity\CompetitionApplication as CompetitionEntity;
use Zend\InputFilter\InputFilterProviderInterface;

//use Zend\Stdlib\Hydrator\ClassMethods as ClassMethodsHydrator;

class CompetitionApplicationFieldset extends BasicFieldset implements InputFilterProviderInterface
{

    protected $formElements = array();
    protected $inputFilterSpecification = array();
    protected $competition_service;
    public $uploadDir;

    public function __construct($sm, $uploadDir = null, $options = array())
    {
        parent::__construct('competitionFieldset', new CompetitionEntity(), $sm, $options);
        $this->uploadDir = $uploadDir;
    }

    public function getCompetitionService()
    {
        if (!$this->competition_service) {
            $this->competition_service = $this->sm->get('competition_service');
        }
        return $this->competition_service;
    }

    public function setUploadDir($uploadDir)
    {
        $this->uploadDir = $uploadDir;
    }

    public function getUploadDir()
    {
        return $this->uploadDir;
    }

    protected function setBasicFieldsToForm()
    {

        $this->formElements[] = $this->getHiddenElement('section')->setAttribute('class', 'js-section');
        $this->formElements[] = $this->getTextElement('youtubeLink', 'label_youtube_link')->setAttribute('class', 'youtubeurl');
        $this->formElements[] = $this->getTextareaElement('content', 'label_content')->setAttribute('class', 'required');
        $this->formElements[] = $this->getTextElement('userEmail', 'label_user_email')->setAttribute('class', 'required email');
        $this->formElements[] = $this->getTextElement('userName', 'label_user_name')->setAttribute('class', 'required');
//        $this->formElements[] = $this->getTextElement('userLastname', 'label_user_lastname')->setAttribute('class', 'required');
//        $this->formElements[] = $this->getTextElement('userPhone', 'label_user_phone')->setAttribute('class', 'required phone');
//        $this->formElements[] = $this->getTextElement('userAddress', 'label_user_address')->setAttribute('class', 'required');
//        $this->formElements[] = $this->getTextElement('userPostalcode', 'label_user_postalcode')->setAttribute('class', 'required');
//        $this->formElements[] = $this->getTextElement('userCity', 'label_user_city')->setAttribute('class', 'required');
//        $this->formElements[] = $this->getTextElement('userCountry', 'label_user_country')->setAttribute('class', 'required');

        $this->formElements[] = $this->getCheckboxElement('rule1', 'label_rule1', 1)->setAttribute('class', 'required');
        $this->formElements[] = $this->getCheckboxElement('rule2', 'label_rule2', 1)->setAttribute('class', 'required');
        $this->formElements[] = $this->getCheckboxElement('rule3', 'label_rule3', 1)->setAttribute('class', 'required');
        $this->formElements[] = $this->getCheckboxElement('rule4', 'label_rule4', 1)->setAttribute('class', 'required');
        $this->formElements[] = $this->getCheckboxElement('rule5', 'label_rule5', 1)->setAttribute('class', 'required');
        $this->formElements[] = $this->getCheckboxElement('rule6', 'label_rule6', 1)->setAttribute('class', 'required');
        
    }

    protected function addAll()
    {
        if (!empty($this->formElements)) {
            foreach ($this->formElements as $element) {
                $this->add($element);
            }
        }
    }

    public function prepareBasicFieldsetForText()
    {
        $this->setBasicFieldsToForm();
        $this->prepareBasicInputFilter();
        $this->addAll();

        $this->get('youtubeLink')->setAttribute('class', 'youtubeurl');
        $this->inputFilterSpecification['content'] = array(
            'required' => true,
            'filters' => array(
                array('name' => 'StringTrim'),
                array('name' => 'StripTags'),
                array('name' => 'App\\Filter\\HtmlSpecialchars')
            ),
            'properties' => array(
                'required' => true
            ),
            'validators' => array(
                
            )
        );
    }

    public function prepareBasicFieldsetForYoutube()
    {
        $this->setBasicFieldsToForm();
        $this->prepareBasicInputFilter();
        $this->addAll();

        $this->get('content')->setAttribute('class', '');
        $this->inputFilterSpecification['youtubeLink'] = array(
            'required' => true,
            'filters' => array(
                array('name' => 'StringTrim'),
                array('name' => 'StripTags'),
                array('name' => 'App\\Filter\\HtmlSpecialchars')
            ),
            'properties' => array(
                'required' => true
            ),
            'validators' => array(
                 new \Competition\Validator\YoutubeValidator($this->getCompetitionService())
            )
        );
    }

    protected function prepareBasicInputFilter()
    {
        $textFieldBasic = array(
            'required' => true,
            'filters' => array(
                array('name' => 'StringTrim'),
                array('name' => 'StripTags'),
                array('name' => 'App\\Filter\\HtmlSpecialchars')
            ),
            'properties' => array(
                'required' => true
            )
        );

        $this->inputFilterSpecification['userEmail'] = array(
            'required' => true,
            'filters' => array(
                array('name' => 'StringTrim'),
                array('name' => 'StripTags'),
                array('name' => 'App\\Filter\\HtmlSpecialchars')
            ),
            'properties' => array(
                'required' => true
            ),
            'validators' => array(
                array('name' => 'EmailAddress'),
                //new \Competition\Validator\EmailExistValidator($this->getCompetitionService())
            )
        );

        $this->inputFilterSpecification['section'] = $textFieldBasic;
        $this->inputFilterSpecification['userName'] = $textFieldBasic;

//        $this->inputFilterSpecification['userFirstname'] = $textFieldBasic;
//        $this->inputFilterSpecification['userLastname'] = $textFieldBasic;
//        $this->inputFilterSpecification['userAddress'] = $textFieldBasic;
//        $this->inputFilterSpecification['userPostalcode'] = $textFieldBasic;
//        $this->inputFilterSpecification['userCity'] = $textFieldBasic;
//        $this->inputFilterSpecification['userCountry'] = $textFieldBasic;
//        $this->inputFilterSpecification['userPhone'] = array(
//            'required' => true,
//            'filters' => array(
//                array('name' => 'StringTrim'),
//                array('name' => 'StripTags'),
//                array('name' => 'App\\Filter\\HtmlSpecialchars')
//            ),
//            'properties' => array(
//                'required' => true
//            ),
//            'validators' => array(
//                array('name' => 'StringLength', 'options' => array('min' => 11, 'max' => '12')),
//                new \Competition\Validator\PhoneExistValidator($this->getCompetitionService())
//            )
//        );

        $this->inputFilterSpecification['rule1'] = $textFieldBasic;
        $this->inputFilterSpecification['rule2'] = $textFieldBasic;
    }

    protected function prepareFileUplad()
    {
        $file = new \Zend\Form\Element\File('filePath');
        $file->setAttribute('class', 'required');
        $this->formElements[] = $file;


        $dirDestination = APPLICATION_PATH . '/data/upload/' . $this->uploadDir;
        if (!file_exists($dirDestination)) {
            mkdir($dirDestination, 0777, 1);
        }
        $this->inputFilterSpecification['filePath'] = array(
            'required' => true,
            'filters' => array(
                array(
                    'name' => 'File\RenameUpload',
                    'options' => array(
                        'target' => $dirDestination,
                        'randomize' => true,
                        'overwrite' => true,
                        'use_upload_extension' => true
                    ),
                ),
            ),
            'validators' => array(
                array('name' => "Zend\Validator\File\Size", 'options' => array('max' => '3MB')),
                array('name' => "Zend\Validator\File\Extension",
                    'options' => array(
                        'extension' => array('pdf', 'doc', 'docx', 'odt')
                    ))
            ),
            'properties' => array(
                'required' => true
            )
        );
    }

    /**
     * Define InputFilterSpecifications
     *
     * @access public
     * @return array
     */
    public function getInputFilterSpecification()
    {
        return $this->inputFilterSpecification;
    }

}
