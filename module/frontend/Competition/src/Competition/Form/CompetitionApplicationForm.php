<?php

namespace Competition\Form;

use Zend\Form\Element;
use Zend\Form\Form;
use Application\Form\BasicForm as BasicForm;
use Competition\Form\Fieldset\CompetitionApplicationFieldset;
use Zend\InputFilter\InputFilter;

class CompetitionApplicationForm extends BasicForm {

    protected $competitionApplicationFieldset;
    public function __construct($sm, $uploadDir=null, $options = array()) {
        parent::__construct('competitionFieldset', $sm, $options);
        $elementsObject = new CompetitionApplicationFieldset($sm,$uploadDir,$options);
        $elementsObject->setUseAsBaseFieldset(true); //, array('options' => array('use_as_base_fieldset' => true)));
        $this->competitionApplicationFieldset = $elementsObject;
        $this->add($elementsObject);
        $this->setAttribute('enctype', 'multipart/form-data');
    }

    public function setUploadDir($uploadDir)
    {
        $this->competitionApplicationFieldset->setUploadDir($uploadDir);
        return $this;
    }

    public function getUploadDir()
    {
        return $this->competitionApplicationFieldset->getUploadDir();
    }

}
