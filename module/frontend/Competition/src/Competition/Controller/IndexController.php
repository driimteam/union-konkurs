<?php

namespace Competition\Controller;

use Zend\Mvc\Controller\AbstractActionController,
    Zend\View\Model\ViewModel,
    Zend\Mvc\MvcEvent,
    Doctrine\ORM\EntityManager,
    Competition\Form\CompetitionApplicationForm,
    Competition\Form\SmsForm,
    CompetitionApp\Models\Entity\CompetitionApplication,
    CompetitionApp\Models\Entity\Competition as CompetitionEntity;

class IndexController extends AbstractActionController
{

    protected $appService;
    protected $competition_session_service;
    protected $competitionapp_service;
    protected $competition_service;

    public function setEventManager(\Zend\EventManager\EventManagerInterface $events)
    {
        parent::setEventManager($events);
        return $this;
    }

    public function onDispatch(MvcEvent $e)
    {

        return parent::onDispatch($e);
    }

    public function getAppService()
    {
        if (!$this->appService) {
            $this->appService = $this->getServiceLocator()->get('application_service');
        }
        return $this->appService;
    }

    public function getCompetitionAppService()
    {
        if (!$this->competitionapp_service) {
            $this->competitionapp_service = $this->getServiceLocator()->get('competitionapp_service');
        }
        return $this->competitionapp_service;
    }

    public function getCompetitionService()
    {
        if (!$this->competition_service) {
            $this->competition_service = $this->getServiceLocator()->get('competition_service');
        }
        return $this->competition_service;
    }

    public function saveAction()
    {
        return $this->redirect()->toRoute('home', array());
        $viewModel = new ViewModel();
        if ($type = $this->getEvent()->getRouteMatch()->getParam('type', null)) {
            $types = CompetitionApplication::getTypes();
            if (in_array($type, $types)) {
                $request = $this->getRequest();

                $competitionApplication = $this->getCompetitionService()->getNewCompetitionApplicationEntity($type);
                $section = CompetitionApplication::SECTION_TEXT;
                if ($fieldsetCompetition = $this->params()->fromPost('competitionFieldset', null)) {
                    $section = $fieldsetCompetition['section'];
                }
                $competitionApplication->setSection($section);

                $form = $this->getCompetitionService()->getCompetitionForm($competitionApplication);
                if (!$form) {
                    return $this->redirect()->toRoute('home', array());
                }
                //var_dump($form);exit;

                if ($request->isPost()) {
                    $postData = $request->getPost()->getArrayCopy();
                    $fileData = $request->getFiles()->toArray();
                    $formData = array_merge_recursive($postData, $fileData);
                    $form->setData($formData);

                    if ($form->isValid()) {
                        if ($form->getUploadDir()) {
                            $this->getCompetitionService()->preapreUploadFileName($form->getUploadDir(), $competitionApplication);
                        }
                        
                        if ($this->getCompetitionService()->getOptions()->getVerificationEmailEnabled()) {
                            $this->getCompetitionService()->sendToConfirmApplication($competitionApplication);
                        }
                        $this->getCompetitionAppService()->getCompetitionApplicationMapper()->save($competitionApplication);
                        
                        return $this->redirect()->toRoute('finishedSuccess', array());
                    } else {
                        $this->flashMessenger()->addErrorMessage('form_invalid_data');
                    }
                }

                $viewModel->type = $type;
                $viewModel->section = $section;
                $viewModel->form = $form;
            } else {
                return $this->redirect()->toRoute('home', array());
            }
        } else {
            return $this->redirect()->toRoute('home', array());
        }
        return $viewModel;
    }

    public function rulesAction()
    {
        $viewModel = new ViewModel();
        return $viewModel;
    }

    public function finishedSuccessAction()
    {
        $viewModel = new ViewModel();
        return $viewModel;
    }

    public function galleryVideoAction()
    {
        $viewModel = new ViewModel;
        $startLimit = 9;
        $params = array();
        $page = $this->getEvent()->getRouteMatch()->getParam('page', 1);
        $limit = $this->getEvent()->getRouteMatch()->getParam('limit', $startLimit);
        $paginator = $this->getCompetitionAppService()->getListPagerBySection($page, $limit, CompetitionApplication::SECTION_YOUTUBE);
        $viewModel->paginator = $paginator;
        $viewModel->params = $params;
        $viewModel->page = $page;
        $viewModel->startPosition = ($page - 1) * $limit + 1;

        if ($this->getRequest()->getQuery('disablelayout', null)) {
            $viewModel->setTerminal(true);
            $viewModel->setTemplate('competition/index/videolist');
        }
        return $viewModel;
    }

    public function galleryTextAction()
    {
        $viewModel = new ViewModel;
        $startLimit = 9;
        $params = array();
        $page = $this->getEvent()->getRouteMatch()->getParam('page', 1);
        $limit = $this->getEvent()->getRouteMatch()->getParam('limit', $startLimit);
        $paginator = $this->getCompetitionAppService()->getListPagerBySection($page, $limit, CompetitionApplication::SECTION_TEXT);
        $viewModel->paginator = $paginator;
        $viewModel->params = $params;
        $viewModel->page = $page;
        $viewModel->startPosition = ($page - 1) * $limit + 1;

        if ($this->getRequest()->getQuery('disablelayout', null)) {
            $viewModel->setTerminal(true);
            $viewModel->setTemplate('competition/index/textlist');
        }
        return $viewModel;
    }

    public function confirmapplicationAction()
    {
        $viewModel = new ViewModel();
        if ($token = $this->getEvent()->getRouteMatch()->getParam('token', null)) {
            $competitionApplication = $this->getCompetitionService()->getCompetitionApplicationByToken($token);
            if ($competitionApplication) {
                $canChange = $this->getCompetitionService()->canChangeStatusToVerify($competitionApplication);
                if($canChange){
                    $this->getCompetitionService()->changeStatusToVerify($competitionApplication);
                }
                $viewModel->competitionApplication = $competitionApplication;
                return $viewModel;
            } else {
                $this->flashMessenger()->addErrorMessage('msg_cofirm_application_error');
                return $this->redirect()->toRoute('home', array());
            }
        } else {
            return $this->redirect()->toRoute('home', array());
        }
    }

    public function indexAction()
    {
        $viewModel = new ViewModel();
        $this->layout()->homepage = true;
        return $viewModel;
    }
    
    public function winnersAction()
    {
        $viewModel = new ViewModel();
        return $viewModel;
    }
}
