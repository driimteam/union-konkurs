<?php
namespace Competition;
$competitionOptions = [
    'verification_email_enabled'=>true

];
return array(
    "competition_module_options"=>$competitionOptions,
    'service_manager' => array(
        'invokables'=>array(
            'competition_session_service' => 'Competition\Service\SessionService',
            'linkedin_service' => 'Competition\Service\LinkedInService',
        ),
        'factories' => array(
                'competition_service' => "Competition\Factory\CompetitionServiceFactory",
                'competition_options' => "Competition\Factory\ModuleOptionsFactory"
            ),
     ),
    'controllers' => array(
        'invokables' => array(
            'Competition\Controller\Index' => 'Competition\Controller\IndexController'
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
     'view_helpers' => array(
        'invokables'=> array(
            'youtubeThumb'=>'\Competition\View\Helper\YoutubeThumb',
            'youtubeVideoId'=>'\Competition\View\Helper\YoutubeVideoId'
        )
    ),
);