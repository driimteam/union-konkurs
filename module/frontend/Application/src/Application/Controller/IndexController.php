<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController,
    Zend\View\Model\ViewModel,
    Doctrine\ORM\EntityManager;

class IndexController extends AbstractActionController
{
    protected $appService;
    protected $statService;
    protected $tshirt_session_service;
    protected $tshirtapp_service;

    public function getAppService()
    {
        if (!$this->appService) {
            $this->appService = $this->getServiceLocator()->get('application_service');
        }
        return $this->appService;
    }

    public function getTshirtSessionService()
    {
        if (!$this->tshirt_session_service) {
            $this->tshirt_session_service = $this->getServiceLocator()->get('tshirt_session_service');
        }
        return $this->tshirt_session_service;
    }

    public function getTshirtAppService()
    {
        if (!$this->tshirtapp_service) {
            $this->tshirtapp_service = $this->getServiceLocator()->get('tshirtapp_service');
        }
        return $this->tshirtapp_service;
    }

    public function indexAction()
    {
        $params             = array();
        $viewModel          = new ViewModel();
        
        return $viewModel;
    }
}