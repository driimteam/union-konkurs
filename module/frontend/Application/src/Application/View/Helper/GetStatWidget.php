<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/zf2 for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\View\Helper;

use Zend\View\Helper\AbstractHtmlElement as AbstractHtmlElement;

/**
 * Helper for ordered and unordered lists
 */
class GetStatWidget extends AbstractHtmlElement
{
    protected $statService = null;

    public function __construct($statService)
    {
        $this->statService = $statService;
    }

    public function getStatisticsService()
    {
        return $this->statService;
    }

    protected function calculatePercentWinners($nrUsers, $nrGames, $nrLosers)
    {
        $allUsersWithoutCurrentUser = $nrUsers - $nrGames;
        $percent                    = 0;
        if ($allUsersWithoutCurrentUser > 0) {
            $percent = (100 * $nrLosers) / $allUsersWithoutCurrentUser;
        }
        return $percent;
    }

    public function __invoke($user, $theme = 'orange', $percent = 0)
    {
        $params                = array();
        $params['nrQuestions'] = $this->getStatisticsService()->getNrQuestionsByUser($user);
        $params['nrGames']     = $this->getStatisticsService()->getNrGamesByUser($user);
        $params['nrLifebuoys'] = $this->getStatisticsService()->getNrLifebuoysByUser($user);
        $params['nrLosers']    = $nrLosers              = $this->getStatisticsService()->getTotalNrLosersForUser($user);
        $params['totalAmount'] = $totalAmount           = $this->getStatisticsService()->getTotalBalanceByUser($user);
        $params['totalEarned'] = $totalEarned           = $this->getStatisticsService()->getTotalEarnedByUser($user);

        if ($nrLosers > 0){
            $params['placeInSuccessfulRanking'] = $this->getStatisticsService()->getPlaceInSuccessfulRanking($nrLosers);
            $params['placeInMostEarnedRanking'] = $this->getStatisticsService()->getPlaceInMostEarnedRanking($totalEarned);
        }else {
            $params['placeInSuccessfulRanking'] = 0;
            $params['placeInMostEarnedRanking'] = 0;
        }

        $nrTotalUsersInUserQuiz = $this->getStatisticsService()->getNrAllUsersInGamesByUser($user);

        $percent = $this->calculatePercentWinners($nrTotalUsersInUserQuiz,
            $params['nrGames'], $params['nrLosers']);

        $params['user']    = $user;
        $params['theme']   = $theme;
        $params['percent'] = $percent;
        return $this->getView()->render('widgets/stat', $params);
    }
}