<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/zf2 for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\View\Helper;
use Zend\View\Helper\AbstractHtmlElement as AbstractHtmlElement;
/**
 * Helper for ordered and unordered lists
 */
class GetCircleColor extends AbstractHtmlElement
{
   
   
    public function __invoke($type)
    {
        switch($type){
            case 'NORMAL':
                return 'navy';
            break;
            case 'CUMULATION':
                return 'orange';
            break;
            case 'FREE':
                return 'blue';
            break;
            default:
            break;
        }
        
    }
}