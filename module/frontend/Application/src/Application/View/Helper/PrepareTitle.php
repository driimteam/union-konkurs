<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/zf2 for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\View\Helper;
use Zend\View\Helper\AbstractHtmlElement as AbstractHtmlElement;
/**
 * Helper for ordered and unordered lists
 */
class PrepareTitle extends AbstractHtmlElement
{
   
   
    public function __invoke($string)
    {
        $stringArr = explode(" ",$string);
        $newStringArr=array();
        $counter = count($stringArr);
        $i=1;
        $skipToNext='';
        foreach($stringArr as $stringone){
            
            if((strlen($stringone)<=2) && ($i<$counter)){
                $skipToNext.= $stringone.'&nbsp;';
            }else{
                $stringone=$skipToNext.$stringone;
                $skipToNext=null;
            }
            
            if(empty($skipToNext)){
                if(($i>1) && ($i<=$counter)){
                    $stringone = ' '.$stringone;
                }
                $newStringArr[]='<span>'.$stringone.'</span>';
            }
            $i++;
        }
        $newString = implode('',$newStringArr);
        return $newString;
        
    }
}