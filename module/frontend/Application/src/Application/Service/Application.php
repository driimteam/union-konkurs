<?php

namespace Application\Service;

use Zend\Form\Form;
use Zend\ServiceManager\ServiceManagerAwareInterface;
use Zend\ServiceManager\ServiceManager;
use Zend\Crypt\Password\Bcrypt;
use Zend\Stdlib\Hydrator;
use Zend\Authentication\Storage\Session as StorageSession;
use Zend\Stdlib\DispatchableInterface;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;
use Zend\Form\Annotation\AnnotationBuilder;

class Application implements ServiceManagerAwareInterface
{
    /**
     * @var pluginMapper
     */
    protected $translator;
    protected $headTitle = array();
    protected $auth;
    protected $em;

    /**
     * @var ServiceManager
     */
    protected $serviceManager;
    protected $eventManager;

    /**
     * @var UserServiceOptionsInterface
     */

    /**
     * @var Hydrator\ClassMethods
     */
    protected $formHydrator;

    /**
     * Retrieve service manager instance
     *
     * @return ServiceManager
     */
    public function getServiceManager()
    {
        return $this->serviceManager;
    }

    /**
     * Set service manager instance
     *
     * @param ServiceManager $serviceManager
     * @return User
     */
    public function setServiceManager(ServiceManager $serviceManager)
    {
        $this->serviceManager = $serviceManager;
        return $this;
    }

    public function setEntityManager(EntityManager $em)
    {
        $this->em = $em;
    }

    public function getEntityManager()
    {
        if (null === $this->em) {
            $this->em = $this->getServiceManager()->get('Doctrine\ORM\EntityManager');
        }
        return $this->em;
    }

    public function getTranslator()
    {
        if (null === $this->translator) {
            $this->translator = $this->getServiceManager()->get('translator');
        }
        return $this->translator;
    }

    public function setHeadTitle($title)
    {
        $titleHelper = $this->getServiceManager()->get('viewhelpermanager')->get('headTitle');
        $titleHelper()->prepend($title);
    }

    public function setNameAuthSessionStorage($nameStorage)
    {
        $auth = $this->getAuth();
        $auth->setStorage(new StorageSession($nameStorage));
    }

    public function getIdentity()
    {
        /* $user = new \Application\Models\Entity\User();
          $user->agent_id = 444444;
          $user->group_id = 1;
          $user->competition_id = 2;
          return $user; */
        if ($this->getAuth()->hasIdentity()) {
            return $this->getAuth()->getIdentity();
        }
        return false;
        ;
    }

    public function getConfig()
    {
        $config = $this->getServiceManager()->get('Configuration');
        return $config;
    }

    public function getNavigationContainerById($idName)
    {
        $navigationHelper = $this->getServiceManager()->get('viewhelpermanager')->get('navigation');
        $navigation       = $navigationHelper('Navigation');
        $container        = $navigation->getContainer();
        $competitionsPage = $container->findOneBy('id', $idName);
        return $competitionsPage;
    }

    public function initNavigation($navigationSection)
    {
        $navigationHelper = $this->getServiceManager()->get('viewhelpermanager')->get('navigation');
        $navigation       = $navigationHelper($navigationSection);
        $container        = $navigation->getContainer();
        $page             = $container->findBy('active', 'true');
        $this->setHeadTitles($page, $container);
    }

    public function setSubmenu($page = null, $pageId = null,
                               $navigationName = 'navigationSubmenu')
    {
        $navigationHelper = $this->getServiceManager()->get('viewhelpermanager')->get('navigation');
        $navigation       = $navigationHelper('Navigation');
        $container        = $navigation->getContainer();
        if ($page == null) {
            if ($pageId == null) {
                $page = $container->findBy('active', 'true');
            } else {
                $page = $container->findBy('id', $pageId);
            }
            $page->active = 1;
        }
        $application                = $this->getServiceManager()->get('Application');
        $viewModel                  = $application->getMvcEvent()->getViewModel();
        $viewModel->$navigationName = $page;
        return $page;
    }

    public function setHeadTitles($active, $container)
    {
        $titleHelper = $this->getServiceManager()->get('viewhelpermanager')->get('headTitle');
        while (is_object($active) && ($parent      = $active->getParent())) {
            $titleHelper($active->getLabel());
            if ($parent === $container) {
                break;
            }
            $active = $parent;
        }
    }

    public function createForm($entity)
    {
        $builder  = new AnnotationBuilder($this->getEntityManager());
        $hydrator = new DoctrineHydrator($this->getEntityManager());
        $form     = $builder->createForm($entity);
        $form->setHydrator($hydrator);
        return $form;
    }

    public function getNewHydrator()
    {
        $em = $this->getEntityManager();
        return new DoctrineHydrator($em);
    }
}
