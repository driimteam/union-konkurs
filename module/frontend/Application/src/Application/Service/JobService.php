<?php

namespace Application\Service;

use Zend\Form\Form;
use Zend\ServiceManager\ServiceManagerAwareInterface;
use Zend\ServiceManager\ServiceManager;
use Zend\Crypt\Password\Bcrypt;
use Zend\Stdlib\Hydrator;
use Zend\Authentication\Storage\Session as StorageSession;
use Zend\Stdlib\DispatchableInterface;

class JobService implements ServiceManagerAwareInterface {

    const JOB_MESSAGE_UPDATE_SESSION = 'UPDATE_SESSION_USER';
    const JOB_NAMESPACE = 'JOB';

    protected $serviceManager;
    protected $eventManager;
    protected $user_service;

    /**
     * @var UserServiceOptionsInterface
     */

    /**
     * Retrieve service manager instance
     *
     * @return ServiceManager
     */
    public function getServiceManager() {
        return $this->serviceManager;
    }

    /**
     * Set service manager instance
     *
     * @param ServiceManager $serviceManager
     * @return User
     */
    public function setServiceManager(ServiceManager $serviceManager) {
        $this->serviceManager = $serviceManager;
        return $this;
    }

    public function getUserService() {
        if ($this->user_service === null) {
            $this->user_service = $this->getServiceManager()->get('user_service');
        }
        return $this->user_service;
    }

    public function checkJobs() {
        $flashMessenger = $this->getServiceManager()->get('ControllerPluginManager')->get('flashMessenger');
        $flashMessenger->setNamespace(self::JOB_NAMESPACE);
        if ($flashMessenger->hasMessages()) {
            foreach ($flashMessenger->getMessages() as $message) {
                $this->job($message);
            }
        }
    }

    public function job($name) {
        switch ($name) {
            case self::JOB_MESSAGE_UPDATE_SESSION:
                $this->updateUserSession();
                break;
            default:
                break;
        }
    }

    public function updateUserSession() {
        $identity = $this->getUserService()->getAuthService()->getIdentity();
        if ($identity) {
            $userMapper = $this->getUserService()->getUserMapper();
            $user = $userMapper->findById($identity->getId());
            $money=$user->getWallet()->getMoney();
            $identity->getWallet()->setMoney($money);
        }
    }

}
