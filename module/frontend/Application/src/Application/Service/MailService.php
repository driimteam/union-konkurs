<?php

namespace Application\Service;

use Zend\Form\Form;
use Zend\ServiceManager\ServiceManagerAwareInterface;
use Zend\ServiceManager\ServiceManager;
use Zend\Crypt\Password\Bcrypt;
use Zend\Stdlib\Hydrator;
use Zend\Stdlib\DispatchableInterface;
use Zend\Mail\Transport\Smtp as SmtpTransport;
use Zend\Mail\Transport\SmtpOptions;
use Zend\Mail;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;

class MailService implements ServiceManagerAwareInterface {

    protected $transport; 
    protected $mailFrom; 
    protected $mailFromName; 
    protected $mailTo; 
    protected $mailToName; 
    /**
     * @var ServiceManager
     */
    protected $serviceManager;
    protected $eventManager;

    /**
     * Retrieve service manager instance
     *
     * @return ServiceManager
     */
    public function getServiceManager() {
        return $this->serviceManager;
    }

    public function setServiceManager(ServiceManager $serviceManager) {
        $this->serviceManager = $serviceManager;
        return $this;
    }
    
    public function getTransport(){
        if($this->transport === null){
            $config = $this->getServiceManager()->get('Configuration');
            $transport = new SmtpTransport();                
            $transport->setOptions(new SmtpOptions($config['mail']['transport']['options']));
            $this->transport = $transport;
        }
        return $this->transport;
    }
    public function getMailFrom(){
        $config = $this->getServiceManager()->get('Configuration');
        return $config['mail']['mailFrom'];
    }
    public function getMailFromName(){
        $config = $this->getServiceManager()->get('Configuration');
        return $config['mail']['mailFromName'];
    }
    public function getMailTo(){
        $config = $this->getServiceManager()->get('Configuration');
        return $config['mail']['mailTo'];
    }
    public function getMailNameTo(){
        $config = $this->getServiceManager()->get('Configuration');
        return $config['mail']['mailNameTo'];
    }
    
    public function sendMail($subject, $bodyContent, $mailTo=null, $mailName=null){
        $html = new MimePart($bodyContent);
        $html->type = "text/html";
        $html->charset = 'utf-8';
        $body = new MimeMessage();
        $body->setParts(array($html));
        
        $mail = new Mail\Message();
        $mail->setBody($body);
        
        $mail->setFrom($this->getMailFrom(), $this->getMailFromName());
        
        $mail->setEncoding('UTF-8');
        if(!$mailTo){
            $mail->addTo($this->getMailTo(), $this->getMailNameTo());
        }else{
            $mail->addTo($mailTo, $mailName);
        }
        
        $mail->setSubject($subject);
        $transport = $this->getTransport();
        $transport->send($mail);
    }
    
    
    
}
