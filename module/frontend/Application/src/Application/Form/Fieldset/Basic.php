<?php
namespace Application\Form\Fieldset;
use Zend\Form\Element;
use Zend\Form\Form;
use Zend\Form\Fieldset;

use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Stdlib\Hydrator\ClassMethods as ClassMethodsHydrator;

class Basic extends Fieldset implements InputFilterProviderInterface
{
    protected $formElements;
    protected $filter;
    protected $options;
    protected $sm;
    protected $applicationService;
    protected $em;

    public function getApplicationService() {
        if (!$this->applicationService) {
            $this->applicationService = $this->sm->get('application_service');
        }
        return $this->applicationService;
    }

    public function getEntityManager() {
        return $this->getApplicationService()->getEntityManager();
    }
    
    public function __construct($name, $object,$sm,$options=array())
    {
        parent::__construct($name,$options);
        $this->sm = $sm;
        $this->options = $options;
        $hydrator = $this->getApplicationService()->getNewHydrator();
        $this->setHydrator($hydrator)->setObject($object);

    }
    
    /**
     * Define InputFilterSpecifications
     *
     * @access public
     * @return array
     */
    public function getInputFilterSpecification() {
        return array();
    }
    
    public function getTextElement($name, $label) {
        $element = new \Zend\Form\Element\Text($name);
        $element->setOptions(array(
            'label' => $label,
            'partial' => "formpartial/text"
        ));
        return $element;
    }

     public function getTextareaElement($name, $label)
    {
        $element = new \Zend\Form\Element\Textarea($name);
        $element->setOptions(array(
            'label' => $label,
            'partial' => "formpartial/textarea"
        ));
        return $element;
    }
    
    public function getHiddenElement($name) {
        $element = new \Zend\Form\Element\Hidden($name);

        $element->setOptions(array(
            'partial' => null
        ));
        return $element;
    }

    public function getLeadElement($name, $label) {
        $element = new \Zend\Form\Element\Text($name);
        $element->setOptions(array(
            'label' => $label,
            'partial' => "formpartial/lead"
        ));
        return $element;
    }
    
    public function getNumberElement($name, $label) {
        $element = new \Zend\Form\Element\Number($name);
        $element->setOptions(array(
            'label' => $label,
            'partial' => "formpartial/number"
        ));
        return $element;
    }

    public function getDateElement($name, $label) {
        $element = new \Zend\Form\Element\Text($name);
        $element->setOptions(array(
            'label' => $label,
            'partial' => "formpartial/date"
        ));
        return $element;
    }
    
     public function getTimeElement($name, $label) {
        $element = new \Zend\Form\Element\Text($name);
        $element->setOptions(array(
            'label' => $label,
            'partial' => "formpartial/time"
        ));
        return $element;
    }

    public function getMainPictureElement($name, $label) {
        $element = new \Zend\Form\Element\Hidden($name);
        $element->setOptions(array(
            'label' => $label,
            'partial' => "formpartial/filemanager"
        ));
        return $element;
    }

    public function getButtonElement($name,$label) {
        $element = new \Zend\Form\Element\Button($name);
        $element->setOptions(array(
            'label' => $label,
            'partial' => "formpartial/button"
        ));
        $element->setValue($label);
        return $element;
    }

    public function getTinymceElement($name, $label) {
        $element = new \Zend\Form\Element\Textarea($name);
        $element->setOptions(array(
            'label' => $label,
            'partial' => "formpartial/tinymce"
        ));
        return $element;
    }
    
    public function getRadioElement($name, $label, $options) {

        $radioStatus = new \Zend\Form\Element\Radio($name);
        $radioStatus->setLabel($label);
        $radioStatus->setValueOptions($options);
        
        $radioStatus->setOptions(array('partial' => 'formpartial/multiradio'));
        return $radioStatus;
    }

    public function getCheckboxElement($name, $label,$value) {

        $checkbox = new \Zend\Form\Element\Checkbox($name);
        $checkbox->setLabel($label);
        $checkbox->setCheckedValue($value);
        $checkbox->setOptions(array('partial' => 'formpartial/checkbox'));
        return $checkbox;
    }
    
    public function getSelectElement($name, $label, $options) {

        $element = new \Zend\Form\Element\Select($name);
        $element->setLabel($label);
        $element->setValueOptions($options);
        $element->setOptions(array('partial' => 'formpartial/select'));
        return $element;
    }
    
}
