<?php

namespace Application\Form;

use Zend\Form\Form;
use Application\Form\BasicForm;
use Application\Form\Filter\ContactFilter;

class ContactForm extends BasicForm {

    protected $filter;
    protected $node;
    protected $content;
    protected $options=array();
    
    public function __construct($content, $options = array()) {
        // we want to ignore the name passed
        parent::__construct($options);
        
        $this->options = $options;
        $this->content = $content;
        //$this->_serviceCms ;
        //$this->_translator = $translator;
        
        $this->formElements[] = $this->getTextElement('email', 'E-mail')
                ->setAttribute('class', 'required email')->setOptions(array(
                    'label' => $this->_translator->translate('form_label_email') ,
                    'partial' => "formpartial/contact/text"
                ));
        
        
        //var_dump($content->getContentLang()->lead);exit;
        $subjectsContent = $content->getContentLang()->lead;
        $subjectsArr = explode("\n",$subjectsContent);
        $optionsSubject =array();
        $firstValue = null;
        if(!empty($subjectsArr)){
            $i=0;
            foreach($subjectsArr as $subject){
                
                if(!empty($subject)){
                    if($i==0){
                        $firstValue=trim($subject);
                    }
                    $optionsSubject[trim($subject)]=trim($subject);
                }
                $i++;
            }
        }
        //$optionsSubject[$this->_translator->translate('contact_form_subject_1')] = $this->_translator->translate('contact_form_subject_1');
        //$optionsSubject[$this->_translator->translate('contact_form_subject_2')] = $this->_translator->translate('contact_form_subject_2');
        
        $this->formElements[] = $this->getRadioElement('subject', $this->_translator->translate('form_label_subject'),$optionsSubject)
                ->setAttribute('class', 'required')->setOptions(array(
                    'label' => $this->_translator->translate('form_label_subject'),
                    'partial' => "formpartial/contact/multiradio"
                ))->setValue($firstValue);
        
        ;
        $this->formElements[] = $this->getTextareaElement('text', $this->_translator->translate('form_label_textarea'))
                ->setAttribute('class', 'required')->setOptions(array(
                    'label' => $this->_translator->translate('form_label_textarea'),
                    'partial' => "formpartial/contact/textarea"
                ));
        
        foreach($this->formElements as $element){
            $this->add($element);
        }
       
        $inputFilter = $this->geFilter();
        $this->setInputFilter($inputFilter);
    }
    
    public function geFilter() {
        if($this->filter==null){
            $this->filter = new ContactFilter($this->options);
        }
        return $this->filter;
        
    }
}
