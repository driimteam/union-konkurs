<?php

namespace Application\Form\Filter;

use Zend\InputFilter\InputFilter;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\EventManager;
use App\Filter\HtmlSpecialchars;

class ContactFilter extends InputFilter
{
    public function __construct($options)
    {
        
        $factory = $this->getFactory();
        $this->add($factory->createInput(array(
                'name'       => 'email',
                'required'   => true,
                'filters' => array(
                    array('name' => 'App\\Filter\\HtmlSpecialchars'),
                    array('name'    => 'StringTrim'),
                ),
                'validators' => array(array('name' => 'EmailAddress')),
            )));
        
        $this->add($factory->createInput(array(
                'name'       => 'subject',
                'required'   => true,
                'filters' => array(
                    array('name' => 'App\\Filter\\HtmlSpecialchars'),
                    array('name'    => 'StringTrim'),
                )
            )));
        $this->add($factory->createInput(array(
                'name'       => 'text',
                'required'   => true,
                'filters' => array(
                    array('name' => 'App\\Filter\\HtmlSpecialchars'),
                    array('name'    => 'StringTrim'),
                )
            )));
        
        
        
    }
}