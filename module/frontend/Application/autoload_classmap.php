<?php
return array(
    'Tools_Multiupload' => __DIR__ . '/../../commons/Tools/Multiupload.php',
    'Tools_Image' => __DIR__ . '/../../commons/Tools/Image.php',
    'Tools_File' => __DIR__ . '/../../commons/Tools/File.php',
);
