<?php
namespace Application;
return array(
     'controllers' => array(
        'invokables' => array(
            'Application\Controller\Index' => 'Application\Controller\IndexController',
            'Application\Controller\NotLuxoftEmployee' => 'Application\Controller\NotLuxoftEmployeeController',
            'Application\Controller\LuxoftEmployee' => 'Application\Controller\LuxoftEmployeeController',
            'Application\Controller\Cv' => 'Application\Controller\CvController',
        ),
    ),
    
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'exception_layout'       => 'layout/layouterror',
        'template_map' => array(
            'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'layout/mainpage_layout'           => __DIR__ . '/../view/layout/mainpage_layout.phtml',
            'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
            'paginator-slide' => __DIR__ . '/../view/layout/slidePaginator.phtml',
            'navigation' => __DIR__ . '/../view/layout/navigation.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
    'view_helpers' => array(
        'invokables'=> array(
            'currentPage'=>'\Application\View\Helper\CurrentPage',
            'truncate'=>'\Application\View\Helper\Truncate',
            'getCircleColor'=>'\Application\View\Helper\GetCircleColor'
        )
    ),
);