<?php
namespace ApplicationTest;
$env = getenv('APPLICATION_ENV') ?: 'production';
return array(
    'modules' => array(
        'AssetManager',
        'DoctrineModule',
        'DoctrineORMModule',
        'DoctrineExtensions',
        'ScnSocialAuth',
        'ZfcRbac',
        'ZfcBase',
        'ZfcUser',
        'ZfcUserDoctrineORM',
        'MtMail',
        'HtUserRegistration',
        'UserApp',
        'User',
        'app',
        'Application',
        'QuizApp',
        'Quiz',
        'PaymentApp',
        'Payment',
        'Przelewy24'
        
        
    ),
    'module_listener_options' => array(
        'config_glob_paths'    => array(
            'config/frontend/autoload/{,*.}{global,local}.php',
            'config/frontend/autoload/{,*.}' . $env. '.php',
            'config/frontend/autoload/application/{,*.}{global,local}.php',
            'config/frontend/autoload/user/{,*.}{global,local}.php',
            //'config/frontend/autoload/cms/{,*.}{global,local}.php',
            'config/frontend/autoload/payment/{,*.}{global,local}.php',
            'config/frontend/autoload/quiz/{,*.}{global,local}.php',
            'config/frontend/autoload/mail/{,*.}{global,local}.php',
        ),
        'module_paths' => array(
            './module/frontend',
            './module/commons',
            './vendor/doctrine',
            './vendor/zf-commons',
            './vendor/socalnick',
            './vendor',
        ),
        'config_cache_enabled' => false,

        'config_cache_key' => 'app_config',

        // Use the $env value to determine the state of the flag
        'module_map_cache_enabled' => false,

        'module_map_cache_key' => 'module_map',

        'cache_dir' => 'data/frontend/cache',

        // Use the $env value to determine the state of the flag
        'check_dependencies' => false,
    )
);