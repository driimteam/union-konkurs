<?php

namespace ApplicationTest\Controller;

use ApplicationTest\Bootstrap;
use Application\Controller\IndexController;
use Zend\Mvc\Router\Http\TreeRouteStack as HttpRouter;
use Zend\Http\Request;
use Zend\Http\Response;
use Zend\Mvc\MvcEvent;
use Zend\Mvc\Router\RouteMatch;
use PHPUnit_Framework_TestCase;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;

class IndexControllerTest extends AbstractHttpControllerTestCase {

    protected $controller;

    public function setUp() {
        $this->setApplicationConfig(
                include 'TestConfig.php'
        );
        parent::setUp();
        $authMock = $this->getMock('ZfcUser\Controller\Plugin\ZfcUserAuthentication');
        $this->getApplicationServiceLocator()->setAllowOverride(true);
        $this->getApplicationServiceLocator()->setService('zfcUserAuthentication', $authMock);
    }

    protected function mockLogin() {
        $authService = $this->getMock('Zend\Authentication\AuthenticationService');

        $authService->expects($this->any())
                ->method('getIdentity')
                ->will($this->returnValue(1));

        $authService->expects($this->any())
                ->method('hasIdentity')
                ->will($this->returnValue(true));

        $authService->expects($this->any())
                ->method('getRole')
                ->will($this->returnValue(2));
        $this->getApplicationServiceLocator()->setAllowOverride(true);
        $this->getApplicationServiceLocator()->setService('Zend\Authentication\AuthenticationService', $authService);
    }
    
    public function testHomePage() {
        $this->dispatch('/');
        $this->assertResponseStatusCode(200);
    }
    public function testRules() {
        $this->dispatch('/zasady');
        $this->assertResponseStatusCode(200);
    }

}