<?php

namespace Application;

use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use Doctrine\ORM\Mapping\Driver\XmlDriver;
use Application\Module as Application;
use Zend\ModuleManager\ModuleManager;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ModuleManager\Feature\ServiceProviderInterface;
use Zend\Stdlib\Hydrator\ClassMethods;
use Zend\Mvc\MvcEvent;
use Zend\View\Helper;
use Zend\Mvc\ModuleRouteListener;
use Zend\View\Model\ViewModel;

class Module implements ServiceProviderInterface
{

    public function onBootstrap(MvcEvent $e)
    {
        $eventManager        = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
        $eventManager->attach(MvcEvent::EVENT_RENDER,
            array($this, 'initNavigation'), 98);

        $sharedEvents = $eventManager->getSharedManager();
        $sharedEvents->attach('Zend\Mvc\Controller\AbstractController',
            'dispatch',
            function($event) {
                $result        = $event->getResult();
                $application   = $event->getApplication();
                $request       = $application->getRequest();
                $isChild       = $event->getRouteMatch()->getParam('modal');
                $disablelayout = $request->getQuery('disablelayout', null);
                if ($disablelayout && ($result instanceof ViewModel) && !$isChild) {
                    $result->setTerminal(true);
                }

                $serviceLocator = $application->getServiceManager();
                $jobService     = $serviceLocator->get('job_service');
                $jobService->checkJobs();

                //check sign in Quiz
            });

        $t = $e->getTarget();
        $t->getEventManager()->attach(
            $t->getServiceManager()->get('ZfcRbac\View\Strategy\UnauthorizedStrategy')
        );
        $t->getEventManager()->attach(
            $t->getServiceManager()->get('ZfcRbac\View\Strategy\RedirectStrategy')
        );
    }

    public function init(ModuleManager $moduleManager)
    {

        // $sharedEvents->attach(__NAMESPACE__,MvcEvent::EVENT_DISPATCH, array($this, 'checkAuth'), 100);
        //$eventManager->attach(MvcEvent::EVENT_RENDER, array($this, 'initNavigation'),98);
        //$eventManager->attach(MvcEvent::EVENT_ROUTE, array($this, 'checkAuth'),1);
    }

    public function initNavigation($event)
    {
        $service = $event->getApplication()->getServiceManager()->get('application_service');
        $service->initNavigation('Navigation');
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__.'/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__.'/src/'.__NAMESPACE__,
                ),
            ),
        );
    }

    public function getControllerPluginConfig()
    {
        return array(
            'factories' => array(
            ),
        );
    }

    public function getServiceConfig()
    {
        $module = $this;
        return array(
            'invokables' => array(
                'application_service' => 'Application\Service\Application',
                'mail_service' => 'Application\Service\MailService',
                'job_service' => 'Application\Service\JobService'
            ),
            'aliases' => array(
            ),
            'factories' => array(
            ),
        );
    }

    public function getViewHelperConfig()
    {
        return array(
            'factories' => array(
            )
        );
    }

    public function getConfig()
    {
        return include __DIR__.'/config/module.config.php';
    }

    public function getCmsPluginConfig()
    {
        return include 'config/frontend/plugins.config.php';
    }
}
