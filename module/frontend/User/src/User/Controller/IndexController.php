<?php

namespace User\Controller;

use Zend\Mvc\Controller\AbstractActionController,
    Zend\View\Model\ViewModel,
    Doctrine\ORM\EntityManager;

class IndexController extends AbstractActionController
{
    protected $userService;

    public function getUserService()
    {
        if (!$this->userService) {
            $this->userService = $this->getServiceLocator()->get('zfcuser_user_service');
        }
        return $this->userService;
    }

    public function indexAction()
    {

        $userService = $this->getUserService();
        $user        = $userService->getAuthService()->getIdentity();
    }

    public function checkemailAction()
    {
        if (($email = $this->params()->fromPost('email', null)) || ($email = $this->params()->fromPost('newIdentity',
            null))) {
            $result = $this->getUserService()->getUserMapper()->findByEmail($email);
            if ($result) {
                echo 'false';
            } else {
                echo 'true';
            }
        }
        exit;
    }

    public function afterregisterAction()
    {
        
    }

    public function firstloginAction()
    {
        
    }
}