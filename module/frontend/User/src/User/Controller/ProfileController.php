<?php

namespace User\Controller;

use Zend\Mvc\Controller\AbstractActionController,
    Zend\View\Model\ViewModel,
    Doctrine\ORM\EntityManager;
use Zend\Mvc\MvcEvent;
use User\Form\UserDataForm;
use User\Form\AvatarForm;
use User\Form\PaymentdataForm;
use EventUser\Options\ModuleOptions as EventModuleOptions;

class ProfileController extends AbstractActionController
{
    protected $userService;
    protected $user;
    protected $viewModel;
    protected $quizapp_service;
    protected $paymentapp_service;
    protected $statistics_service;
    protected $event_user_service;

    public function setEventManager(\Zend\EventManager\EventManagerInterface $events)
    {
        parent::setEventManager($events);
        $this->getServiceLocator()->get('application_service')->setSubmenu(null,
            'profile');
        $this->viewModel = new ViewModel();
        return $this;
    }

    public function onDispatch(MvcEvent $e)
    {
        $this->viewModel = new ViewModel();
        if (!$this->zfcUserAuthentication()->hasIdentity()) {
            return $this->redirect()->toRoute('home', array());
        } else {
            $userSession           = $this->zfcUserAuthentication()->getIdentity();
            $this->user            = $this->getUserService()->getUserMapper()->findById($userSession->getId());
            $this->viewModel->user = $this->user;
        }
        return parent::onDispatch($e);
    }

    public function getQuizAppService()
    {
        if ($this->quizapp_service === null) {
            $this->quizapp_service = $this->getServiceLocator()->get('quizapp_service');
        }
        return $this->quizapp_service;
    }

    //setSubmenu();
    //$serviceApplication = $this->getServiceLocator()->get('application_service');
    //$serviceApplication->setSubmenu(null, 'game');
    public function getUserService()
    {
        if (!$this->userService) {
            $this->userService = $this->getServiceLocator()->get('zfcuser_user_service');
        }
        return $this->userService;
    }

    public function getStatisticsService()
    {
        if ($this->statistics_service === null) {
            $this->statistics_service = $this->getServiceLocator()->get('statistics_service');
        }
        return $this->statistics_service;
    }

    public function getPaymentAppService()
    {
        if ($this->paymentapp_service === null) {
            $this->paymentapp_service = $this->getServiceLocator()->get('paymentapp_service');
        }
        return $this->paymentapp_service;
    }

    public function getEventUserService()
    {
        if ($this->event_user_service === null) {
            $this->event_user_service = $this->getServiceLocator()->get('event_user_service');
        }
        return $this->event_user_service;
    }

    public function getEventResourcePager($page, $limit, $resourceName,
                                          $resourceConditionName, $user)
    {
        
    }

    public function accountAction()
    {
//        $this->viewModel->nrQuestions = $this->getStatisticsService()->getNrQuestionsByUser($this->user);
//        $this->viewModel->nrGames     = $this->getStatisticsService()->getNrGamesByUser($this->user);
//        $this->viewModel->nrLifebuoys = $this->getStatisticsService()->getNrLifebuoysByUser($this->user);
//        $this->viewModel->nrLosers    = $nrLosers                     = $this->getStatisticsService()->getTotalNrLosersForUser($this->user);
//        $this->viewModel->totalAmount = $totalAmount                  = $this->getStatisticsService()->getTotalBalanceByUser($this->user);
//        $this->viewModel->totalEarned = $totalEarned                  = $this->getStatisticsService()->getTotalEarnedByUser($this->user);
//
//        $this->viewModel->placeInSuccessfulRanking = $this->getStatisticsService()->getPlaceInSuccessfulRanking($nrLosers);
//        $this->viewModel->placeInMostEarnedRanking = $this->getStatisticsService()->getPlaceInMostEarnedRanking($totalEarned);
//        $this->viewModel->user                     = $this->user;

        return $this->viewModel;
    }

    public function datasettingsAction()
    {
        $request = $this->getRequest();
        $form    = new UserDataForm($this->getServiceLocator());
        $form->bind($this->user);
        if ($request->isPost()) {
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $this->getUserService()->getUserMapper()->save($this->user);
                $this->flashMessenger()->addSuccessMessage('msg_user_data_save_ok');
                return $this->redirect()->toRoute('profile/settings', array());
            } else {
                $this->flashMessenger()->addErrorMessage('msg_user_data_save_error');
            }
        }


        $paymentdata    = $this->forward()->dispatch('User\Controller\Profile',
            array('action' => 'paymentdata', 'modal' => true));
        $changePassword = $this->forward()->dispatch('User\Controller\Profile',
            array('action' => 'changepassword', 'modal' => true));
        $changeEmail    = $this->forward()->dispatch('User\Controller\Profile',
            array('action' => 'changeemail', 'modal' => true));

        $this->viewModel->addChild($paymentdata, 'paymentdata');
        $this->viewModel->addChild($changePassword, 'changePassword');
        $this->viewModel->addChild($changeEmail, 'changeEmail');

        $this->viewModel->form = $form;
        return $this->viewModel;
    }

    public function changeemailAction()
    {
        $viewModel = new ViewModel();
        $form      = $this->getServiceLocator()->get('zfcuser_change_email_form');
        $request   = $this->getRequest();
        if ($request->isPost()) {
            $form->setData($request->getPost());
            if ($form->isValid()) {
                if ($this->getUserService()->changeEmail($form->getData())) {
                    $this->flashMessenger()->addSuccessMessage('msg_user_emailchange_save_ok');
                } else {
                    $this->flashMessenger()->addErrorMessage('msg_user_emailchange_save_error');
                }
            } else {
                $this->flashMessenger()->addErrorMessage('msg_user_emailchange_save_error');
            }
            return $this->redirect()->toRoute('profile/settings', array());
        }
        $viewModel->form = $form;
        return $viewModel;
    }

    public function deleteavatarAction()
    {
        $this->user->setAvatarPath(null);
        $this->zfcUserAuthentication()->getIdentity()->setAvatarPath(null);
        $this->getUserService()->getUserMapper()->save($this->user);
        return $this->redirect()->toRoute('profile/settings/changeavatar',
                array());
    }

    public function changeavatarAction()
    {

        $request   = $this->getRequest();
        $uploadDir = 'profile-images/'.date('Y/m/');

        $form = new AvatarForm($this->getServiceLocator(), $uploadDir);
        $form->bind($this->user);
        if ($request->isPost()) {
            $form->setData($this->getRequest()->getFiles()->toArray());
            if ($form->isValid()) {
                $fileData    = $this->user->getAvatarPath();
                $newFileName = array_pop(explode('/', $fileData['tmp_name']));
                $this->user->setAvatarPath($uploadDir.$newFileName);
                $this->getUserService()->getUserMapper()->save($this->user);
                $this->zfcUserAuthentication()->getIdentity()->setAvatarPath($uploadDir.$newFileName);
                $this->flashMessenger()->addSuccessMessage('msg_user_data_save_ok');
                return $this->redirect()->toRoute('profile/settings/changeavatar',
                        array());
            } else {
                $this->flashMessenger()->addErrorMessage('msg_user_data_save_error');
            }
        }
        $this->viewModel->form = $form;
        return $this->viewModel;
    }

    public function changepasswordAction()
    {
        $viewModel = new ViewModel();
        $form      = $this->getServiceLocator()->get('zfcuser_change_password_form');
        $request   = $this->getRequest();
        if ($request->isPost()) {
            $form->setData($request->getPost());
            if ($form->isValid()) {
                if ($this->getUserService()->changePassword($form->getData())) {
                    $this->flashMessenger()->addSuccessMessage('msg_user_passwordchange_save_ok');
                } else {
                    $this->flashMessenger()->addErrorMessage('msg_user_passwordchange_save_error');
                }
            } else {
                $this->flashMessenger()->addErrorMessage('msg_user_passwordchange_save_error');
            }
            return $this->redirect()->toRoute('profile/settings', array());
        }
        $viewModel->form = $form;
        return $viewModel;
    }

    public function lifebuoysAction()
    {
        $startLimit = 10;
        $page       = $this->getEvent()->getRouteMatch()->getParam('page', 1);
        $limit      = $this->getEvent()->getRouteMatch()->getParam('limit',
            $startLimit);

        $wonlifebuoys  = $this->forward()->dispatch('User\Controller\Profile',
            array('action' => 'wonlifebuoys', 'modal' => true));
        $lostlifebuoys = $this->forward()->dispatch('User\Controller\Profile',
            array('action' => 'lostlifebuoys', 'modal' => true));
        $this->viewModel->addChild($wonlifebuoys, 'wonlifebuoys');
        $this->viewModel->addChild($lostlifebuoys, 'lostlifebuoys');

        $nrLifebuoys                  = $this->getStatisticsService()->getNrLifebuoysByUser($this->user);
        $this->viewModel->nrLifebuoys = (!empty($nrLifebuoys)) ? $nrLifebuoys : 0;
        return $this->viewModel;
    }

    public function wonlifebuoysAction()
    {
        $viewModel  = new ViewModel;
        $startLimit = 5;
        $page       = $this->getEvent()->getRouteMatch()->getParam('page', 1);
        $limit      = $this->getEvent()->getRouteMatch()->getParam('limit',
            $startLimit);

        $resourceName            = EventModuleOptions::RESOURCE_LIFEBUOY;
        $mapperEventResourceUser = $this->getEventUserService()->getEventResourceUserMapper();
        $paginator               = $mapperEventResourceUser->getEventResourcePager($page,
            $limit, $this->user, $resourceName);

        $viewModel->paginator = $paginator;
        $viewModel->page      = $page;
        return $viewModel;
    }

    public function lostlifebuoysAction()
    {
        $viewModel            = new ViewModel;
        $startLimit           = 5;
        $page                 = $this->getEvent()->getRouteMatch()->getParam('page',
            1);
        $limit                = $this->getEvent()->getRouteMatch()->getParam('limit',
            $startLimit);
        $lifebuoyMapper       = $this->getQuizAppService()->getLifebuoyMapper();
        $paginator            = $lifebuoyMapper->getUsedLifebuoysByUserPager($this->user,
            $page, $limit);
        $viewModel->paginator = $paginator;
        $viewModel->page      = $page;
        return $viewModel;
    }

    public function gamehistoryAction()
    {
        $limit = 20;

        $listWonGame = $this->forward()->dispatch('User\Controller\Profile',
            array('action' => 'wongame', 'modal' => true));
        $listAllGame = $this->forward()->dispatch('User\Controller\Profile',
            array('action' => 'allgame', 'modal' => true));
        $this->viewModel->addChild($listAllGame, 'listAllGame');
        $this->viewModel->addChild($listWonGame, 'listWonGame');

        return $this->viewModel;
    }

    public function wongameAction()
    {
        $viewModel            = new ViewModel;
        $startLimit           = 5;
        $params               = array();
        $page                 = $this->getEvent()->getRouteMatch()->getParam('page',
            1);
        $limit                = $this->getEvent()->getRouteMatch()->getParam('limit',
            $startLimit);
        $paginator            = $this->getQuizAppService()->getHistoryMapper()->historyWonGamesByUser($this->user,
            $page, $limit);
        $viewModel->paginator = $paginator;
        $viewModel->params    = $params;
        $viewModel->page      = $page;
        return $viewModel;
    }

    public function allgameAction()
    {
        $viewModel  = new ViewModel;
        $startLimit = 5;
        $params     = array();
        $page       = $this->getEvent()->getRouteMatch()->getParam('page', 1);
        $limit      = $this->getEvent()->getRouteMatch()->getParam('limit',
            $startLimit);

        //sort
        $paginator            = $this->getQuizAppService()->getHistoryMapper()->historyAllGamesByUser($this->user,
            $page, $limit);
        $viewModel->paginator = $paginator;
        $viewModel->params    = $params;
        $viewModel->page      = $page;
        return $viewModel;
    }

    public function paymentdataAction()
    {
        $viewModel         = new ViewModel();
        $request           = $this->getRequest();
        $form              = new PaymentdataForm($this->getServiceLocator());
        $paymentdataEntity = $this->user->getPaymentdata();
        $form->bind($paymentdataEntity);
        if ($request->isPost()) {
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $this->getPaymentAppService()->getPaymentdataMapper()->save($paymentdataEntity);
                $this->flashMessenger()->addSuccessMessage('msg_user_data_save_ok');
            } else {
                $this->flashMessenger()->addErrorMessage('msg_user_data_save_error');
            }
            return $this->redirect()->toRoute('profile/settings', array());
        }
        $viewModel->form = $form;

        return $viewModel;
    }
}