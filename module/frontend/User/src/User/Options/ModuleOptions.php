<?php

namespace User\Options;

use Zend\Stdlib\AbstractOptions;

class ModuleOptions extends AbstractOptions {

    protected $default_role = 'member';

    public function getDefaultRole() {
        return $this->default_role;
    }

    public function setDefaultRole($defaultRole) {
        $this->default_role = $defaultRole;
        return $this;
    }

}
