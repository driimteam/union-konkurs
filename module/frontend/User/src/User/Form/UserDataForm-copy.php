<?php

namespace User\Form;

use Zend\Form\Form;
use Application\Form\BasicForm;
use User\Form\Filter\UserDataFilter;

class UserDataForm extends BasicForm {

    protected $filter;
    protected $options=array();
    
    public function __construct($options = array()) {
        // we want to ignore the name passed
        parent::__construct('userData');
        
        $this->options = $options;
        //$this->_serviceCms ;
        //$this->_translator = $translator;
        
        $this->formElements[] = $this->getTextElement('email', 'E-mail')
                ->setAttribute('class', 'required email')->setOptions(array(
                    'label' => 'form_label_email',
                    'partial' => "formpartial/text"
                ));
       
        foreach($this->formElements as $element){
            $this->addElement($element);
        }
        $inputFilter = $this->geFilter();
        $this->setInputFilter($inputFilter);
    }
    
    public function geFilter() {
        if($this->filter==null){
            $this->filter = new UserDataFilter($this->options);
        }
        return $this->filter;
        
    }
}
