<?php

namespace User\Form;

use Zend\Form\Element;
use Zend\Form\Form;
use Application\Form\BasicForm as BasicForm;
use User\Form\Fieldset\PaymentdataFieldset;
use Zend\InputFilter\InputFilter;

class PaymentdataForm extends BasicForm {

    public function __construct($sm, $options = array()) {
        parent::__construct('paymentdata', $sm, $options);
        $elementsObject = new PaymentdataFieldset($sm);
        $elementsObject->setUseAsBaseFieldset(true); //, array('options' => array('use_as_base_fieldset' => true)));
        $this->add($elementsObject);
    }

}
