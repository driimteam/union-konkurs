<?php

namespace User\Form;

use Zend\Form\Element;
use Zend\Form\Form;
use Application\Form\BasicForm as BasicForm;
use User\Form\Fieldset\UserFieldset;
use Zend\InputFilter\InputFilter;

class UserDataForm extends BasicForm {

    public function __construct($sm, $options = array()) {
        parent::__construct('user', $sm, $options);
        $elementsObject = new UserFieldset($sm);
        $elementsObject->setUseAsBaseFieldset(true); //, array('options' => array('use_as_base_fieldset' => true)));
        $this->add($elementsObject);
    }

}
