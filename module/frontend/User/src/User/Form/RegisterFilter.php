<?php

namespace User\Form;

use ZfcUser\Form\RegisterFilter as ZfcUserRegisterFilter;
use ZfcUser\Options\RegistrationOptionsInterface;

class RegisterFilter extends ZfcUserRegisterFilter {

    public function __construct($emailValidator, $usernameValidator, RegistrationOptionsInterface $options) {
        parent::__construct($emailValidator, $usernameValidator, $options);

        $this->add(array(
            'name' => 'ruleAdult',
            'required' => true,
            'validators' => array(
                array(
                    'name' => 'GreaterThan',
                    'options' => array(
                        'min' => 1,
                        'inclusive'=>true
                    ),
                ),
            ),
        ));
        $this->add(array(
            'name' => 'rulePersonalData',
            'required' => true,
            'validators' => array(
                array(
                    'name' => 'GreaterThan',
                    'options' => array(
                        'min' => 1,
                        'inclusive'=>true
                    ),
                ),
            ),
        ));
        //$this->getEventManager()->trigger('init', $this);
    }

}
