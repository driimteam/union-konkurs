<?php

namespace User\Form\Fieldset;

use Zend\Form\Element;
use Zend\Form\Form;
use Application\Form\Fieldset\Basic as BasicFieldset;
use UserApp\Models\Entity\User;
use Zend\InputFilter\InputFilterProviderInterface;

//use Zend\Stdlib\Hydrator\ClassMethods as ClassMethodsHydrator;

class AvatarFieldset extends BasicFieldset implements InputFilterProviderInterface {

    public $uploadDir;
    public function __construct($sm, $uploadDir, $options = array()) {
        parent::__construct('user', new User(), $sm, $options);
        $this->uploadDir = $uploadDir;
        $file = new \Zend\Form\Element\File('avatarPath');
        $file->setAttribute('class', 'required');
        $this->add($file);
        
    }

    /**
     * Define InputFilterSpecifications
     *
     * @access public
     * @return array
     */
    public function getInputFilterSpecification() {
        $dirDestination =APPLICATION_PATH.'/data/upload/'.$this->uploadDir;
        if(!file_exists ($dirDestination)){
            mkdir($dirDestination, 0777,1);
        }
        return array(
            'avatarPath' => array(
                'required' => true,
                'filters' => array(
                            array(
                                    'name' => 'File\RenameUpload',
                                    'options' => array(
                                            'target' => $dirDestination,
                                            'randomize' => true,
                                            'overwrite' => true,
                                            'use_upload_extension'=>true
                                    ),
                            ),
                    ),
                'validators' => array(
                    array('name'=>"File\Size",'options'=>array('max'=>10000000))
                ),
                'properties' => array(
                    'required' => true
                )
            ),
        );
    }

}
