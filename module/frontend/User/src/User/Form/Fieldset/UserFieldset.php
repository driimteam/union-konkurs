<?php

namespace User\Form\Fieldset;

use Zend\Form\Element;
use Zend\Form\Form;
use Application\Form\Fieldset\Basic as BasicFieldset;
use UserApp\Models\Entity\User;
use Zend\InputFilter\InputFilterProviderInterface;

//use Zend\Stdlib\Hydrator\ClassMethods as ClassMethodsHydrator;

class UserFieldset extends BasicFieldset implements InputFilterProviderInterface {

    public function __construct($sm, $options = array()) {
        parent::__construct('user', new User(), $sm, $options);
        $this->add($this->getTextElement('displayName', 'Twoje imię w grze')->setAttribute('class', 'required'));
        
    }

    /**
     * Define InputFilterSpecifications
     *
     * @access public
     * @return array
     */
    public function getInputFilterSpecification() {
        return array(
            'displayName' => array(
                'required' => true,
                'filters' => array(
                    array('name' => 'StringTrim'),
                    array('name' => 'StripTags'),
                    array('name' => 'App\\Filter\\HtmlSpecialchars')
                ),
                'properties' => array(
                    'required' => true
                )
            ),
        );
    }

}
