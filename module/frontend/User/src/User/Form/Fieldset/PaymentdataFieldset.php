<?php

namespace User\Form\Fieldset;

use Zend\Form\Element;
use Zend\Form\Form;
use Application\Form\Fieldset\Basic as BasicFieldset;
use PaymentApp\Models\Entity\AppPaymentdata;
use Zend\InputFilter\InputFilterProviderInterface;

//use Zend\Stdlib\Hydrator\ClassMethods as ClassMethodsHydrator;

class PaymentdataFieldset extends BasicFieldset implements InputFilterProviderInterface {

    public function __construct($sm, $options = array()) {
        parent::__construct('paymentdata', new AppPaymentdata(), $sm, $options);
        $this->add($this->getTextElement('name', 'Imię')->setAttribute('class', 'required'));
        $this->add($this->getTextElement('surname', 'Nazwisko')->setAttribute('class', 'required'));
        $this->add($this->getTextElement('nrAccount', 'Nr konta')->setAttribute('class', 'number required'));
        $this->add($this->getTextElement('address', 'Adres')->setAttribute('class', 'required'));
        $this->add($this->getTextElement('postalCode', 'Kod pocztowy')->setAttribute('class', 'postalcode required'));
        $this->add($this->getTextElement('city', 'Miasto')->setAttribute('class', 'required'));
        
    }

    /**
     * Define InputFilterSpecifications
     *
     * @access public
     * @return array
     */
    public function getInputFilterSpecification() {
        return array(
            'name' => array(
                'required' => true,
                'filters' => array(
                    array('name' => 'StringTrim'),
                    array('name' => 'StripTags'),
                    array('name' => 'App\\Filter\\HtmlSpecialchars')
                ),
                'properties' => array(
                    'required' => true
                )
            ),
            'surname' => array(
                'required' => true,
                'filters' => array(
                    array('name' => 'StringTrim'),
                    array('name' => 'StripTags'),
                    array('name' => 'App\\Filter\\HtmlSpecialchars')
                ),
                'properties' => array(
                    'required' => true
                )
            ),
            'nrAccount' => array(
                'required' => true,
                'filters' => array(
                    array('name' => 'StringTrim'),
                    array('name' => 'StripTags'),
                    array('name' => 'App\\Filter\\HtmlSpecialchars')
                ),
                'properties' => array(
                    'required' => true
                )
            ),
            'address' => array(
                'required' => true,
                'filters' => array(
                    array('name' => 'StringTrim'),
                    array('name' => 'StripTags'),
                    array('name' => 'App\\Filter\\HtmlSpecialchars')
                ),
                'properties' => array(
                    'required' => true
                )
            ),
            'postalCode' => array(
                'required' => true,
                'filters' => array(
                    array('name' => 'StringTrim'),
                    array('name' => 'StripTags'),
                    array('name' => 'App\\Filter\\HtmlSpecialchars')
                ),
                'properties' => array(
                    'required' => true
                )
            ),
            'city' => array(
                'required' => true,
                'filters' => array(
                    array('name' => 'StringTrim'),
                    array('name' => 'StripTags'),
                    array('name' => 'App\\Filter\\HtmlSpecialchars')
                ),
                'properties' => array(
                    'required' => true
                )
            ),
        );
    }

}
