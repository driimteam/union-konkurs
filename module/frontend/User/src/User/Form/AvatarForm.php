<?php

namespace User\Form;

use Zend\Form\Element;
use Zend\Form\Form;
use Application\Form\BasicForm as BasicForm;
use User\Form\Fieldset\AvatarFieldset;
use Zend\InputFilter\InputFilter;

class AvatarForm extends BasicForm {

    public function __construct($sm, $uploadDir, $options = array()) {
        parent::__construct('user', $sm, $options);
        $elementsObject = new AvatarFieldset($sm,$uploadDir);
        $elementsObject->setUseAsBaseFieldset(true); //, array('options' => array('use_as_base_fieldset' => true)));
        $this->add($elementsObject);
        $this->setAttribute('enctype', 'multipart/form-data');
    }

}
