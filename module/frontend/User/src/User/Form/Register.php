<?php

namespace User\Form;

use ZfcUser\Form\Register as ZfcUserRegister;
use ZfcUser\Options\RegistrationOptionsInterface;
use Zend\Form\Form;
use Zend\Form\Element;
use ZfcBase\Form\ProvidesEventsForm;

class Register extends ZfcUserRegister {

    /**
     * @param string|null $name
     * @param RegistrationOptionsInterface $options
     */
    public function __construct($name, RegistrationOptionsInterface $options) {
        parent::__construct($name, $options);
        $this->get('display_name')->setLabel('form_label_displayname')->setAttribute('class','required');
        $this->get('email')->setLabel('form_label_email')->setAttribute('class','required email');
        $this->get('password')->setLabel('form_label_password')->setAttribute('class','required');
        $this->get('passwordVerify')->setLabel('form_label_repeat_password')->setAttribute('class','required');
        
        $this->add(array(
            'name' => 'ruleAdult',
            'options' => array(
                'label' => 'form_label_rule_adult',
                'checked_value' => 1,
                'unchecked_value' => 0,
                'use_hidden_element' => true,
                'partial' => 'formpartial/checkbox'
            ),
            'attributes' => array(
                'type' => 'checkbox',
                'class'=>"required"
            ),
        ));
        $this->add(array(
            'name' => 'rulePersonalData',
            'options' => array(
                'label' => 'form_label_rule_personaldata',
                'checked_value' => 1,
                'unchecked_value' => 0,
                'use_hidden_element' => true,
                'partial' => 'formpartial/checkbox'
            ),
            'attributes' => array(
                'type' => 'checkbox',
                'class'=>"required"
            ),
        ));
    }

    public function getSortedElements() {
        return [
            'display_name'=>'formpartial/text',
            'email'=>'formpartial/email',
            'password'=>'formpartial/password',
            'passwordVerify'=>'formpartial/password',
            'ruleAdult'=>'formpartial/checkbox',
            'rulePersonalData'=>'formpartial/checkbox'
        ];
    }

}
