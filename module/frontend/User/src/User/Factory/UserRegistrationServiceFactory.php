<?php
namespace User\Factory;

use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\FactoryInterface;
use User\Service\UserRegistrationService;

class UserRegistrationServiceFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        return new UserRegistrationService(
            $serviceLocator->get('user_module_options'),
            $serviceLocator->get('zfcuser_user_mapper'),
            $serviceLocator->get('zfcuser_module_options')
        );
    }
}
