<?php
namespace User\Models\Entity;
use UserApp\Models\Interfaces\PlayerInterface;

/**
 * UserNotLogged
 */
class UserNotLogged implements PlayerInterface {

    const TYPE_USER = "NOTLOGGED";

    protected $id;

    public function __construct($id) {
        $this->id = $id;
    }

    public function getId() {
        return $this->id;
    }

    public function getType() {
        return self::TYPE_USER;
    }
    public function getDisplayName(){
        return 'user_not_logged';
    }

}

