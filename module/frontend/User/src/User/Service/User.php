<?php

namespace User\Service;

use Zend\Form\Form;
use Zend\ServiceManager\ServiceManagerAwareInterface;
use Zend\ServiceManager\ServiceManager;
use Zend\Crypt\Password\Bcrypt;
use Zend\Stdlib\Hydrator;
use Zend\Stdlib\DispatchableInterface;
use UserApp\Models\Entity\User as UserEntity;
use PaymentApp\Models\Entity\AppPaymentdata;
use PaymentApp\Models\Entity\AppWallet;

class User implements ServiceManagerAwareInterface
{
    /**
     * @var pluginMapper
     */
    protected $userMapper;
    protected $userapp_service;

    /**
     * @var ServiceManager
     */
    protected $serviceManager;
    protected $eventManager;
    protected $authService;

    /**
     * @var UserServiceOptionsInterface
     */

    /**
     * @var Hydrator\ClassMethods
     */
    protected $formHydrator;

    /**
     * Retrieve service manager instance
     *
     * @return ServiceManager
     */
    public function getServiceManager()
    {
        return $this->serviceManager;
    }

    /**
     * Set service manager instance
     *
     * @param ServiceManager $serviceManager
     * @return User
     */
    public function setServiceManager(ServiceManager $serviceManager)
    {
        $this->serviceManager = $serviceManager;
        return $this;
    }

    public function getUserAppService()
    {
        if ($this->userapp_service === null) {
            $this->userapp_service = $this->getServiceManager()->get('userapp_service');
        }
        return $this->userapp_service;
    }

    public function getRoleMapper()
    {
        return $this->getUserAppService()->getRoleMapper();
    }

    public function getPermissionMapper()
    {
        return $this->getUserAppService()->getPermissionMapper();
    }

    public function getUserMapper()
    {
        if (null === $this->userMapper) {
            $this->userMapper = $this->getServiceManager()->get('zfcuser_user_mapper');
        }
        return $this->userMapper;
    }

    public function setUserMapper($user_mapper)
    {
        $this->userMapper = $user_mapper;
        return $this->userMapper;
    }

    public function getAuthService()
    {
        if (null === $this->authService) {
            $this->authService = $this->getServiceManager()->get('zfcuser_auth_service');
        }
        return $this->authService;
    }

    public function setAuthService($authService)
    {
        $this->authService = $authService;
        return $this;
    }

    public function createSmsUser()
    {
        $user        = new UserEntity();
        $user->setDisplayName('SMS');
        $user->setRole(UserEntity::USER_ROLE_SMS);
        $wallet      = new AppWallet();
        $paymentdata = new AppPaymentdata();
        $user->setWallet($wallet);
        $user->setPaymentdata($paymentdata);
        $this->getUserMapper()->save($user);
        return $user;
    }
}
