<?php
namespace User\Service;

use Zend\EventManager\EventInterface;

interface UserRegistrationServiceInterface
{
    /**
     * Listener for registration, when a new user is registered
     *
     * @param  EventInterface $e
     * @return void
     */
    public function onConfirmRegistration(EventInterface $e);

}
