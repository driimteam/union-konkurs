<?php

namespace User\Service;

use Zend\ServiceManager\ServiceManagerAwareInterface;
use Zend\ServiceManager\ServiceManager;
use Zend\EventManager\EventInterface;
use ZfcUser\Entity\UserInterface;
use ZfcBase\EventManager\EventProvider;
use DateTime;
use Zend\Crypt\Password\Bcrypt;
use User\Options\ModuleOptions;
use ZfcUser\Options\ModuleOptions as ZfcUserOptions;
use ZfcUser\Mapper\UserInterface as UserMapperInterface;

class UserRegistrationService extends EventProvider implements ServiceManagerAwareInterface,
    UserRegistrationServiceInterface
{
    /**
     * @var ModuleOptions
     */
    protected $options;
    protected $zfcUserOptions;
    protected $userMapper;
    protected $serviceManager;
    protected $recommendapp_service;
    protected $recommend_service;
    protected $auth_service;
    protected $user_service;
    protected $tshirt_session_service;


    /**
     * Constructor
     *
     * @param ModuleOptions $options
     * @param ZfcUserOptions $zfcUserOptions
     * @paramUserMapperInterface $userMapper
     */
    public function __construct(
    ModuleOptions $options, UserMapperInterface $userMapper,
    ZfcUserOptions $zfcUserOptions
    )
    {
        $this->options        = $options;
        $this->zfcUserOptions = $zfcUserOptions;
        $this->userMapper     = $userMapper;
    }

    public function getTshirtSessionService()
    {
        if (!$this->tshirt_session_service) {
            $this->tshirt_session_service = $this->getServiceManager()->get('tshirt_session_service');
        }
        return $this->tshirt_session_service;
    }

    public function getRecommendAppService()
    {
        if ($this->recommendapp_service === null) {
            $this->recommendapp_service = $this->getServiceManager()->get('recommendapp_service');
        }
        return $this->recommendapp_service;
    }

    public function getRecommendService()
    {
        if ($this->recommend_service === null) {
            $this->recommend_service = $this->getServiceManager()->get('recommend_service');
        }
        return $this->recommend_service;
    }

    public function getAuthService()
    {
        if ($this->auth_service === null) {
            $this->auth_service = $this->getServiceManager()->get('zfcuser_auth_service');
        }
        return $this->auth_service;
    }

    public function getUserService()
    {
        if ($this->user_service === null) {
            $this->user_service = $this->getServiceManager()->get('user_service');
        }
        return $this->user_service;
    }


    public function getServiceManager()
    {
        return $this->serviceManager;
    }

    /**
     * Set service manager instance
     *
     * @param ServiceManager $serviceManager
     * @return User
     */
    public function setServiceManager(ServiceManager $serviceManager)
    {
        $this->serviceManager = $serviceManager;
        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function onConfirmRegistration(EventInterface $e)
    {
        $user = $e->getParam('user');
        $user->setState(1);
        $this->userMapper->save($user);

        if ($this->getRecommendService()->hasSessionToRecommend()) {
            $this->getRecommendService()->createRecommendUser($user->getId());
        }

        $controllerPluginManager = $this->getServiceManager()->get('ControllerPluginManager');
        $flashMessenger          = $controllerPluginManager->get('flashMessenger');
        $flashMessenger->addSuccessMessage('msg_confirm_registration');
    }

    public function onLoginFail(EventInterface $e)
    {
        $controllerPluginManager = $this->getServiceManager()->get('ControllerPluginManager');
        $flashMessenger          = $controllerPluginManager->get('flashMessenger');
        $flashMessenger->addErrorMessage('msg_login_error');
    }

    public function onLoginSuccess(EventInterface $e)
    {
        
    }

    public function onUserRegistration(EventInterface $e)
    {
        $user = $e->getParam('user');
        $user->setState(0);
        $this->createUser($user);

        $controllerPluginManager = $this->getServiceManager()->get('ControllerPluginManager');
        $flashMessenger          = $controllerPluginManager->get('flashMessenger');
        $flashMessenger->addSuccessMessage('msg_register_success');
    }

    public function onRegisterViaProvider(EventInterface $e)
    {
        $user = $e->getParam('user');
        $provider = $e->getParam('provider');
        $userProfile = $e->getParam('userProfile');
        $this->setSessionSocialData($provider,$userProfile);
       
        $user->setState(1);
        $this->createUser($user);
    }

    public function onLoginViaProvider(EventInterface $e)
    {
        $user = $e->getParam('user');
        $provider = $e->getParam('provider');
        $userProfile = $e->getParam('userProfile');
        $this->setSessionSocialData($provider,$userProfile);
        
    }

    protected function setSessionSocialData($provider,$userProfile){
        $provider = strtolower($provider);
        $this->getTshirtSessionService()->addTshirtSession($provider,$userProfile);
        $this->getTshirtSessionService()->addTshirtSession('provider',$provider);
    }


    public function createUser($user)
    {
        $user->setRole($this->options->getDefaultRole());
    }
}
