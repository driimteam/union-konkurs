<?php

namespace UserTest\Controller;

use UserTest\Bootstrap;
use Zend\Mvc\Router\Http\TreeRouteStack as HttpRouter;
use User\Controller\ProfileController;
use Zend\Http\Request;
use Zend\Http\Response;
use Zend\Mvc\MvcEvent;
use Zend\Mvc\Router\RouteMatch;
use PHPUnit_Framework_TestCase;
use UserApp\Models\Entity\User as UserIdentity;
use PaymentApp\Models\Entity\AppWallet;
use Quiz\Service\Quiz as QuizService;
use ZfcRbac\Service\RoleService;
use ZfcRbac\Options\ModuleOptions as RbacModuleOptions;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;

class UserControllerTest extends AbstractHttpControllerTestCase
{

    protected $controller;
    protected $request;
    protected $response;
    protected $routeMatch;
    protected $event;
    protected $pluginManager;
    protected $options;
    protected $application;
    protected $authorizationServiceRbac;

    protected function setUp()
    {
        $this->setApplicationConfig(
                include 'TestConfig.php'
        );

        parent::setUp();

        $this->authService = $this->getMock('Zend\Authentication\AuthenticationService');
        $this->sessionManager = $this->getMock('Zend\Session\SessionManager');
        $this->sessionManager->expects($this->any())
                ->method('start')
                ->will($this->returnValue(true));
        
        $this->serviceManager = $this->getApplicationServiceLocator();
        $this->serviceManager->setAllowOverride(true);
        $this->serviceManager->setService('Zend\Authentication\AuthenticationService', $this->authService);
        $this->serviceManager->setService('Zend\Session\SessionManager', $this->sessionManager);
        
        $this->pluginAuth = $this->getMock('ZfcUser\Controller\Plugin\ZfcUserAuthentication');
        $this->serviceManager->get('ControllerPluginManager')->setService('zfcUserAuthentication', $this->pluginAuth);
        
    }

    protected function mockLogin()
    {

        $userIdentity = new UserIdentity();
        $userIdentity->setId(22);
        //$userIdentity->setUserId(22);
        $userIdentity->setEmail("test@driim.com");
        $userIdentity->setRole("member");
        $wallet = new AppWallet();
        $userIdentity->setWallet($wallet);

        $this->authService->expects($this->any())
                ->method('getIdentity')
                ->will($this->returnValue($userIdentity));

        $this->authService->expects($this->any())
                ->method('hasIdentity')
                ->will($this->returnValue(true));


        $this->pluginAuth->expects($this->any())
                ->method('getIdentity')
                ->will($this->returnValue($userIdentity));

        $this->pluginAuth->expects($this->any())
                ->method('hasIdentity')
                ->will($this->returnValue(true));


        $identityProvider = $this->getMock('ZfcRbac\Identity\IdentityProviderInterface');
        $identityProvider->expects($this->any())
                ->method('getIdentity')
                ->will($this->returnValue($userIdentity));
    }

    public function testRegisterAction()
    {
        //$this->mockLogin();    
        $this->dispatch('/user/register');
        $this->assertResponseStatusCode(200);
    }
    
    public function testLoginAction()
    {
        //$this->mockLogin();    
        $this->dispatch('/user/login');
        $this->assertResponseStatusCode(200);
    }
}