<?php

namespace UserTest\Controller;

use UserTest\Bootstrap;
use Zend\Mvc\Router\Http\TreeRouteStack as HttpRouter;
use User\Controller\ProfileController;
use Zend\Http\Request;
use Zend\Http\Response;
use Zend\Mvc\MvcEvent;
use Zend\Mvc\Router\RouteMatch;
use PHPUnit_Framework_TestCase;
use UserApp\Models\Entity\User as UserIdentity;
use PaymentApp\Models\Entity\AppWallet;
use Quiz\Service\Quiz as QuizService;
use ZfcRbac\Service\RoleService;
use ZfcRbac\Options\ModuleOptions as RbacModuleOptions;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;

class ProfileControllerTest extends AbstractHttpControllerTestCase
{

    protected $controller;
    protected $request;
    protected $response;
    protected $routeMatch;
    protected $event;
    protected $pluginManager;
    protected $options;
    protected $application;
    protected $authorizationServiceRbac;

    protected function setUp()
    {
        $this->setApplicationConfig(
                include 'TestConfig.php'
        );
        parent::setUp();
//        
//        $this->serviceManager = Bootstrap::getServiceManager();
//        $this->application = Bootstrap::getApplication();
//        $this->controller = new ProfileController();
//
//        $this->request = new Request();
//        $this->routeMatch = new RouteMatch(array('controller' => 'Profile'));
//        $this->event = new MvcEvent();
//        $config = $this->serviceManager->get('Config');
//        $routerConfig = isset($config['router']) ? $config['router'] : array();
//        $router = HttpRouter::factory($routerConfig);
//
//        $this->event->setRouter($router);
//        $this->event->setRouteMatch($this->routeMatch);
//        $this->controller->setEvent($this->event);
//        $this->controller->setServiceLocator($this->serviceManager);

        $this->authService = $this->getMock('Zend\Authentication\AuthenticationService');
        //$this->controller->getUserService()->setAuthService($this->authService);

        $this->serviceManager = $this->getApplicationServiceLocator();
        $this->serviceManager->setAllowOverride(true);
        $this->serviceManager->setService('Zend\Authentication\AuthenticationService', $this->authService);
        
        $this->pluginAuth = $this->getMock('ZfcUser\Controller\Plugin\ZfcUserAuthentication');
        
        $this->serviceManager->get('ControllerPluginManager')->setService('zfcUserAuthentication', $this->pluginAuth);
        
        //

        //$this->controller->getPluginManager()->setService('zfcUserAuthentication', $this->pluginAuth);


        //$trait                = $this->getObjectForTrait('ZfcRbac\Service\Authoriza//onServiceAwareTrait');
        //$this->authorizationServiceRbac = $this->getMock('ZfcRbac\Service\AuthorizationService', [], [], '', false);
        //$trait->setAuthorizationService($this->authorizationServiceRbac);
    }

    protected function mockLogin()
    {

        $userIdentity = new UserIdentity();
        $userIdentity->setId(22);
        //$userIdentity->setUserId(22);
        $userIdentity->setEmail("test@driim.com");
        $userIdentity->setRole("member");
        $wallet = new AppWallet();
        $userIdentity->setWallet($wallet);

        $this->authService->expects($this->any())
                ->method('getIdentity')
                ->will($this->returnValue($userIdentity));

        $this->authService->expects($this->any())
                ->method('hasIdentity')
                ->will($this->returnValue(true));


        $this->pluginAuth->expects($this->any())
                ->method('getIdentity')
                ->will($this->returnValue($userIdentity));

        $this->pluginAuth->expects($this->any())
                ->method('hasIdentity')
                ->will($this->returnValue(true));


        $identityProvider = $this->getMock('ZfcRbac\Identity\IdentityProviderInterface');
        $identityProvider->expects($this->any())
                ->method('getIdentity')
                ->will($this->returnValue($userIdentity));



//        $moduleOptions = new RbacModuleOptions($this->serviceManager->get('Config')['zfc_rbac']);
//        $roleProviderConfig = $moduleOptions->getRoleProvider();
//
//        //$authorizationService->isGranted($permission, $context))
//
//        if (empty($roleProviderConfig)) {
//            throw new \Exception('No role provider has been set for ZfcRbac');
//        }
//
//        /* @var \ZfcRbac\Role\RoleProviderPluginManager $pluginManager */
//        $pluginManager = $this->serviceManager->get('ZfcRbac\Role\RoleProviderPluginManager');
//        $roleProvider = $pluginManager->get(key($roleProviderConfig), current($roleProviderConfig));
//        $traversalStrategy = $this->serviceManager->get('Rbac\Rbac')->getTraversalStrategy();
//
//        $roleService = new RoleService($identityProvider, $roleProvider, $traversalStrategy);
//        $roleService->setGuestRole($moduleOptions->getGuestRole());
//        $this->serviceManager->setAllowOverride(true);
//        $this->serviceManager->setService('ZfcRbac\Service\RoleService', $roleService);
    }

    public function testAccountAction()
    {
        $this->mockLogin();    
        $this->dispatch('/profil');
        $this->assertResponseStatusCode(200);
        
        //$this->routeMatch->setParam('action', 'account');
        //$result = $this->controller->dispatch($this->request);
        //$response = $this->controller->getResponse();
        //$this->assertEquals(200, $response->getStatusCode());
    }
    
    public function testDatasettings()
    {
        $this->mockLogin();    
        $this->dispatch('/profil/dane-i-ustawienia');
        $this->assertResponseStatusCode(200);
    }
    
    public function testLifebuoys()
    {
        $this->mockLogin();    
        $this->dispatch('/profil/kola-ratunkowe');
        $this->assertResponseStatusCode(200);
    }
    
    public function testLifebuoysAction()
    {
        $this->mockLogin();    
        $this->dispatch('/profil/kola-ratunkowe');
        $this->assertResponseStatusCode(200);
    }
    
    public function testGamehistoryAction()
    {
        $this->mockLogin();    
        $this->dispatch('/profil/historia-gier');
        $this->assertResponseStatusCode(200);
    }
    
    public function testWongameAction()
    {
        $this->mockLogin();    
        $this->dispatch('/profil/historia-wygranych-gier');
        $this->assertResponseStatusCode(200);
    }
    
    public function testAllgameAction()
    {
        $this->mockLogin();    
        $this->dispatch('/profil/historia-wszystkich-gier');
        $this->assertResponseStatusCode(200);
    }
}