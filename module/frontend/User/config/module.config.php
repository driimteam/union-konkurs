<?php
namespace User;
return array(
    'controllers' => array(
        'invokables' => array(
            'User\Controller\Index' => 'User\Controller\IndexController',
            'User\Controller\Profile' => 'User\Controller\ProfileController',
        ),
    ),
    'service_manager' => array(
        'factories' => array(
            'User\Authorization\RbacListener' => 'User\Factory\RbacListenerFactory',
            
        ),
    ),
    
    'view_manager' => array(
        'template_path_stack' => array(
            __DIR__ . '/../view',
            'zfcuser' => __DIR__ . '/../view',
            'HtProfileImage' => __DIR__ . '/../view',
            
            'HtUserRegistration' => __DIR__ . '/../view',
        ),
        'template_map' => array(
            'zfc-user/user/login' => __DIR__ . '/../view/zfc-user/user/login.phtml',
        ),
    ),
     'view_helpers' => array(
        'invokables' => array(
        )
    ),
    'user_options'=>[
        'default_role'=>'member'
    ]
);

