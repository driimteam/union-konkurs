<?php

namespace User;

use User\Module as User;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use Doctrine\ORM\Mapping\Driver\XmlDriver;
use Zend\ModuleManager\ModuleManager;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ModuleManager\Feature\ServiceProviderInterface;
use Zend\Stdlib\Hydrator\ClassMethods;
use Zend\EventManager\EventInterface;
use Zend\Mvc\MvcEvent;
use Zend\View\Helper;
use Zend\Mvc\ModuleRouteListener;
use User\Controller\RedirectCallback;

class Module implements ServiceProviderInterface {

    public function onBootstrap(MvcEvent $e) {
        $eventManager = $e->getApplication()->getEventManager();
        //$moduleRouteListener = new ModuleRouteListener();
        //$moduleRouteListener->attach($eventManager);

        $application = $e->getParam('application');
        $sharedManager = $eventManager->getSharedManager();

        $rbacListener = $e->getApplication()->getServiceManager()->get('User\Authorization\RbacListener');
        $sharedManager->attach('Zend\View\Helper\Navigation\AbstractHelper', 'isAllowed', array($rbacListener, 'accept'));
        
        $t = $e->getTarget();

//
//        $sharedManager->attach('HtUserRegistration\Service\UserRegistrationService', 'verifyEmail.post', function (EventInterface $event) use ($application) {
//                    $serviceLocator = $application->getServiceManager();
//                    $userRegistrationService = $serviceLocator->get('user_registration_service');
//                    $userRegistrationService->onConfirmRegistration($event);
//                });
//
//        $sharedManager->attach('ZfcUser\Service\User', 'register', function (EventInterface $event) use ($application) {
//                    $serviceLocator = $application->getServiceManager();
//                    $userRegistrationService = $serviceLocator->get('user_registration_service');
//                    $userRegistrationService->onUserRegistration($event);
//                });


        $sharedManager->attach('ScnSocialAuth\Authentication\Adapter\HybridAuth', 'registerViaProvider', function (EventInterface $event) use ($application) {
                    $serviceLocator = $application->getServiceManager();
                    $userRegistrationService = $serviceLocator->get('user_registration_service');
                    $userRegistrationService->onRegisterViaProvider($event);
                });
                
        $sharedManager->attach('ZfcUser\Authentication\Adapter\AdapterChain', 'authenticate.fail', function (EventInterface $event) use ($application) {
            $serviceLocator = $application->getServiceManager();
            $userRegistrationService = $serviceLocator->get('user_registration_service');
            $userRegistrationService->onLoginFail($event);
        });
        
        $sharedManager->attach('ZfcUser\Authentication\Adapter\AdapterChain', 'authenticate.success', function (EventInterface $event) use ($application) {
            $serviceLocator = $application->getServiceManager();
            $userRegistrationService = $serviceLocator->get('user_registration_service');
            $userRegistrationService->onLoginSuccess($event);
        });

        $sharedManager->attach('ScnSocialAuth\Authentication\Adapter\HybridAuth', 'scnUpdateUser.post', function (EventInterface $event) use ($application) {
                    $serviceLocator = $application->getServiceManager();
                    $userRegistrationService = $serviceLocator->get('user_registration_service');
                    $userRegistrationService->onLoginViaProvider($event);
                });


    }

    public function init(ModuleManager $moduleManager) {
        $sharedEvents = $moduleManager->getEventManager()->getSharedManager();
        //$sharedEvents->attach(__NAMESPACE__,MvcEvent::EVENT_DISPATCH, array($this, 'initSubmenu'), 2);
    }

    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getControllerPluginConfig() {
        return array(
            'factories' => array(
            ),
        );
    }

    public function getServiceConfig() {
        $module = $this;
        return array(
            'invokables' => array(
                'User\Authentication\Adapter\Db' => 'User\Authentication\Adapter\Db',
                'user_service' => 'User\Service\User',
            ),
            'aliases' => array(
            ),
            'factories' => array(
                'user_registration_service' => 'User\Factory\UserRegistrationServiceFactory',
                'user_module_options' => 'User\Factory\ModuleOptionsFactory',
                'zfcuser_user_mapper' => function ($sm) {
                    $options = $sm->get('zfcuser_module_options');
                    $options->setEnableDefaultEntities(false);
                    $em = $sm->get('Doctrine\ORM\EntityManager');
                    $mapper = new \UserApp\Models\Mapper\User($em, $options);
                    return $mapper;
                },       
                'zfcuser_register_form' => function ($sm) {
                    $options = $sm->get('zfcuser_module_options');
                    $form = new Form\Register(null, $options);
                    $form->setInputFilter(new Form\RegisterFilter(
                            new \ZfcUser\Validator\NoRecordExists(array(
                        'mapper' => $sm->get('zfcuser_user_mapper'),
                        'key' => 'email'
                            )), new \ZfcUser\Validator\NoRecordExists(array(
                        'mapper' => $sm->get('zfcuser_user_mapper'),
                        'key' => 'username'
                            )), $options
                    ));
                    return $form;
                }
            ),
            'initializers' => array(
                'ZfcRbac\Initializer\AuthorizationServiceInitializer'
            )
        );
    }

    public function getConfig() {
        return include __DIR__ . '/config/module.config.php';
    }

}