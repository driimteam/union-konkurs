<?php

return array(
    'HOMEPAGE' => array(
        'name' => "Strona główna",
        'fields' => array(
            "title" => array('label' => "Tytuł", 'form_type' => \Cms\Form\ContentForm::ELEMENT_TYPE_TEXT,'required'=>true),
            "lead" => array('label' => "Podtytuł", 'form_type' => \Cms\Form\ContentForm::ELEMENT_TYPE_TEXT,'required'=>true),
            "text" => array('label' => "Opis",'form_type' => \Cms\Form\ContentForm::ELEMENT_TYPE_TINYMCE,'required'=>true),
            "status" => array('label' => "Status",'form_type' => \Cms\Form\ContentForm::ELEMENT_TYPE_STATUS,'required'=>true),
        ),
        'table_fields' => array("title", "status"),
        'has_gallery' => false,
        'has_attachments' => false,
        'related' => null,
        'is_search' => true,
        'search_fields'=>array('title','text'),
    ),
    'ARTICLE' => array(
        'name' => "Arytkuły",
        'fields' => array(
            "title" => array('label' => "Tytuł", 'form_type' => \Cms\Form\ContentForm::ELEMENT_TYPE_TEXT,'required'=>true),
            "lead" => array('label' => "Lead", 'form_type' => \Cms\Form\ContentForm::ELEMENT_TYPE_LEAD,'required'=>true),
            "main_picture" => array('label' => "Zdjęcie główne", 'form_type' => \Cms\Form\ContentForm::ELEMENT_TYPE_PICTURE,'required'=>false),
            "publication_date" => array('label' =>"Data publikacji", 'form_type' => \Cms\Form\ContentForm::ELEMENT_TYPE_DATE,'required'=>false),
            "text" => array('label' => "Opis",'form_type' => \Cms\Form\ContentForm::ELEMENT_TYPE_TINYMCE,'required'=>true),
            "status" => array('label' => "Status",'form_type' => \Cms\Form\ContentForm::ELEMENT_TYPE_STATUS,'required'=>true),
        ),
        'table_fields' => array("title", "status", "publication_date"),
        'has_gallery' => true,
        'has_attachments' => false,
        'is_search' => true,
        'search_fields'=>array('title','lead','text'),
        'related' => null,
        'isSortable'=>false,
        'sort'=>array('order'=>"cl.publication_date","dir"=>'asc')
    ),
    
    'ONE' => array(
        'name' => "Zwykła strona",
        'fields' => array(
            "title" => array('label' => "Tytuł", 'form_type' => \Cms\Form\ContentForm::ELEMENT_TYPE_TEXT,'required'=>true),
            "main_picture" => array('label' => "Zdjęcie główne", 'form_type' => \Cms\Form\ContentForm::ELEMENT_TYPE_PICTURE,'required'=>false),
            "text" => array('label' => "Opis",'form_type' => \Cms\Form\ContentForm::ELEMENT_TYPE_TINYMCE,'required'=>true),
            "status" => array('label' => "Status",'form_type' => \Cms\Form\ContentForm::ELEMENT_TYPE_STATUS,'required'=>true),
        ),
        'table_fields' => array("title", "status"),
        'has_gallery' => true,
        'has_attachments' => true,
        'related' => null,
        'is_search' => true,
        'search_fields'=>array('title','text'),
    ),
    'PRODUCTS' => array(
        'name' => "Produkty",
        'fields' => array(
            "title" => array('label' => "Nazwa", 'form_type' => \Cms\Form\ContentForm::ELEMENT_TYPE_TEXT,'required'=>true),
            "thumb" => array('label' => "Zdjęcie", 'form_type' => \Cms\Form\ContentForm::ELEMENT_TYPE_PICTURE,'required'=>true),
            "string_1" => array('label' => "Ikona", 'form_type' => \Cms\Form\ContentForm::ELEMENT_TYPE_PICTURE,'required'=>false),
            //"text" => array('label' => "Opis",'form_type' => \Cms\Form\ContentForm::ELEMENT_TYPE_TINYMCE,'required'=>true),
            "status" => array('label' => "Status",'form_type' => \Cms\Form\ContentForm::ELEMENT_TYPE_STATUS,'required'=>true),
        ),
        'table_fields' => array("thumb","title","status"),
        'has_gallery' => false,
        'has_attachments' => false,
        'is_search' => true,
        'related' => null,
        'search_fields'=>array('title','text'),
        'isSortable'=>true,
        'sort'=>array('order'=>"cl.title","dir"=>'asc')
    ),
    'CATEGORY_PRODUCTS' => array(
        'name' => "Kategoria produktów",
        'fields' => array(
            "title" => array('label' => "Nazwa", 'form_type' => \Cms\Form\ContentForm::ELEMENT_TYPE_TEXT,'required'=>true),
            "thumb" => array('label' => "Zdjęcie", 'form_type' => \Cms\Form\ContentForm::ELEMENT_TYPE_PICTURE,'required'=>true),
            "text" => array('label' => "Opis",'form_type' => \Cms\Form\ContentForm::ELEMENT_TYPE_TINYMCE,'required'=>true),
            "status" => array('label' => "Status",'form_type' => \Cms\Form\ContentForm::ELEMENT_TYPE_STATUS,'required'=>true),
        ),
        'table_fields' => array("thumb","title",'category_id', "text", "status"),
        'has_gallery' => false,
        'has_attachments' => false,
        'is_search' => true,
        'related' => null,
        'search_fields'=>array('title','text'),
        'isSortable'=>true,
        'sort'=>array('order'=>"cl.title","dir"=>'asc')
    ),
    'LINE_PRODUCTS' => array(
        'name' => "Linia produktów",
        'fields' => array(
            "title" => array('label' => "Nazwa", 'form_type' => \Cms\Form\ContentForm::ELEMENT_TYPE_TEXT,'required'=>true),
            "thumb" => array('label' => "Zdjęcie", 'form_type' => \Cms\Form\ContentForm::ELEMENT_TYPE_PICTURE,'required'=>true),
            "text" => array('label' => "Opis",'form_type' => \Cms\Form\ContentForm::ELEMENT_TYPE_TINYMCE,'required'=>true),
            "status" => array('label' => "Status",'form_type' => \Cms\Form\ContentForm::ELEMENT_TYPE_STATUS,'required'=>true),
        ),
        'table_fields' => array("thumb","title",'category_id', "text", "status"),
        'has_gallery' => true,
        'has_attachments' => false,
        'is_search' => true,
        'related' => null,
        'search_fields'=>array('title','text'),
        'isSortable'=>true,
        'sort'=>array('order'=>"cl.title","dir"=>'asc')
    ),
    'VIDEO' => array(
        'name' => "Filmy",
        'fields' => array(
            "title" => array('label' => "Tytuł", 'form_type' => \Cms\Form\ContentForm::ELEMENT_TYPE_TEXT,'required'=>true),
            "main_picture" => array('label' => "Zdjęcie główne", 'form_type' => \Cms\Form\ContentForm::ELEMENT_TYPE_PICTURE,'required'=>false),
            "string_1" => array('label' => "Zdjęcie małe",'form_type' => \Cms\Form\ContentForm::ELEMENT_TYPE_PICTURE,'required'=>false),
            "text" => array('label' => "Link",'form_type' => \Cms\Form\ContentForm::ELEMENT_TYPE_TEXT, 'required'=>true),
            "status" => array('label' => "Status",'form_type' => \Cms\Form\ContentForm::ELEMENT_TYPE_STATUS,'required'=>true),
        ),
        'table_fields' => array("title", "status"),
        'has_gallery' => false,
        'has_attachments' => false,
        'is_search' => false,
        'search_fields'=>array('title','lead','text'),
        'related' => null,
        'isSortable'=>false,
        'sort'=>array('order'=>"cl.publication_date","dir"=>'asc')
    ),
    'CONTACT_LIST' => array(
        'name' => "Kontakt",
        'fields' => array(
            "title" => array('label' => "Nazwa", 'form_type' => \Cms\Form\ContentForm::ELEMENT_TYPE_TEXT,'required'=>true),
            "text" => array('label' => "Adres kontaktowy",'form_type' => \Cms\Form\ContentForm::ELEMENT_TYPE_TINYMCE,'required'=>true),
            "string_1" => array('label' => "Współrzędne GPS",'form_type' => \Cms\Form\ContentForm::ELEMENT_TYPE_TEXT,'required'=>false),
            "status" => array('label' => "Status",'form_type' => \Cms\Form\ContentForm::ELEMENT_TYPE_STATUS,'required'=>true),
            
            
        ),
        'table_fields' => array("title", "status"),
        'has_gallery' => false,
        'has_attachments' => false,
        'is_search' => true,
        'search_fields'=>array('title','text'),
        'isSortable'=>true,
        'sort'=>array('order'=>"c.id","dir"=>'asc')
    ),
    
);


