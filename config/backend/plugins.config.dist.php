<?php

return array(
    'ARTICLE' => array(
        'name' => "Arytkuły", //nazwa pluginu
        'fields' => array( //zdefiniowane pola uzyte w tabeli cms_contents
            "title" => array('label' => "Tytuł", //nazwa pola
                             'form_type' => \Cms\Form\ContentForm::ELEMENT_TYPE_TEXT)//typ pola uzyty w formularzu,
        ),
        'table_fields' => array(), //wyswietlane pola w tabeli - lista
        'has_gallery' => true, //czy posiada galerie
        'has_attachments' => true, //czy posiada załączniki
        'is_search' => false, //czy obiket ma byc dodany do wyszukiwarki
        'related' => null, //obiekty powiazane
        'isSortable'=>false, //flaga okreslacja czy dany typ tresci ma mieć mozliwosc sortowania na liście (drag&drop)
        'sort'=>array('order'=>"cl.publication_date","dir"=>'asc') //kierunek sortowania
    ),
    
    
    
);
