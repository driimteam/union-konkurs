<?php

return array(
    'router' => array(
        'routes' => array(
            'admin' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/admin',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Admin\Controller',
                        'controller' => 'Index',
                        'action' => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'default' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/[:controller[/:action]]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                                '__NAMESPACE__' => 'Admin\Controller',
                                'controller' => 'Index',
                                'action' => 'index',
                            ),
                        ),
                    ),
                    'adminlist' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/uzytkownicy[/:action]',
                            'defaults' => array(
                                '__NAMESPACE__' => 'Admin\Controller',
                                'controller' => 'Admin',
                                'action' => 'index'
                            ),
                        ),
                    ),
                    'roles' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/role[/:action[/:roleId]]',
                            'defaults' => array(
                                '__NAMESPACE__' => 'Admin\Controller',
                                'controller' => 'Roles',
                                'action' => 'index'
                            ),
                        ),
                    ),
                    'permissions' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/uprawnienia[/:action[/:roleId]]',
                            'defaults' => array(
                                '__NAMESPACE__' => 'Admin\Controller',
                                'controller' => 'Permissions',
                                'action' => 'index'
                            ),
                        ),
                    ),
                ),
            ),
        ),
    )
);
