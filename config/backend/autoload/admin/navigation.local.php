<?php
return array(
    'navigation' => array(
         'default' => array(
//             array(
//                 'label' => 'Ustawienia',
//                 'route' => 'admin/adminlist',
//                 'permission'=>'admin',
//                 'id'=>'admin',
//                 'order'=>99,
//                 'pages' => array(
//                     array(
//                         'label' => 'Użytkownicy CMS',
//                         'id'=>'adminlist',
//                         'route' => 'admin/adminlist',
//                         'permission'=>'admin/adminlist',
//                         'pages' => array(
//                             array(
//                                'label' => 'Dodaj użytkownika CMS',
//                                'route' => 'admin/adminlist',
//                                'permission'=>'admin/adminlist/add',
//                                'controller'=> 'Admin',
//                                'action'=> 'add',
//                                 'params'=>array('controller'=>"admin","action"=>"add"),
//                            ),
//                         )
//
//                     ),
//                     array(
//                         'label' => 'Role',
//                         'id'=>'roles',
//                         'route' => 'admin/roles',
//                         'permission'=>'admin/roles',
//                         'pages' => array(
//                             array(
//                                'label' => 'Dodaj role',
//                                'route' => 'admin/roles',
//                                'permission'=>'admin/roles/add',
//                                'controller'=> 'Roles',
//                                'action'=> 'add',
//                                'leftmenu'=>true,
//                                'spanclass'=>'glyphicon glyphicon-plus-sign',
//                                'params'=>array('controller'=>"roles","action"=>"add"),
//                            ),
//                             array(
//                                'label' => 'Edycja',
//                                'route' => 'admin/roles',
//                                'permission'=>'admin/roles/edit',
//                                'controller'=> 'Roles',
//                                'action'=> 'edit',
//                                'params'=>array('controller'=>"roles","action"=>"edit"),
//                            ),
//                             array(
//                                'label' => 'Uprawnienia',
//                                'route' => 'admin/permissions',
//                                'permission'=>'admin/permissions',
//                                'controller'=> 'Permissions',
//                                'action'=> 'index',
//                                'params'=>array('controller'=>"permissions","action"=>"index"),
//                            ),
//                         )
//
//                     ),
//
//                 ),
//             ),
             array(
                 'label' => 'Wyloguj',
                 'route' => 'zfcuser/logout',
                 'permission'=>'logout',
                 'disable-ajax'=>true,
                 'id'=>'logout',
                 'order'=>999999,
                )
         ),
     ),
);
