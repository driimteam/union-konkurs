<?php
return array(
    'permissions' => array(
         'contents'=> array(
             'label'=>"Treści",
             'resources'=>[
                    'type'=>"object"
                    ]
         ),
        'users'=> array(
             'label'=>"Użytkownicy",
             'resources'=>[
                    'type'=>"array"
                    ]
        ),
        'groups'=> array(
             'label'=>"Grupy",
             'resources'=>[
                    'type'=>"array"
                 
                    ]
         ),
     ),
);
