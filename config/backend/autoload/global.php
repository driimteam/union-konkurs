<?php

/**
 * Global Configuration Override
 *
 * You can use this file for overridding configuration values from modules, etc.
 * You would place values in here that are agnostic to the environment and not
 * sensitive to security.
 *
 * @NOTE: In practice, this file will typically be INCLUDED in your source
 * control, so do not include passwords or other sensitive information in this
 * file.
 */
return array(
    'doctrine' => array(
        'configuration' => array(
            'orm_default' => array(
                'string_functions' => array(
                    'FIND_IN_SET' => 'DoctrineExtensions\Query\Mysql\FindInSet',
                    'IF' => 'DoctrineExtensions\Query\Mysql\IfElse',
                    'IFNULL' => 'DoctrineExtensions\Query\Mysql\IfNull'
                ),
                'metadata_cache' => 'array',
                'query_cache' => 'array',
                'result_cache' => 'array',
            ),
        )
    ),
    'db' => array(
        'driver' => 'Pdo',
        'driver_options' => array(
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''
        ),
    ),
    'service_manager' => array(
        'allow_override' => 'true',
        'factories' => array(
            'Zend\Db\Adapter\Adapter' => 'Zend\Db\Adapter\AdapterServiceFactory',
            'translator' => 'Zend\I18n\Translator\TranslatorServiceFactory',
            'Navigation' => 'Zend\Navigation\Service\DefaultNavigationFactory',
        ),
        'abstract_factories' => array(
            'Zend\Log\LoggerAbstractServiceFactory',
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory'
        ),
        'aliases' => array(
            'translator' => 'MvcTranslator',
        ),
    ),
    'translator' => array(
        'locale' => 'pl',
        'translation_file_patterns' => array(
            array(
                'type' => 'phparray',
                'base_dir' => 'language/backend',
                'pattern' => '%s.php',
            ),
        ),
    ),
    'view_helpers' => array(
        'invokables' => array(
            'photo' => '\App\View\Helper\Photo',
            'printTree' => '\App\View\Helper\PrintTree'
        )
    ),
    'multilang' => false,
    'languages' => array('PL', 'EN', 'RU'),
    'log' => array(
        'consolelogger' => array(
            'writers' => array(
                array(
                    'name' => 'stream',
                    'priority' => 1000,
                    'options' => array(
                        'stream' => __DIR__ .'/../../../data/console/logs/app-'.date('Y-m').'.log',
                        'mode'=>'a',
                    ),
                ),
            ),
        ),
        'loggerFreeGame' => array(
            'writers' => array(
                array(
                    'name' => 'stream',
                    'priority' => 1000,
                    'options' => array(
                        'stream' => __DIR__ .'/../../../data/console/logs/freegame-'.date('Y-m').'.log',
                        'mode'=>'a',
                    ),
                ),
            ),
        ),
        'loggerCumulationGame' => array(
            'writers' => array(
                array(
                    'name' => 'stream',
                    'priority' => 1000,
                    'options' => array(
                        'stream' => 'data/console/logs/cumulationgame-'.date('Y-m').'.log',
                        'mode'=>'a',
                    ),
                ),
            ),
        ),
    ),
        // ...languages
);
