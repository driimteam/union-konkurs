<?php
return array(
    'router' => array(
        'routes' => array(
            'users' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/users',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'module' => 'User',
                        'controller' => 'User\Controller\Index',
                        'action' => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'page' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '[/page/:page]',
                        ),
                    ),
                    'show' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/show[/:userId]',
                            'defaults' => array(
                                '__NAMESPACE__' => 'User\Controller',
                                'controller' => 'Index',
                                'action' => 'show',
                            ),
                        ),
                    ),
                ),
            ),
        ),
    ),
);
