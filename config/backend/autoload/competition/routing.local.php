<?php

$competitionsRoute = array(
    'type' => 'Segment',
    'options' => array(
        'route' => '/competitions',
        'defaults' => array(
            '__NAMESPACE__' => 'Competition\Controller',
            'module' => 'Competition',
            'controller' => 'Index',
            'action' => 'index'
        ),
    ),
    'may_terminate' => true,
    'child_routes' => array(
        'page' => array(
            'type' => 'Segment',
            'options' => array(
                'route' => '[/page/:page]',
            ),
        ),
        'show' => array(
            'type' => 'Segment',
            'options' => array(
                'route' => '/show[/id/:id]',
                'constraints' => array(
                    'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                ),
                'defaults' => array(
                    '__NAMESPACE__' => 'Competition\Controller',
                    'module' => 'Competition',
                    'controller' => 'Index',
                    'action' => 'show',
                ),
            ),
        ),
        'csv' => array(
            'type' => 'Segment',
            'options' => array(
                'route' => '/generatecsv',
                'defaults' => array(
                    '__NAMESPACE__' => 'Competition\Controller',
                    'controller' => 'Index',
                    'action' => 'generatecsv'
                ),
            ),
        ),
        'default' => array(
            'type' => 'Segment',
            'options' => array(
                'route' => '/[:action[/id/:id]]',
                'constraints' => array(
                    'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                ),
                'defaults' => array(
                    '__NAMESPACE__' => 'Competition\Controller',
                    'controller' => 'Index'
                ),
            ),
        ),
    ),
);


$verificationsRoute = array(
    'type' => 'Segment',
    'options' => array(
        'route' => '/verifications',
        'defaults' => array(
            '__NAMESPACE__' => 'Competition\Controller',
            'module' => 'Competition',
            'controller' => 'Index',
            'action' => 'verifications'
        ),
    ),
    'may_terminate' => true,
    'child_routes' => array(
        'page' => array(
            'type' => 'Segment',
            'options' => array(
                'route' => '[/page/:page]',
            ),
        ),
        'confirm' => array(
            'type' => 'Segment',
            'options' => array(
                'route' => '/confirm[/id/:id]',
                'defaults' => array(
                    '__NAMESPACE__' => 'Competition\Controller',
                    'controller' => 'Index',
                    'action' => 'confirm'
                ),
            ),
        ),
        'reject' => array(
            'type' => 'Segment',
            'options' => array(
                'route' => '/reject[/id/:id]',
                'defaults' => array(
                    '__NAMESPACE__' => 'Competition\Controller',
                    'controller' => 'Index',
                    'action' => 'reject'
                ),
            ),
        ),
    ),
);

return array(
    'router' => array(
        'routes' => array(
            'competitions' => $competitionsRoute,
            'verifications' => $verificationsRoute
        ),
    ),
);
