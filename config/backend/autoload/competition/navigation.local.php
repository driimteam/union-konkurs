<?php
return array(
    'navigation' => array(
        'default' => array(
            array(
                'id' => 'competitions',
                'label' => 'Moderacja ',
                'permission' => 'competition',
                'route' => 'verifications',
                'order' => 2,
            )
        ),
    ),
);
