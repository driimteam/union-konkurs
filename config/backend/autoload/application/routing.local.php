<?php
return array(
    'router' => array(
        'routes' => array(
            'home' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Application\Controller',
                        'controller'    => 'Index',
                        'action'        => 'Index',
                    ),
                ),
            ),
            'command' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/command',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Application\Controller',
                        'controller'    => 'Index',
                        'action'        => 'command',
                    ),
                ),
            ),

        ),
    ),
);
