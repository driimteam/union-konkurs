<?php
return array(
    'navigation' => array(
         'default' => array(
             array(
                 'label' => 'Start',
                 'route' => 'home',
                 'permission'=>'home',
                 'order'=>1,
             )
         ),
     ),
);

