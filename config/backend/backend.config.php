<?php
$env = getenv('APPLICATION_ENV') ?: 'production';

return array(
    'modules' => array(
//        'ZFTool',
//        'ZendDeveloperTools',
//        'SanSessionToolbar',
        'DoctrineModule',
        'DoctrineORMModule',
        'DoctrineExtensions',
        'ZfcRbac',
        'ZfcBase',
        'ZfcUser',
        'ZfcUserDoctrineORM',
        'MtMail',
        'app',
        'UserApp',
        'CompetitionApp',
        'Application',
        'Admin',
        'User',
        'Competition'

    ),
    'module_listener_options' => array(
        'config_glob_paths'    => array(
            'config/backend/autoload/{,*.}{global,local}.php',
            'config/backend/autoload/{,*.}' . $env. '.php',
            'config/backend/autoload/application/{,*.}{global,local}.php',
            'config/backend/autoload/admin/{,*.}{global,local}.php',
            'config/backend/autoload/user/{,*.}{global,local}.php',
            'config/backend/autoload/competition/{,*.}{global,local}.php',
            'config/backend/autoload/mail/{,*.}{global,local}.php',
        ),
        'module_paths' => array(
            './module/backend',
            './module/console',
            './module/commons',
            './vendor/doctrine',
            './vendor/zf-commons',
            './vendor',
        ),
        'config_cache_enabled' => false,

        'config_cache_key' => 'app_config',

        // Use the $env value to determine the state of the flag
        'module_map_cache_enabled' => false,

        'module_map_cache_key' => 'module_map',

        'cache_dir' => 'data/backend/cache',

        // Use the $env value to determine the state of the flag
        'check_dependencies' => false,
    ),
);

