<?php
$env = 'development';

return array(
    'modules' => array(
        'DoctrineModule',
        'DoctrineORMModule',
        'DoctrineExtensions',
        'ZfcBase',
        'ZfcUser',
        'ZfcUserDoctrineORM',
        'app',
        'UserApp',
        'QuizApp',
        'Console',
        'PaymentApp'
    ),
    'module_listener_options' => array(
        'config_glob_paths'    => array(
            'config/console/autoload/{,*.}{global,local}.php',
            'config/backend/autoload/{,*.}{global,local}.php',
            'config/backend/autoload/{,*.}' . $env. '.php',
            'config/backend/autoload/application/{,*.}{global,local}.php',
        ),
        'module_paths' => array(
            './module/console',
            './module/commons',
            './vendor/doctrine',
            './vendor/zf-commons',
            './vendor',
        ),
        'config_cache_enabled' => false,

        'config_cache_key' => 'app_config',

        // Use the $env value to determine the state of the flag
        'module_map_cache_enabled' => false,

        'module_map_cache_key' => 'module_map',

        'cache_dir' => 'data/console/cache',

        // Use the $env value to determine the state of the flag
        'check_dependencies' => false,
    ),
);