<?php

return array(
    'service_manager' => array(
        'factories' => array(
        ),
        'abstract_factories' => array(
            'Zend\Log\LoggerAbstractServiceFactory',
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory'
        )
    ),
    'caches' => array(
        'cache' => array(
            'adapter' => array(
                'name' => 'filesystem'
            ),
            'options' => array(
                'cache_dir' => 'data/frontend/cache',
            // other options
            ),
        ),
    ),
);
