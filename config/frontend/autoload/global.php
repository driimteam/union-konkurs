<?php

/**
 * Global Configuration Override
 *
 * You can use this file for overridding configuration values from modules, etc.
 * You would place values in here that are agnostic to the environment and not
 * sensitive to security.
 *
 * @NOTE: In practice, this file will typically be INCLUDED in your source
 * control, so do not include passwords or other sensitive information in this
 * file.
 */
return array(
    'db' => array(
        'driver' => 'Pdo',
        'dsn' => 'mysql:dbname=zf;host=localhost',
        'driver_options' => array(
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''
        ),
    ),
    'service_manager' => array(
        'factories' => array(
            'Zend\Db\Adapter\Adapter' => 'Zend\Db\Adapter\AdapterServiceFactory',
            'translator' => 'Zend\I18n\Translator\TranslatorServiceFactory',
            'Navigation' => 'Zend\Navigation\Service\DefaultNavigationFactory',
        ),
        'abstract_factories' => array(
                'Zend\Log\LoggerAbstractServiceFactory',
                'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
                'Zend\Form\FormAbstractServiceFactory'
            )
    ),
    'doctrine' => array(
        'configuration' => array(
            'orm_default' => array(
                'string_functions' => array(
                    'FIND_IN_SET' => 'DoctrineExtensions\Query\Mysql\FindInSet',
                    'IF' => 'DoctrineExtensions\Query\Mysql\IfElse',
                    'IFNULL' => 'DoctrineExtensions\Query\Mysql\IfNull'
                ),
                'metadata_cache' => 'array',
                'query_cache' => 'array',
                'result_cache' => 'array',
            )
        ),
        'eventmanager' => array(
            'orm_default' => array(
                'subscribers' => array(
                ),
            ),
        ),
    ),
    'translator' => array(
        'locale' => 'pl',
        'translation_file_patterns' => array(
            array(
                'type' => 'phparray',
                'base_dir' => 'language/frontend',
                'pattern' => '%s.php',
            ),
        ),
    ),
    'view_helpers' => array(
        'invokables' => array(
            'photo' => '\App\View\Helper\Photo',
            'stripHtmlTags' => '\App\View\Helper\StripHtmlTags',
            'prepareTitle' => '\Application\View\Helper\PrepareTitle'
        )
    ),
    'languages' => array('pl')
        // ...
);
