<?php
if(getenv('APPLICATION_ENV') == 'development'){
    $mailTransportOptions = array(
        'host' => 'smtp.gmail.com', //'smtp.gmail.com',
        'port' => '587',
        'connection_class' => 'login',
        'connection_config' => array(
            'username' => 'dev.driim',
            'password' => 'driimpswd2013',
            'ssl' => 'tls',
        ),
    );
    $mailConfig           = array(
        'transport' => 'Zend\Mail\Transport\Smtp',
        'transport_options' => $mailTransportOptions,
        'mailFrom' => 'dev.driim@gmail.com', //"dev.driim@gmail.com",
        'mailFromName' => "no-reply",
        'mailTo' => "dev.driim@gmail.com",
        'mailNameTo' => "Admin",
        'default_headers' => array(
                'from' => 'Konkurs <dev.driim@gmail.com>',
                'fromName'=>"Konkurs"
            ),
        'composer_plugins' => array(
            'MessageEncoding'
        ),
        'message_encoding' => 'UTF-8'
    );
    $goaliomailservice    = array(
        /**
         * Transport Class
         *
         * Name of Zend Transport Class to use
         */
        'type' => 'Zend\Mail\Transport\Smtp',
        'options' => $mailTransportOptions,
        /**
         * End of GoalioMailService configuration
         */
    );
}else{
    $mailTransportOptions = array(
        'host' => 'mail.maszpojecie.com.pl', //'smtp.gmail.com',
        'port' => '25',
        'connection_config' => array(
            'username' => 'noreplymp',
            'password' => 'beiQuai8'
        ),
    );
    $mailConfig           = array(
        'transport' => 'Zend\Mail\Transport\Sendmail',
        'transport_options' => $mailTransportOptions,
        'mailFrom' => 'maszpojecie.com.pl', //"dev.driim@gmail.com",
        'mailFromName' => "no-reply",
        'mailTo' => "kontakt@maszpojecie.com.pl",
        'mailNameTo' => "kontakt",
        'default_headers' => array(
                'from' => 'Union Investment TFI - maszpojecie.com.pl <no-reply@maszpojecie.com.pl>',
                'fromName'=>"Union Investment TFI - maszpojecie.com.pl"
            ),
        'composer_plugins' => array(
            'MessageEncoding'
        ),
        'message_encoding' => 'UTF-8'
    );
    $goaliomailservice    = array(
        /**
         * Transport Class
         *
         * Name of Zend Transport Class to use
         */
        'type' => 'Zend\Mail\Transport\Smtp',
        'options' => $mailTransportOptions,
        /**
         * End of GoalioMailService configuration
         */
    );
}

return array(
    'mail' => $mailConfig,
    'mt_mail' => $mailConfig,
    'goaliomailservice' => $goaliomailservice
);
