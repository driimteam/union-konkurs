<?php
return array(
    'navigation' => array(
        'default' => array(
//            array(
//                'label' => 'Loguj',
//                'route' => 'scn-social-auth-user/login',
//                'permission' => 'notlogged',
//                'id' => 'zfcuser/login',
//                'order' => 2,
//            ),
//            array(
//                'label' => 'Zarejestruj',
//                'route' => 'scn-social-auth-user/register',
//                'permission' => 'notlogged',
//                'id' => 'zfcuser/register',
//                'order' => 3,
//            ),
            array(
                'label' => 'Logout',
                'route' => 'scn-social-auth-user/logout',
                'permission' => 'logged',
                'id' => 'zfcuser/logout',
                'order' => 9999999,
            ),
        ),
    ),
);
