<?php
return array(
    'router' => array(
        'routes' => array(
            'profile' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/profile',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Profile',
                        'action' => 'account',
                    ),
                )
            ),
            'checkemail' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/checkemail[/:email]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Index',
                        'action' => 'checkemail'
                    ),
                ),
            ),
            'afterregister' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/afterregister',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Index',
                        'action' => 'afterregister'
                    ),
                ),
            ),
            'firstlogin' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/firstlogin',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Index',
                        'action' => 'firstlogin'
                    ),
                ),
            ),
        ),
    )
);
