<?php

$dbParams = array(
    'hostname'  => '10.11.1.220',
    'database'  => 'konkurs-union',
    'username'  => 'root',
    'password'  => 'grdyk@k0ta',
    
);
return array(
    'doctrine' => array(
        'connection' => array(
            'orm_default' => array(
                'driverClass' => 'Doctrine\DBAL\Driver\PDOMySql\Driver',
                'params' => array(
                    'host'=>$dbParams['hostname'],
                    'port'  => '3306',
                    'user' => $dbParams['username'],
                    'password' => $dbParams['password'],
                    'dbname'=> $dbParams['database'],
                    'charset' => 'utf8',
                    'driverOptions' => array(
                        PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'
                    ),
                  ),
                'doctrine_type_mappings' => array('enum' => 'string'),
              ),
           )
        ),
    'db' => array(
      'driver'    => 'pdo',
      'dsn' => 'mysql:dbname=' . $dbParams['database'] . ';host=' . $dbParams['hostname'],
        'database' => $dbParams['database'],
        'username' => $dbParams['username'],
        'password' => $dbParams['password'],
        'hostname' => $dbParams['hostname'],
    ),
    /*'service_manager' => array(
        'factories' => array(
            'Zend\Db\Adapter\Adapter' => function ($sm) use ($dbParams) {
                return new Zend\Db\Adapter\Adapter(array(
                    'driver'    => 'pdo',
                    'dsn'       => 'mysql:dbname='.$dbParams['database'].';host='.$dbParams['hostname'],
                    'database'  => $dbParams['database'],
                    'username'  => $dbParams['username'],
                    'password'  => $dbParams['password'],
                    'hostname'  => $dbParams['hostname'],
                ));
            },
        ),
    ),*/
);
