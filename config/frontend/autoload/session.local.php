<?php

return array(
    'service_manager' => [
        'factories' => [
            // Configures the default SessionManager instance
            'Zend\Session\ManagerInterface' => 'Zend\Session\Service\SessionManagerFactory',
            // Provides session configuration to SessionManagerFactory
            'Zend\Session\Config\ConfigInterface' => 'Zend\Session\Service\SessionConfigFactory',
        ],
        'aliases' => array(
            // Aliasing a FQCN to a service name
            'Zend\Session\SessionManager' => 'Zend\Session\ManagerInterface'
        ),
    ],
    'session_config' => array(
        'cache_expire'=>864000000,
        'gc_maxlifetime'=>864000000,
        'remember_me_seconds'=>864000000, //
        'cookie_lifetime' => 864000000, // dsa
        'name' => 'core_session',
        //'cookie_domain'=>'.devtest.driim.com',
        'use_cookies' => true,
        'cookie_secure'=>false,
        'cookie_httponly'=>false
    ),
    'session_manager' => [
        // SessionManager config: validators, etc
    ],
//    'session' => array(
//        'config' => array(
//            'class' => 'Zend\Session\Config\SessionConfig',
//            'options' => array(
//                'cache_expire'=>864000000,
//                'gc_maxlifetime'=>864000000,
//                'remember_me_seconds'=>864000000, //
//                'cookie_lifetime' => 864000000, // dsa
//                'name' => 'core_session',
//                //'cookie_domain'=>'.devtest.driim.com',
//                'use_cookies' => true,
//                'cookie_secure'=>false,
//                'cookie_httponly'=>false
//            ),
//        ),
//        'storage' => 'Zend\Session\Storage\SessionArrayStorage',
//        'validators' => array(
//            array(
//                'Zend\Session\Validator\RemoteAddr',
//                'Zend\Session\Validator\HttpUserAgent',
//            ),
//        ),
//    ),
);