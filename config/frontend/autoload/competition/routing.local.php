<?php

return array(
    'router' => array(
        'routes' => array(
            'home' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/',
                    'defaults' => array(
                        'controller' => 'Competition\Controller\Index',
                        'action' => 'index',
                        'lang' => 'pl',
                    ),
                ),
            ),
            'rules' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/zasady',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Competition\Controller',
                        'controller' => 'Index',
                        'action' => 'rules'
                    ),
                ),
            ),
            'rules' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/zasady',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Competition\Controller',
                        'controller' => 'Index',
                        'action' => 'rules'
                    ),
                ),
            ),
            'winners' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/zwyciezcy',
                    'defaults' => array(
                        'controller' => 'Competition\Controller\Index',
                        'action' => 'winners'
                    ),
                ),
            ),
            'finishedSuccess' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/zgloszenie-zapisane',
                    'defaults' => array(
                        'controller' => 'Competition\Controller\Index',
                        'action' => 'finishedSuccess'
                    ),
                ),
            ),
            
            'galleryVideo' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/galeria-wideo[/page/:page[/limit/:limit]]',
                    'defaults' => array(
                        'controller' => 'Competition\Controller\Index',
                        'action' => 'galleryVideo'
                    ),
                ),
            ),
            'galleryText' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/galeria-teksty[/page/:page[/limit/:limit]]',
                    'defaults' => array(
                        'controller' => 'Competition\Controller\Index',
                        'action' => 'galleryText'
                    ),
                ),
            ),
            'confirm_application' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/potwierdzenie-zgloszenia[/token/:token]',
                    'defaults' => array(
                        'controller' => 'Competition\Controller\Index',
                        'action' => 'confirmapplication'
                    ),
                ),
            ),
            
            
        ),
    ),
);
