<?php

return array(
    'navigation' => array(
        'default' => array(
            array(
                'id'=>'home',
                'label' => 'Strona główna',
                'permission'=>'all',
                'route' => 'home',
                'order' =>1
            ),
             array(
                'id'=>'rules',
                'label' => 'Zasady',
                'permission'=>'all',
                'route' => 'rules',
                'order' =>2
            ),
            array(
                'id'=>'gallery',
                'label' => 'Galeria',
                'permission'=>'all',
                'route' => 'galleryText',
                'order' =>2,
                'pages' => array(
                    array(
                        'id'=>'gallery-text',
                        'label' => 'Teksty',
                        'permission'=>'all',
                        'route' => 'galleryText',
                        'order' =>1
                    ),
                    array(
                        'id'=>'gallery-video',
                        'label' => 'Teksty',
                        'permission'=>'all',
                        'route' => 'galleryVideo',
                        'order' =>2
                    ),
                )
            ),
            array(
                'id'=>'winners',
                'label' => 'Zwycięzcy',
                'permission'=>'all',
                'route' => 'winners',
                'order' =>5
            ),
        ),
        'top_menu'=>array(
        ),
    ),
);
