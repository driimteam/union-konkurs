<?php
return array(
    'asset_manager' => array(
        'resolver_configs' => array(
            'collections' => array(
                'assets/js/compress.js' => array(
                    'js/vendor/modernizr-2.8.3-respond-1.4.2.min.js',
                    'js/jquery-1.11.3.min.js',
                    'js/bootstrap/bootstrap.min.js',
                    'js/jquery.validate.min.js',
                    'js/localization/messages_pl.js',
                    'js/jquery.validate-add.js',
                    'js/placeholders.min.js',
                    'js/js.cookie.js',
                    'js/main.js'
                ),
                'assets/css/compress.css' => array(
                    'css/main.css'
                ),
            ),
            'paths' => array(
                './public/frontend',
            ),
        ),
        'caching' => array(
            'assets/js/compress.js' => array(
//                'cache' => 'AssetManager\\Cache\\FilePathCache',
//                'options' => array(
//                    'dir' => 'public/frontend', // path/to/cache
//                ),
                'cache'     => 'Assetic\\Cache\\FilesystemCache',
                'options' => array(
                    'dir' => 'public/frontend/assets/js', // path/to/cache
                ),
            ),
            'assets/css/compress.css' => array(
//                'cache' => 'AssetManager\\Cache\\FilePathCache',
//                'options' => array(
//                    'dir' => 'public/frontend', // path/to/cache
//                ),
                'cache'     => 'Assetic\\Cache\\FilesystemCache',
                'options' => array(
                    'dir' => 'public/frontend/assets/css', // path/to/cache
                ),
            ),
        ),
    ),
);
