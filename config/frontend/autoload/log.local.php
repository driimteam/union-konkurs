<?php
return array(
    'log' => array(
        'logger' => array(
            'writers' => array(
                array(
                    'name' => 'stream',
                    'priority' => 1000,
                    'options' => array(
                        'stream' => 'data/frontend/logs/app.log',
                    ),
                ),
            ),
        ),
    ),
);