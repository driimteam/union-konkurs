<?php
return array(
    'caches' => array(
        'cache' => array(
            'adapter' => array(
                'name' => 'filesystem',
                'options' => array(
                    'ttl'=>'3600',
                    'cache_dir' => 'data/frontend/cache',
                    // other options
                ),
            ),
            
        ),
    ),
);